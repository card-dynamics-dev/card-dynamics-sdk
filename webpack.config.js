const webpack = require('webpack');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');
require('dotenv').config({ path: './.env' }); 

const stylesHandler = 'style-loader';

const config = {
    entry: './src/index.js',
    output: {
        path: path.resolve(__dirname, 'build'),
        filename: 'sdk-card-dynamics.js'
    },
    resolve: {
        fallback: {
            "crypto": require.resolve("crypto-browserify"),
            "stream": require.resolve("stream-browserify"),
            buffer: require.resolve('buffer/'),
        },
        extensions: ['.js']
	},
    plugins: [
        new HtmlWebpackPlugin({
            template: 'index.html',
        }),
        new webpack.DefinePlugin({
			"process.env": JSON.stringify(process.env)
		}),
        new webpack.ProvidePlugin({
            Buffer: ['buffer', 'Buffer'],
        }),
        new CopyPlugin({
            patterns: [
                {
                    from: path.resolve(__dirname, 'src/assets'),
                    to: path.resolve(__dirname, 'build/assets')
                },
                {
                    from: path.resolve(__dirname, 'replacement.html'),
                    to: path.resolve(__dirname, 'build/replacement.html')
                },
                {
                    from: path.resolve(__dirname, 'enrollment.html'),
                    to: path.resolve(__dirname, 'build/enrollment.html')
                },
                {
                    from: path.resolve(__dirname, 'demo.html'),
                    to: path.resolve(__dirname, 'build/demo.html')
                },
                {
                    from: path.resolve(__dirname, 'test.html'),
                    to: path.resolve(__dirname, 'build/test.html')
                },
                {
                    from: path.resolve(__dirname, 'versions'),
                    to: path.resolve(__dirname, 'build/versions')
                },
                {
                    from: path.resolve(__dirname, 'mit'),
                    to: path.resolve(__dirname, 'build/mit')
                },
                {
                    from: path.resolve(__dirname, 'authenticated.html'),
                    to: path.resolve(__dirname, 'build/authenticated.html')
                },
                {
                    from: path.resolve(__dirname, 'merchants.dev.json'),
                    to: path.resolve(__dirname, 'build/merchants.dev.json')
                },
                {
                    from: path.resolve(__dirname, 'merchants.pre.json'),
                    to: path.resolve(__dirname, 'build/merchants.pre.json')
                },
                {
                    from: path.resolve(__dirname, 'reports/cucumber-reports.html'),
                    to: path.resolve(__dirname, 'build/cucumber-reports.html')
                }
            ]
        })
    ],
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader'
                }
            },
            {
                test: /\.css$/i,
                use: [stylesHandler, 'css-loader'],
            },
            {
                test: /\.s[ac]ss$/i,
                use: [stylesHandler, 'css-loader', 'sass-loader'],
            },
            {
                test: /\.(eot|svg|ttf|woff|woff2|png|jpg|gif)$/i,
                type: 'asset',
            },

            // Add your rules for custom modules here
            // Learn more about loaders from https://webpack.js.org/loaders/
        ],
    },
    devServer: {
        static: {
            directory: path.join(__dirname, 'build'),
        },
        compress: true,
        port: 3200,
    },
    optimization: {
		splitChunks: {
		  chunks: 'async',
		  minSize: 30000,
		  maxSize: 0,
		  minChunks: 1,
		  maxAsyncRequests: 5,
		  maxInitialRequests: 3,
		  automaticNameDelimiter: '~',
		  name: false,
		  cacheGroups: {
			vendors: {
			  test: /[\\/]node_modules[\\/]/,
			  priority: -10
			},
			default: {
			  minChunks: 2,
			  priority: -20,
			  reuseExistingChunk: true
			}
		  }
		}
	}
};

module.exports = () => {
    if (process.env.NODE_ENV == 'production') {
        config.mode = 'production';

    }
    else {
        config.mode = 'development';
    }
    return config;
};
