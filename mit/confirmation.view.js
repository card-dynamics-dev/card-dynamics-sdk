export default function confirmationView(ctx) {
    try {

        createViewInHtml(ctx);
        addEventListeners();

    } catch (error) {
        console.log(error);
    }
}

function createViewInHtml(context) {

    let container = document.querySelector('div.checkout');
 
    let personalInfo = context?.personalInfo;
    let accounts = context?.paymentMethods?.bankAccounts;
    let accountsHtml = "";

    let index = 0;
    for (const account of accounts) {

        let selected = "";
        if(index === 0) selected = "selected";
        accountsHtml += `
            <div class="account ${selected}">
                <div class="account-holder"> ${ account.accountHolder } </div>
                <div class="number"> ${ account.number } </div>
            </div>`;
        index++;
    }

    let html = `<div class="confirmation">
        <h1> Informacion personal </h1>
        <div class="personal-info">`;

    if(personalInfo.name){
        html += `<div>
            <label> Nombre </label>
            <span> ${personalInfo.name} </span>
        </div>`
    }

    if(personalInfo.surname || personalInfo.surname2){
        html += `<div>
            <label> Apellidos </label>
            <span> ${personalInfo.surname} ${personalInfo.surname2} </span>
        </div>`;
    }

    if(personalInfo.email){
        html += `<div>
                <label> Correo electrónico </label>
                <span> ${personalInfo.email} </span>
            </div>`
    }
            if(personalInfo.document){
                html += `<div>
                <label> CIP </label>
                <span> ${personalInfo.document} </span>
            </div>`
        }
        if(personalInfo.birthday){
            html += `<div>
                <label> Fecha de nacimiento </label>
                <span> ${personalInfo.birthday} </span>
            </div>`
        }
            if(personalInfo["phone-number"]){
                html += `<div>
                <label> Teléfono </label>
                <span> ${personalInfo["phone-number"]}  </span>
            </div>`
        }

        html += `</div>`;
        if(personalInfo["address-street"] ){
            html += ` <h1> Dirección de envío </h1>
            <div class="address">`

            if(personalInfo["address-street"]){
                html += `<div> ${personalInfo["address-street"] } </div>`
            }

            if(personalInfo["address-postal-code"] || personalInfo["address-city"]){
                html += `<div> ${personalInfo["address-postal-code"] } ${ personalInfo["address-city"] } </div>`
            }

            if(personalInfo["address-province"] || personalInfo["address-country"]){
                html += `<div> ${personalInfo["address-province"] } ${ personalInfo["address-country"] } </div>`
            }


            html += `</div>`
        }
            

    container.outerHTML = `
        
        ${html}
        
        <h1> Datos de pago </h1>
        <div class="accounts">
            ${ accountsHtml }
        </div>

        <button id="confirm">Confirmar</button>
    </div>`;

}

function addEventListeners(){

    let accounts = document.querySelectorAll('div.accounts div.account');
    for (const account of accounts) {

        account.addEventListener('click', function(){
            accounts.forEach(x => x.classList.remove('selected'));

            this.classList.add('selected');
        });
        
    }

    let confirm = document.getElementById('confirm');
    if(confirm){
        confirm.addEventListener('click', function(){
            let container = document.querySelector('div.confirmation');
            container.outerHTML = `<div class="congrats">
                <img src="./assets/img/fireworks.png" />
                <div>Gracias por realizar el pedido!</div>
                
                <a href="./index.html">Seguir comprando</a>
            </div>`;
        });
    }
}