Feature: Reemplazo Comercial
Scenario: [OC] Levantar el proyecto y pintar el formulario y la tarjeta nueva y el listado de comercios
   Given [OC] Levantar proyecto
   Given [OC] Rellenar formulario tarjeta antigua
   Then [OC] Pintar la tarjeta nueva
   Then [OC] Pintar el listado de 4 comecios y el primero es Cucumber M1
   Given [OC] Selecionar el comercio Cucumber M2
   Then [OC] Habilitar el boton Actualizar
   Given [OC] Lanzar reemplazo operacional
   Then [OC] Pintar el estado de reemplazo del comercio Cucumber M2
   Then [OC] Estado de reemplazo -- Pendiente --