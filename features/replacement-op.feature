Feature: Reemplazo Operacional
Scenario: [OP] Levantar el proyecto y pintar las tarjetas y listado de comercios
   Given [OP] Levantar proyecto
   Then [OP] Pintar la tarjeta antigua
   Then [OP] Pintar la tarjeta nueva
   Then [OP] Pintar el listado de 4 comecios y el primero es Cucumber M1
   Given [OP] Selecionar el comercio Cucumber M2
   Then [OP] Habilitar el boton Actualizar
   Given [OP] Lanzar reemplazo operacional
   Then [OP] Pintar el estado de reemplazo del comercio Cucumber M2
   Then [OP] Estado de reemplazo -- Pendiente --