Feature: [Botón Registro] Botón 
Scenario: Levantar el proyecto y pintar el botón
   Given Levantar proyecto
   Then Pintar el botón
Scenario: Cargar pantalla de bancos y pintar el banco caixa
   Given Ir a la pantall de bancos
   Then Pintar el banco caixa
Scenario: Cargar pantalla de login y pintar el texto de TCs y Politica de privacidad
   Given Ir a la pantall de login
   Then Pintar el texto de TCs y politica de privacidad 
   Then Botón login deshabilitado por defecto
Scenario: Rellenar los campos obligatorios del login
   Given Rellenar los campos obligatorios
   Then Botón login habilitado
Scenario: Cargar pantalla de contraseñas
   Given Ir a la pantalla de contraseña
   Then Botón continuar deshabilitado por defecto
Scenario: Rellenar las contraseñas y aceptar los TCs y politica de privacidad
   Given Rellenar las contraseñas
   Given Aceptar los TCs y politica de privacidad
   Then Botón continuar habilitado
Scenario: Cargar pantalla de registro
   Given Ir a la pantalla de registro
   Then Contraseña rellena por defecto
   Then Botón continuar habilitado por defecto
Scenario: Cargar pantalla de medio de pago (tarjetas)
   Given Ir a la pantalla de medio de pago
   Then Tab tarjetas activo por defecto
   Then CVV vacio
   Then Botón registrame deshabilitado por defecto
   Given Rellenar CVV
   Then Botón registrame habilitado
Scenario: Seleccionar medio de pago cuentas
   Given Seleccionar medio de pago cuentas
   Then Botón registrame habilitado en cuentas