Feature: Registro desde el banco
Scenario: [Registro desde el banco] Levantar el proyecto y pintar el listado de comercios
   Given [Registro] Levantar proyecto
   Then [Registro] Pintar el listado de comercios
Scenario: [Registro desde el banco] Cargar pantalla de registro 
   Given [Registro] Ir a la pantalla de registro
   Then [Registro] Botón continuar deshabilitado por defecto
   Given [Registro] Rellenar la contraseña
   Given [Registro] Aceptar los TCs y politica de privacidad
   Then [Registro] Botón continuar habilitado
Scenario: [Registro desde el banco] Cargar pantalla de medio de pago (tarjetas)
   Given [Registro] Ir a la pantalla de medio de pago
   Then [Registro] Tab tarjetas activo por defecto
   Then [Registro] CVV vacio
   Then [Registro] Botón registrame deshabilitado por defecto
   Given [Registro] Rellenar CVV
   Then [Registro] Botón registrame habilitado
Scenario: [Registro desde el banco] Seleccionar medio de pago cuentas
   Given [Registro] Seleccionar medio de pago cuentas
   Then [Registro] Botón registrame habilitado en cuentas