const { Given, When, Then, AfterAll, Before, After } = require('@cucumber/cucumber');
const { Builder, By, Capabilities, until, Key } = require('selenium-webdriver');
const { expect, assert, should } = require('chai');

require("chromedriver");

// driver setup
const capabilities = Capabilities.chrome();
capabilities.set('chromeOptions', { "w3c": false });

let driver;

let wait = (seconds) => {
  return new Promise((resolve) => {
    setTimeout(resolve, seconds * 1000);
  });
}

Given('Levantar proyecto', async function () {
  driver = new Builder().withCapabilities(capabilities).build();
  await driver.get('http://localhost:3200?demo=true&language=en');
});

Then('Pintar el botón', async function () {
  await driver.executeScript(`

    document.body.innerHTML = '<div id="cd-sdk-button-container"></div>'; 
    
    let carddynamics = new Carddynamics({
      country: "es",
      language: "es",
      customerId: 39,
      cdMerchantId: 39,
      sdkButtonType: "combined"
    });

    let options = {
      type: "default",
      theme: "white",
      fontsize: "13px",
      fontweight: "550",
      logowidth: "50px",
      messagetype: "REG",
      slogan: false,
    };

    carddynamics.renderButton(options, function (result) {
      console.log(result);
    });

  `);


  let button = await driver.findElement(By.id('cd-sdk-button'));
  let text = await button.getText();

  expect(text).equal('Register with your bank');

});

Given('Ir a la pantall de bancos', async function () {
  let button = await driver.findElement(By.id('cd-sdk-button'));
  await button.click();
});

Then('Pintar el banco caixa', async function () {
  let caixa = await driver.wait(until.elementLocated(By.css("div[adapter-code='MBCXABNKES'] div")), 5000);
  let text = await caixa.getText();
  expect(text).equal('Caixabank');
});

Given('Ir a la pantall de login', async function () {
  let caixa = await driver.wait(until.elementLocated(By.css("div[adapter-code='MBCXABNKES']")), 5000);
  await caixa.click();
});

Then('Pintar el texto de TCs y politica de privacidad', async function () {
  await wait(2);

  let textEl = await driver.wait(until.elementLocated(By.css('div.cd-sdk-privacy-row')), 5000);
  let text = await textEl.getText();

  assert(text == 'By continuing you accept the Terms and Conditions and the Privacy Policy');

});

Then('Botón login deshabilitado por defecto', async () => {

  let btnLogin = await driver.wait(until.elementLocated(By.id("btn-login")), 5000);
  let disabled = await btnLogin.getAttribute("disabled");

  expect(disabled).equal("true");
});

Given('Rellenar los campos obligatorios', async function () {
  let username = await driver.wait(until.elementLocated(By.id("username")), 5000);
  await username.sendKeys("username");

  let password = await driver.wait(until.elementLocated(By.id("password")), 5000);
  await password.sendKeys("123456");
});

Then('Botón login habilitado', async function () {
  let btnLogin = await driver.wait(until.elementLocated(By.id("btn-login")), 5000);
  let disabled = await btnLogin.getAttribute("disabled");
  expect(disabled).equal(null);

});

Given('Ir a la pantalla de contraseña', async function () {
  let btnLogin = await driver.wait(until.elementLocated(By.id("btn-login")), 5000);
  btnLogin.click();
});

Then('Botón continuar deshabilitado por defecto', async function () {
  await wait(2);

  let btnContinue = await driver.wait(until.elementLocated(By.id("btn-continue")), 5000);
  let disabled = await btnContinue.getAttribute("disabled");

  expect(disabled).equal("true");
});

Given('Rellenar las contraseñas', async function () {
  let password = await driver.wait(until.elementLocated(By.id("password")), 5000);
  await password.sendKeys("Cd@2023!!");

  let confirmPassword = await driver.wait(until.elementLocated(By.id("confirm-password")), 5000);
  await confirmPassword.sendKeys("Cd@2023!!");
});

Given('Aceptar los TCs y politica de privacidad', async function () {
  let privacy = await driver.wait(until.elementLocated(By.xpath("//label[@for='ch-privacy']")), 5000);
  await privacy.click();

  let terms = await driver.wait(until.elementLocated(By.xpath("//label[@for='ch-terms']")), 5000);
  await terms.click();
});

Then('Botón continuar habilitado', async function () {
  let btnContinue = await driver.wait(until.elementLocated(By.id("btn-continue")), 5000);
  let disabled = await btnContinue.getAttribute("disabled");
  expect(disabled).equal(null);
});

Given('Ir a la pantalla de registro', async function () {
  let btnContinue = await driver.wait(until.elementLocated(By.id("btn-continue")), 5000);
  btnContinue.click();
});

Then('Contraseña rellena por defecto', async function () {
  await wait(2);
  
  let password = await driver.wait(until.elementLocated(By.css('input[type=password]')), 7000);
  let value = await password.getAttribute("value");

  expect(value).equal('Cd@2023!!');
});

Then('Botón continuar habilitado por defecto', async function () {
  let btnContinue = await driver.wait(until.elementLocated(By.id("btn-continue")), 5000);
  let disabled = await btnContinue.getAttribute("disabled");

  expect(disabled).equal(null);
});

Given('Ir a la pantalla de medio de pago', async function () {

  await driver.executeScript(`
  let container = document.querySelector('#cd-sdk-container');
  if (container) {
    container.scroll({ top: container.offsetHeight });
  }`);

  await wait(2);

  let btnContinue = await driver.wait(until.elementLocated(By.css("div.cd-sdk-register-container button[name=btn-continue]")), 5000);
  btnContinue.click();
});

Then('Tab tarjetas activo por defecto', async function () {
  let tab = await driver.wait(until.elementLocated(By.css('ul.account-tabs li.active')), 5000);
  let text = await tab.getText();

  expect(text).equal('Cards');
});

Then('CVV vacio', async function () {
  let cvv = await driver.wait(until.elementLocated(By.id('txt-cvv0')), 5000);
  let value = await cvv.getAttribute('');

  expect(value).equal(null);
});

Then('Botón registrame deshabilitado por defecto', async function () {
  let btnContinue = await driver.wait(until.elementLocated(By.id("btn-continue")), 5000);
  let disabled = await btnContinue.getAttribute("disabled");

  expect(disabled).equal("true");
});

Given('Rellenar CVV', async function () {
  let cvv = await driver.wait(until.elementLocated(By.id('txt-cvv0')), 5000);
  await cvv.sendKeys("123");
});

Then('Botón registrame habilitado', async function () {
  let btnContinue = await driver.wait(until.elementLocated(By.id("btn-continue")), 5000);
  let disabled = await btnContinue.getAttribute("disabled");

  expect(disabled).equal(null);
});

Given('Seleccionar medio de pago cuentas', async function () {
  let tab = await driver.wait(until.elementLocated(By.css('ul.account-tabs li[id=accounts]')), 5000);
  await tab.click();
});

Then('Botón registrame habilitado en cuentas', async function () {
  let btnContinue = await driver.wait(until.elementLocated(By.id("btn-continue")), 5000);
  let disabled = await btnContinue.getAttribute("disabled");

  expect(disabled).equal(null);
});

AfterAll(function(){
  driver.close();
})
