const { Given, When, Then, AfterAll, Before, After } = require('@cucumber/cucumber');
const { Builder, By, Capabilities, until, Key } = require('selenium-webdriver');
const { expect, assert, should } = require('chai');

require("chromedriver");

// driver setup
const capabilities = Capabilities.chrome();
capabilities.set('chromeOptions', { "w3c": false });

let driver;

let wait = (seconds) => {
  return new Promise((resolve) => {
    setTimeout(resolve, seconds * 1000);
  });
}

Given('[Registro] Levantar proyecto', async function () {
  driver = new Builder().withCapabilities(capabilities).build();
  await driver.get('http://localhost:3200?demo=true&language=en');
});

Then('[Registro] Pintar el listado de comercios', async function () {
  await driver.executeScript(`

  let carddynamics = new Carddynamics({
    language: 'es',
    issuerId: 7
  });

  carddynamics.initEnrollment({
    defaultOthersInfo: {
      loader:{
        mobile: "",
        web: "",
        height: 2,
        textVisibility: true,
        box: true
      },
      header: {
        text: "Cucumber",
        logo: ""
      },
      cssOverride: {
        colors: {
        },
        fontUrl: "",
        style: ""
      },
      url: "https://www.card-dynamics.com/en/",
      urlResponse: "https://demo2736434.mockable.io/sdk/response",
      hiddenCVV: false
    },
    defaultPersonalInfo: {
      "email": "dromerof@gmail.com",
      "document": "49651235T",
      "name": "OnePhase",
      "surname": "Perez",
      "surname2": "lopez",
      "birthday": "22/01/1985",
      "phone-prefix": "+52",
      "phone-number": "7772965687",
      "address-street": "Calle cabo machichaco",
      "address-number": "38",
      "address-other": "",
      "address-country": "España",
      "address-province": "Madrid",
      "address-city": "Las rozas de Madrid",
      "address-postal-code": "28290"
    },
    defaultPaymentMethods: {
      "creditCards":[
        {"cardHolder":"Chema Chico Gordillo","pan":"5136617217950747","expDate":"0823","country":"ES","currency":["EUR"]},
        {"cardHolder":"Chema Chico Gordillo","pan":"2480359066246773","expDate":"0923","country":"ES","currency":["EUR"]}
      ],"bankAccounts":[
        {"accountHolder":"Chema Chico Gordillo","country":"ES","number":"ES9767124426581758922081","currency":["EUR"]},
        {"accountHolder":"Chema Chico Gordillo","country":"ES","number":"ES6214144642783822069925","currency":["EUR"]}
      ]
    }
  });
`);

  await wait(3);
  
  let categories = await driver.findElements(By.css('div.cd-sdk-merchants-category-container>h2'));
  
  expect(categories.length).equal(1);

  let category = categories[0];
  let text = await category.getText();

  expect(text).equal("LIFESTYLE");

});

Given('[Registro] Ir a la pantalla de registro', async function () {
  let btnRegister = await driver.wait(until.elementLocated(By.css('button.btn-card[data-merchantid="39"]')), 5000);
  btnRegister.click();

  await wait(3);
});

Then('[Registro] Botón continuar deshabilitado por defecto', async function () {
  let btnContinue = await driver.wait(until.elementLocated(By.id("btn-continue")), 5000);
  let disabled = await btnContinue.getAttribute("disabled");

  expect(disabled).equal("true");
});

Given('[Registro] Rellenar la contraseña', async function () {
  let password = await driver.wait(until.elementLocated(By.css(".cd-sdk-form-txt.cd-sdk-form-pass")), 5000);
  await password.sendKeys("Cd@2023!!");
});

Given('[Registro] Aceptar los TCs y politica de privacidad', async function () {
  let privacy = await driver.wait(until.elementLocated(By.xpath("//label[@for='ch-privacy']")), 5000);
  await privacy.click();

  let terms = await driver.wait(until.elementLocated(By.xpath("//label[@for='ch-terms']")), 5000);
  await terms.click();
});

Then('[Registro] Botón continuar habilitado', async function () {
  let btnContinue = await driver.wait(until.elementLocated(By.id("btn-continue")), 5000);
  let disabled = await btnContinue.getAttribute("disabled");
  expect(disabled).equal(null);
});

Given('[Registro] Ir a la pantalla de medio de pago', async function () {

  await driver.executeScript(`
  let container = document.querySelector('#cd-sdk-container');
  if (container) {
    container.scroll({ top: container.offsetHeight });
  }`);

  await wait(2);

  let btnContinue = await driver.wait(until.elementLocated(By.css("div.cd-sdk-register-container button[name=btn-continue]")), 5000);
  btnContinue.click();
});

Then('[Registro] Tab tarjetas activo por defecto', async function () {
  let tab = await driver.wait(until.elementLocated(By.css('ul.account-tabs li.active')), 5000);
  let text = await tab.getText();

  expect(text).equal('Cards');
});

Then('[Registro] CVV vacio', async function () {
  let cvv = await driver.wait(until.elementLocated(By.id('txt-cvv0')), 5000);
  let value = await cvv.getAttribute('');

  expect(value).equal(null);
});

Then('[Registro] Botón registrame deshabilitado por defecto', async function () {
  let btnContinue = await driver.wait(until.elementLocated(By.id("btn-continue")), 5000);
  let disabled = await btnContinue.getAttribute("disabled");

  expect(disabled).equal("true");
});

Given('[Registro] Rellenar CVV', async function () {
  let cvv = await driver.wait(until.elementLocated(By.id('txt-cvv0')), 5000);
  await cvv.sendKeys("123");
});

Then('[Registro] Botón registrame habilitado', async function () {
  let btnContinue = await driver.wait(until.elementLocated(By.id("btn-continue")), 5000);
  let disabled = await btnContinue.getAttribute("disabled");

  expect(disabled).equal(null);
});

Given('[Registro] Seleccionar medio de pago cuentas', async function () {
  let tab = await driver.wait(until.elementLocated(By.css('ul.account-tabs li[id=accounts]')), 5000);
  await tab.click();
});

Then('[Registro] Botón registrame habilitado en cuentas', async function () {
  let btnContinue = await driver.wait(until.elementLocated(By.id("btn-continue")), 5000);
  let disabled = await btnContinue.getAttribute("disabled");

  expect(disabled).equal(null);
});

AfterAll(function(){
  driver.close();
})

