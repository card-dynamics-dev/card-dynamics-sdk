const { Given, When, Then, AfterAll, Before, After } = require('@cucumber/cucumber');
const { Builder, By, Capabilities, until, Key } = require('selenium-webdriver');
const { expect, assert, should } = require('chai');

require("chromedriver");

// driver setup
const capabilities = Capabilities.chrome();
capabilities.set('chromeOptions', { "w3c": false });

let driver;

let wait = (seconds) => {
  return new Promise((resolve) => {
    setTimeout(resolve, seconds * 1000);
  });
}

Given('[OC] Levantar proyecto', async function () {
  driver = new Builder().withCapabilities(capabilities).build();
  await driver.get('http://localhost:3200');

  await driver.executeScript(`

    let carddynamics = new Carddynamics({
      language: 'es',
      issuerId: 7
    });

    carddynamics.initReplacement({
      ids: [],
      newCard: {
        pan: '5579100193584602',
        expDate: '1021',
        cardBackground: ""
      },
      othersInfo: {
        header: {
          background: "#d60235",
          text: "Cucumber Replacement OC"
        },
        cssOverride: {
          colors: {
            primary: '#d60235',
            accent: '#93c3a2',
            warn: '#ff7600'
          },
          style: ""
        },
        urlResponse: "https://demo2736434.mockable.io/sdk/response"
      }
    });
  `);

});

Given('[OC] Rellenar formulario tarjeta antigua', async function () {

  await wait(3);

  let pan = await driver.wait(until.elementLocated(By.css("form div.cd-sdk-card-form-row input[id=txt-number]")), 5000);
  await pan.sendKeys("5555444433332222");

  let expiry = await driver.wait(until.elementLocated(By.css("form div.cd-sdk-card-form-row input[id=txt-expiry]")), 5000);
  await expiry.sendKeys("1224");

  let owner = await driver.wait(until.elementLocated(By.xpath("//label[@for='owner']")), 5000);
  await owner.click();

  let add = await driver.wait(until.elementLocated(By.id("btn-add")), 5000);
  await add.click();

});

Then('[OC] Pintar la tarjeta nueva', async function () {

  await wait(3);

  let pan = await driver.wait(until.elementLocated(By.css('div.cd-sdk-credit-card:last-child div.card-box-data>span')), 6000);
  let value = await pan.getText();

  expect(value).equal('****4602');

});

Then('[OC] Pintar el listado de 4 comecios y el primero es Cucumber M1', async function () {

  let merchants = await driver.findElements(By.css('fieldset.merchants-list div.checkbox-bl-type2 label.checkbox-label'));
  
  expect(merchants.length).equal(4);

  let merchant = merchants[0];
  let text = await merchant.getText();

  expect(text).equal("Cucumber M1");

});

Given('[OC] Selecionar el comercio Cucumber M2',  async function () {

  let m2 = await driver.wait(until.elementLocated(By.xpath("//label[@for='40']")), 5000);
  await m2.click();

});

Then('[OC] Habilitar el boton Actualizar', async function () {
  let update = await driver.wait(until.elementLocated(By.id("update-btn")), 5000);
  let disabled = await update.getAttribute("disabled");
  expect(disabled).equal(null);
});

Given('[OC] Lanzar reemplazo operacional', async function () {
  let update = await driver.wait(until.elementLocated(By.id("update-btn")), 5000);
  await update.click();
});

Then('[OC] Pintar el estado de reemplazo del comercio Cucumber M2', async function () {

  let h3 = await driver.wait(until.elementLocated(By.css("div.cd-sdk-merchants-container>h3")), 5000);
  let value = await h3.getText();

  expect(value).equal('Estado de reemplazo');

  let merchants = await driver.findElements(By.css('div.cd-sdk-merchants-state div.cd-sdk-merchant-state>div:first-child'));
  
  expect(merchants.length).equal(1);

  let merchant = merchants[0];
  let text = await merchant.getText();

  expect(text).equal("Cucumber M2");

});

Then('[OC] Estado de reemplazo -- Pendiente --', async function () {

  let state = await driver.wait(until.elementLocated(By.css('div.cd-sdk-merchants-state div.cd-sdk-merchant-state div.cd-sdk-merchant-state-value span.state-desc')), 5000);
  let text = await state.getText();

  expect(text).equal("Pendiente");

});

AfterAll(function(){
  driver.close();
})


