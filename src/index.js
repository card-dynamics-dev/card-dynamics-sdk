
//import './assets/style/common.scss';
//import './assets/style/style.scss'
//import './assets/style/responsive.scss'

import addButtonView from './views/button/addButtonView';
import addMerchantView from './views/common/addMerchantView';
import addCardView from './views/replacement/addCardView';


(() => {

  //overlay : blur|transparent|carddynamics
  //sdkButtonType: simple|combined,

  class Carddynamics {

    constructor(settings) {
      this.settings = settings;
    }


    renderButton(options, callback) {

      let defaultSettings = { 
        sdkButtonType: 'simple',
        overlay: 'carddynamics',
        form: true
      };

      let context = { 
        button: true, 
        settings: Object.assign(defaultSettings, this.settings) 
      };

      const queryString = window.location.search;
      const urlParams = new URLSearchParams(queryString);

      if(urlParams && urlParams.get('language')){
        context.settings.language = urlParams.get('language');
      }

      if(urlParams && urlParams.get('demo')){
        context.settings.demo = urlParams.get('demo');
      }

      if(urlParams && urlParams.get('test')){
        context.settings.test = urlParams.get('test');
      }

      if(urlParams && urlParams.get('country')){
        context.settings.country = urlParams.get('country');
      }

      addButtonView(context, options, callback);
    }

    initEnrollment(enrollmentData) {
      
      let context = { 
        settings: Object.assign({ sdkButtonType: 'simple'}, this.settings)
      };

      if(enrollmentData){
        let { defaultPersonalInfo, defaultPaymentMethods, defaultOthersInfo } = enrollmentData;

        if(defaultPersonalInfo){
          context.personalInfo = defaultPersonalInfo;
        }

        if(defaultPaymentMethods){
          context.paymentMethods = defaultPaymentMethods;
        }

        if(defaultOthersInfo){
          context.othersInfo = defaultOthersInfo;
        }
      }

      const queryString = window.location.search;
      const urlParams = new URLSearchParams(queryString);

      if (urlParams && urlParams.get('data')) {
        const data = decodeURIComponent(urlParams.get('data'));
        const issuerId = urlParams.get("is_id");
        context = { data, settings: { ...context.settings, issuerId }};
      }

      if(urlParams && urlParams.get('language')){
        context.settings.language = urlParams.get('language');
      }

      addMerchantView(context);
    }

    initReplacement(replacementData){

      let context = {
        ...replacementData,
        settings: { ...this.settings },
        replacementReason: "OC",
      };

      //Replacement with Data
      const queryString = window.location.search;
      const urlParams = new URLSearchParams(queryString);

      if (urlParams && urlParams.get('data')) {
        const data = decodeURIComponent(urlParams.get('data'));
        const issuerId = urlParams.get("is_id");
        context = { data, settings: { ...this.settings, issuerId }};
      }

      if(urlParams && urlParams.get('language')){
        context.settings.language = urlParams.get('language');
      }

      addCardView(context);
    }
  }

  window.Carddynamics = Carddynamics;
})();