
const crypto = require('crypto');
const CIPHER_ALGORITHM = 'aes-256-gcm';

var aes256 = {
  encrypt: function (masterkey, plaintext) {

    if (typeof masterkey !== 'string' || !masterkey) {
      throw new TypeError('Provided "key" must be a non-empty string');
    }
    if (typeof plaintext !== 'string' || !plaintext) {
      throw new TypeError('Provided "plaintext" must be a non-empty string');
    }

    // random initialization vector
    const iv = crypto.randomBytes(12);

    var sha256 = crypto.createHash('sha256');
    sha256.update(masterkey);

    // AES 256 GCM Mode
    const cipher = crypto.createCipheriv(CIPHER_ALGORITHM, masterkey, iv);

    // encrypt the given text
    const encrypted = Buffer.concat([cipher.update(plaintext, 'utf8'), cipher.final()]);

    // extract the auth tag
    const tag = cipher.getAuthTag();

    // generate output
    return Buffer.concat([iv, encrypted, tag]).toString('base64');
  },

  decrypt: function (masterkey, encryptedTxt) {

    if (typeof masterkey !== 'string' || !masterkey) {
      throw new TypeError('Provided "key" must be a non-empty string');
    }
    if (typeof encryptedTxt !== 'string' || !encryptedTxt) {
      throw new TypeError('Provided "encryptedTxt" must be a non-empty string');
    }

    // base64 decoding
    const bData = Buffer.from(encryptedTxt, 'base64');

    // convert data to buffers
    /*
       const iv = bData.slice(0, 12);
         const tag = bData.slice(12, 28);
         const text = bData.slice(28);
    */

    const iv = bData.slice(0, 12);
    const text = bData.slice(12, bData.length - 16);
    const tag = bData.slice(bData.length - 16);

    var sha256 = crypto.createHash('sha256');
    sha256.update(masterkey);

    // AES 256 GCM Mode
    const decipher = crypto.createDecipheriv(CIPHER_ALGORITHM, masterkey, iv);
    decipher.setAuthTag(tag);

    // encrypt the given text
    const decrypted = decipher.update(text, 'binary', 'utf8') + decipher.final('utf8');

    try {
      //control de que si viene un número no hacemos un json parse porque da errores
      if (!isNaN(decrypted)) {
        return decrypted
      }
      return JSON.parse(decrypted);
    }
    catch {
      return decrypted;
    }
  },

  hash256: function (plaintext) {
    var hash = crypto.createHash('sha256');
    let data = hash.update(plaintext, 'utf-8');
  
    return data.digest('hex');
  }
};

module.exports = aes256;