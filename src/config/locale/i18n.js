
export default function i18n(forceLang) {

  const queryString = window.location.search;
  const urlParams = new URLSearchParams(queryString);

  let lang = "es";

  if (forceLang) {
    lang = forceLang;
    sessionStorage.setItem("language", lang);
  } else if (urlParams.get("language") !== null) {
    lang = urlParams.get("language");
  } else if (sessionStorage.getItem("language") !== null) {
    lang = sessionStorage.getItem("language");
  } else {
    lang = "es";
  }

  let translations = require(`./${lang}.json`);
  return translations;
}