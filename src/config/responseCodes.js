export let  responseCodesMap = new Map([
    ['CARD_REGISTRATION_ERRORS',
        new Map([
            ['PAYMENT_METHOD_INCORRECT', { description: 'error_description' }],
            ['PAYMENT_METHOD_REJECTED', { description: 'error_description' }],
            ['REGISTER_OK_PAYMENT_METHOD_KO', { description: 'error_description'}]
        ])],
    ['COMMON_ERRORS',
    new Map([
        ['PARAMETERS_INCORRECT', { description: 'error_description' }],
        ['BAD_REQUEST', { description: 'error_description' }],
        ['UNKNOWN_REGISTER_USER_ERROR', { description: 'error_description'}],
        ['UNKNOWN_BASE_ERROR', { description: 'error_description'}],
        ['PROMOTIONAL_CODE_ERROR', { description: 'error_description'}]        
    ])]]);