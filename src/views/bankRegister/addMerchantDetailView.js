import locale from "../../config/locale/i18n";
import addMerchantView from "../common/addMerchantView";
import registerMerchantView from "../common/addRegisterMerchantView";
import { hideSpinner, openModal, showSpinner } from "../../common/sdk.common";

let sdkButtonType = 'simple';
let context, i18n;
export default function addDetailMerchantView(ctx, merchant) {
  // Create HTML parent view

  context = ctx;
  let { settings: { language }} = ctx;
  i18n = locale(language);

  context.merchant = merchant;

  createViewInHtml(merchant);
  // Listeners configuration
  setEventListeners(merchant);

  hideSpinner();
}

function createViewInHtml(merchant) {

  let container = document.querySelector('.cd-sdk-carddynamics .cd-sdk-enrollment');
    container.outerHTML = `
        <div class="cd-sdk-enrollment">
          <!--header-->
          <header role="banner">
              <div class="cd-sdk-merchant-header-container">
                  <div class="cd-sdk-merchant-header-brand-container">
                      <img src="${merchant.merchantDetails.merchantLogo}" alt="">
                      <span>${merchant.merchantDetails.merchantBrand}</span>
                  </div>
              </div>
          </header>

          <!--main-->
          <div role="main" class="cd-sdk-merchant-details">
    
              <div class="cd-sdk-merchant-details-intro">
                  ${merchant.merchantDetails.merchantDescription}
              </div>
              
              <!-- promo -->
              ${paintPromotionDetail(merchant.promoSelected)}
              <!-- END OF promo --> 
              
              <div class="cd-sdk-${sdkButtonType}-btns initial">
                  <button type="button" name="btn-back" id="btn-back">${i18n.button.back}</button>
                  <button type="button" name="btn-continue" id="btn-continue" class="sdk-btn-main">${i18n.button.register}</button>
              </div>
            </div>
          </div>
        </div>`;
}

function paintPromotionDetail(promoDetail) {
  const i18n = locale();
  let targetSelf = null;
  if (promoDetail) {
    const url = validateUrl(promoDetail.offerTerms);
    let html = '<div class="cd-sdk-merchant-details-available">';
    html += `<div class="cd-sdk-merchant-details-available-header">
    <h3 class="detail-available-title">${i18n.merchantDetail.campaign.title}</h3>`;
    if (promoDetail.validDate) {
      html += `<span class="cd-sdk-merchant-details-available-date">${i18n.merchantDetail.valid.title} ${promoDetail.validDate}</span>`;
    }
    html += `
      </div>
      <div class="cd-sdk-merchant-details-available-content">${promoDetail.offerDetails}</div>
      <!--terms-->
      <a href="${url}" ${targetSelf ? 'target="_self"' : 'target="_blank"' } class="cd-sdk-merchant-details-terms">
          <span class="detail-link">${i18n.merchantDetail.terms.title}</span>
          <span class="icon-arrow"></span>
      </a>
      <br/>
      <!--END OF terms-->`;
    html += '</div>';
    return html;
  }
  return '';
}

function setEventListeners(merchant) {
  const i18n = locale();
  // Back button go to home
  const backBtn = document.getElementById("btn-back");
  backBtn.addEventListener('click', () => {
    location.hash = "";
    addMerchantView(context);
  });
  // Click apply button
  const applyBtn = document.getElementById("btn-continue");
  applyBtn.addEventListener('click', () => {

    let { paymentMethods } = context;
    if (paymentMethods) {
      showSpinner(i18n.spinner.loading);
      registerMerchantView(context, '');
    } else {

        openModal({
            title: i18n.merchantDetail.error.title,
            text: i18n.merchantDetail.error.description,
        },
        'error');
    }
  });
}

function validateUrl(url) {
  if (url !== '' && url.indexOf("http://") === -1 && url.indexOf("https://") === -1) {
    url = 'https://' + url;
  }
  return url;
}