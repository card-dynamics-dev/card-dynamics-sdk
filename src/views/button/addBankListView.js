import locale from "../../config/locale/i18n";
import addBankDetailsView from "./addBankDetailsView";
import { hideSpinner, openModal, showSpinner } from "../../common/sdk.common";
import SdkService from '../../services/sdk.service';
import SdkException from "../../common/sdk-exception";
import BankService from "../../services/bank.service";
import { encryptObject } from "../../helpers/crypto.helper";
import { startAggregation } from "../../actions/adapter.service";

let i18n, context, bankService;
export default async function addBankListView(ctx) {

  try {

    /*window.addEventListener('beforeunload', function (e) {
      return e.returnValue = '';
    }, { capture: true });*/

    context = ctx;
    let { settings, settings: { cdMerchantId, language, test }} = ctx;
    
    i18n = locale(language);

    let sdkService = new SdkService();

    let result = await sdkService.getIssuerSKey();

    if (result.status == 200) {
      let data = Buffer.from(result.data, 'base64').toString('utf-8');
      try {
        Object.assign(context.settings, JSON.parse(data));
      } catch {
        throw new SdkException('settingError')
      }
    }
    else {
      throw new SdkException('settingError')
    }

    bankService = new BankService(settings);
    let promises = [bankService.getBankList()];

    if (cdMerchantId) {
      promises.push(bankService.getBankAdapterSettingsByVisibility(cdMerchantId));
    }

    // Get adapters
    Promise.all(promises).then(result => {

      let adapters = (result[0].data.adapters instanceof Array) ? result[0].data.adapters : [];

      if (result && result.length === 2) {
        let visibleAdapters = (result[1].data instanceof Array) ? result[1].data : [];
        adapters = adapters.filter(x => visibleAdapters.some(i => i.adapter_code === x.adapter_code));
      }
      
      let dummyEnvironments = ['dev', 'pre'];

      if (dummyEnvironments.includes(process.env.BUILD_ENV)) {
        if (!test) {
          return adapters.filter(x => x.running === true && x.adapter_status == "ok");
        }
        return adapters.filter(x => x.running === true);
      } else {
        return adapters.filter(x => x.running === true && x.adapter_status == "ok" && !['BNKDUMES', 'BNKDUMMX'].some(i => i === x.adapter_code));
      }

    })
      .then(adapters => createViewInHtml(adapters))
      .catch(err => {

        hideSpinner();
        throw new SdkException('adaptersError')

      });

    Object.assign(context, { bankService, loginDone: false, aggregation_id: null });

  } catch (err) {
    hideSpinner();
    openModal({ text: 'Vaya, algo salió mal al recuperar los settings, inténtalo de nuevo.'}, 'error');
    console.log(err);
  }

}

function createViewInHtml(adapters) {

  let listItems = "";

  adapters.forEach((adapter, index) => {


    listItems += `<div adapter-code="${adapter.adapter_code}" class="cd-adapter-item">
        <img  class="sdk-img-placeholder" src="${process.env.BANK_ADAPTER_BASE_URL}${adapter.logo}">
        <div class="cd-adapter-name">${adapter.adapter_name}</div>
      </div>`;
    })

  const html = `
         
        <div class="cd-adapters-container">
          <div class="cd-sdk-container-header">
            <div class="cd-sdk-container-header-title">${i18n.enrollmentButton.bankList.SelectBank}</div>
          </div>

            ${listItems}
        </div>`;


  let container = document.getElementById('cd-sdk-container');
  container.innerHTML = html;
  hideSpinner();

  setEventListeners(adapters);
}

function setEventListeners(adapters) {
  adapters.forEach(adapter => {

    let el = document.querySelector(`div[adapter-code=${adapter.adapter_code}]`);
    el.addEventListener("click", function (e) {
      e.preventDefault();
      showSpinner(i18n.spinner.loading);
      let code = e.currentTarget.getAttribute('adapter-code');

      if(adapter.auto_run){
        try{
          startAggregation(context, code, {}, true);
          addBankDetailsView(context, code, true);
        }
        catch(_){
          hideSpinner();
          openModal({ text: i18n.error.default }, 'error');
        }
      }
      else{
        addBankDetailsView(context, code);
      }
    });
  });
}

function loader(style) {
  const loader = document.getElementById("loader");
  loader.style.display = style;
}

function showModalError(message) {
  //loader('none');
  createModal({
    title: '',
    body: message,
    btn: 1,
    eventAction: 'Merchant_',
    eventCategory: 'Error_registro_pago',
    eventLabel: 'Aceptar'
  });
}