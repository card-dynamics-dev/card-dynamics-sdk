import { hideSpinner, loadFont, openModal, showSpinner } from "../../common/sdk.common";
import locale from "../../config/locale/i18n";
import addBankListView from "./addBankListView";

import '../../assets/style/common.scss';
import '../../assets/style/common.responsive.scss';
import '../../assets/style/button.scss'
import '../../assets/style/button.responsive.scss'

let i18n, context;
export default function addButtonView(ctx, options, callback) {
    try {

        context = ctx;
        let { settings: { language }} = ctx;

        i18n = locale(language);

        createViewInHtml(options);
        addEventListeners(callback);

    } catch (error) {
        console.log(error);
    }
}

function createViewInHtml(options) {

    let buttonContainer = document.getElementById('cd-sdk-button-container');

    let style = "cursor:pointer; white-space: nowrap; text-transform: none; padding:2px; display: flex; justify-content: center; align-items: center; box-shadow: rgba(0, 0, 0, 0.2) 0px 2px 1px -1px, rgba(0, 0, 0, 0.14) 0px 1px 1px 0px, rgba(0, 0, 0, 0.12) 0px 1px 3px 0px; text-decoration: none; font-family: Arial, san-serif; filter: brightness(1);"
    let logo = "";
    let logowidth = "60px";
    let message = "";

    if (options.type == "oval") {
        style += "border-radius: 10px;";
    } else {
        style += "border-radius: 0px;"
    }

    //Size
    if (options.height) {
        style += "height:" + options.height + ";";
    } else {
        style += "height: 48px;";
    }

    if (options.width) {
        style += "width:" + options.width + ";";
    } else {
        style += "width: 100%;";
    }

    //theme
    if(options.theme == "MIT"){
        style += "color: white; background: #1d3c7f; border:none; font-size: 16px; display:inline;"
        message = `${i18n.enrollmentButton.messages.MIT} <img style="height: 18px; width: auto;" src="../assets/img/mit.png" />`;
    }
    else{
        if (options.theme == "green") {
            style += "background: rgb(118, 185, 0); color: rgb(255, 255, 255); border: none;";
            logo = "white"
        } else if (options.theme == "grey") {
            style += "background: rgb(103, 101, 101); color: rgb(255, 255, 255); border: none;";
            logo = "white";
        } else {
            style += "background: rgb(255, 255, 255); color: rgb(108, 106, 106); border:solid 1px #807F7F;";
            logo = "green";
        }

        logo = `<img style="width: ${logowidth}; padding: 5px; max-width: 60px; max-height:44px" src="https://cd-logos.s3.amazonaws.com/cd_${logo}_button.png"></img>`;

        //message
        if (options.messagetype && options.messagetype === "REG") {
            message = i18n.enrollmentButton.messages.REG;
        } else if (options.messagetype && options.messagetype === "IMP") {
            message = i18n.enrollmentButton.messages.IMP;
        } else if (options.messagetype && options.messagetype === "USE") {
            message = i18n.enrollmentButton.messages.USE;
        } else {
            message = i18n.enrollmentButton.messages.REG;
        }
    }

    let slogan = "";
    if (options.slogan === true) {
        slogan = `<span style="font-size:10px">${i18n.enrollmentButton.slogan}</span>`;
    }

    let { settings: { overlay, cssOverride, loader }} = context;

    let fontUrl = 'https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800';
    if(cssOverride && cssOverride.fontUrl){
        fontUrl = cssOverride.fontUrl;
    }

    loadFont(fontUrl);

    let cssStyle = "";
    if (cssOverride){
    
        cssStyle = "<style>";
      if(cssOverride.colors && Object.keys(cssOverride.colors).length > 0 ) {
        let colorValues = "";

        if(cssOverride.colors.primary && !cssOverride.colors.accent){
            cssOverride.colors.accent = cssOverride.colors.primary;
        }

        for (const [key, value] of Object.entries(cssOverride.colors)) {
          colorValues += ` --${key}: ${value};`;
        }
  
        cssStyle += `:root { ${ colorValues }}`;
      }
  
      if(cssOverride.style){
        cssStyle += style;
      }

      cssStyle += "</style>";
    }

    let mobileLoader = "assets/img/loading-mobile.gif";
    let webLoader = "assets/img/loading-web.gif";
    let loaderClasses = "";
    let textVisibilityStyle = "";
    if(loader){
        if(loader.mobile){
            mobileLoader = loader.mobile;
        }
        if(loader.web){
            webLoader = loader.web;
        }

        if(loader.textVisibility === false){
            textVisibilityStyle = `style="display: none;"`;
        }
      
        if(loader.box === false){
            loaderClasses = 'cd-sdk-spinner-without-box';
        }
    }

    buttonContainer.innerHTML = `
      ${cssStyle}
      
      <button id="cd-sdk-button" type="button" style="${style}">
        ${logo} 
        ${message}
      </button>
      ${slogan}`;

    let { settings: { demo }} = context;
    let demoClass = demo ? `class="cd-sdk-demo"` : "";
    document.body.innerHTML += `<div id="cd-sdk-overlay" class="cd-sdk-overlay-${overlay ? overlay : 'blur'} cd-sdk-carddynamics">
      <div id="cd-sdk-modal" ${demoClass}>
          <span class="cd-sdk-btn-close">
            <button><span class="icon-close"></span></button>
          </span>
          <div id="cd-sdk-container"></div>
    
          <div style="display: flex;" class="cd-sdk-spinner ${loaderClasses}">
            <div class="cd-sdk-spinner-container">
                <img class="mobile" src="${mobileLoader}" />
                <img class="web" src="${webLoader}" />
                <div ${textVisibilityStyle}>${i18n.spinner.loading}</div>
            </div>
          </div>

          <div style="display: none" class="cd-sdk-popup cd-sdk-alerts">
            <div class="cd-sdk-popup-container">
                <span class="cd-sdk-popup-icon">
                </span>
                <div class="cd-sdk-popup-text"></div>
                <div class="cd-sdk-simple-btns">
                    <button type="button">Cancelar</button>
                    <button type="button">Aceptar</button>
                </div>
            </div>
          </div>
      </div>
    </div>`;


}

function addEventListeners(callback){

    let cdSdkButton = document.getElementById("cd-sdk-button");
    if (cdSdkButton) {
        cdSdkButton.addEventListener('click', function () {

            showSpinner(i18n.spinner.loading);

            let cdSdkOverlay = document.getElementById('cd-sdk-overlay');
            cdSdkOverlay.style.display = "flex";

            let cdSdkContainer = document.getElementById('cd-sdk-container');
            cdSdkContainer.innerHTML = "";

            document.addEventListener('sdkcallback', function (e) {
                let { detail: { result,  close }} = e;
                callback(result);
                if(close) sdkClose();
            }, { once: true });

            addBankListView(context);

        });
    }

    let cdSdkBtnClose = document.querySelector('span.cd-sdk-btn-close>button');
    if (cdSdkBtnClose) {
        cdSdkBtnClose.addEventListener("click", async () => {

            openModal({ 
                text: i18n.enrollment.text.abandon, 
                okLabel: i18n.button.abandon,
                okFn: () => {
                    callback({ status: 'closed' });
                    hideSpinner();
                    sdkClose();
                },
                koLabel: i18n.button.close
            },
            'warning', 'confirm');
            
        });
    }
}

function sdkClose() {
    let sdkOverlay = document.getElementById('cd-sdk-overlay');
    if (sdkOverlay) sdkOverlay.style.display = "none";
}