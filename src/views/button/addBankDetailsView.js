import locale from "../../config/locale/i18n";
import { hideSpinner, openModal, showSpinner } from "../../common/sdk.common";
import { decryptString, encryptObject } from "../../helpers/crypto.helper";
import { getFormValues, isValidForm, isValidInput, paintFormInputs } from "../../common/form-builder.common";
import addBankListView from "./addBankListView";
import { buildDateFromTimestamp, removeSpace } from "../../helpers/generic.helper";
import addMerchantView from "../common/addMerchantView";
import addBankVerificationView from "./addBankVerificationView";
import { aggregationResume, startAggregation } from "../../actions/adapter.service";
import addRegisterPasswordView from "../common/addRegisterPasswordView";

let fieldsInError;

let i18n, context, autorun;
export default function addBankDetailsView(ctx, adapter, _autorun) {
  try {

    context = ctx;
    let { settings, settings: { language } } = ctx;
    i18n = locale(language);

    autorun = _autorun;
    document.addEventListener('autorun', function (e) {
      let { detail } = e;
      if(detail && detail.aggregation_id) context.aggregation_id = detail.aggregation_id;
    }, { once: true });

    // Get Bank details & adapter settings
    Promise.all([context.bankService.getBankDetails(adapter), context.bankService.getBankAdapterSettings(adapter)]).then(result => {

      let { adapter_info: adapterInfo } = result[0].data;
      let othersInfo = result[1].data;


      if (othersInfo && othersInfo.issuer) {
        let issuer = Buffer.from(othersInfo.issuer, 'base64').toString('utf-8');
        try { issuer = JSON.parse(issuer); } catch { issuer = {} }
        Object.assign(settings, issuer);
      }

      Object.assign(context, { settings, adapterInfo, othersInfo });

      //build view
      let inputs = (adapterInfo && adapterInfo.parameters) ? adapterInfo.parameters : [];

      createViewInHtml(inputs, adapter);
      setEventListeners(inputs, adapter);

    })
    .catch(err => {
        console.log(err);
        hideSpinner();
        openModal({ text: 'Vaya, algo salió mal al recuperar los settings, inténtalo de nuevo.'}, 'error');

        
      });
  } catch (error) {
    hideSpinner();
    console.log(error)
    openModal({ text: 'Vaya, algo salió mal, inténtalo de nuevo.'}, 'error');

  }
}

function createViewInHtml(inputs, adapter) {

  let html = `
    <div class="cd-sdk-login-container">
      <!--Form Tab-->
      ${paintForm(adapter, inputs, fieldsInError)}
      <!--END OF Form Tab-->

      <!--Helper Tab-->
      <div class="cd-sdk-login-help">
        <div>
          <img src="assets/img/info.png" />
          <span>${i18n.enrollmentButton.helperTab.message}</span>
          </div>
        <div class="links">
          <a id="btn-help" class="btn-link link-helper">${i18n.enrollmentButton.helperTab.accessKeys}</a>
          <a id="btn-security-guarantee" class="btn-link link-helper">${i18n.enrollmentButton.helperTab.securityGuarantee}</a>
        </div>
      </div>
      <!--End of Helper Tab-->
  </div>

  <div style="display:none" class="cd-sdk-helper-container">
      <div class="cd-sdk-nav-container">
          <a id="btn-back-helper" class="cd-sdk-nav-btn icon-back">${i18n.button.back}</a>
      </div>

      <section class="main"></section>
    </div>
  `;

  let container = document.getElementById('cd-sdk-container');
  if (container) {
    container.scroll({ top: 0 });
    container.innerHTML = html;
    hideSpinner();
  }
}

function setEventListeners(inputs, adapter) {
  // Back button click
  const backButton = document.getElementById('btn-back');
  backButton.addEventListener("click", () => {
    showSpinner(i18n.spinner.loading);
    addBankListView(context);
  });

  // Form validation when some value change
  const form = document.getElementById(adapter);
  const loginBtn = document.getElementById('btn-login');
  loginBtn.disabled = !isValidForm(inputs, null, false);

  form.addEventListener("change", () => loginBtn.disabled = !isValidForm(inputs, fieldsInError, fieldsInError != null));

  // Input validation when value change
  inputs.forEach(input => {
    const inputEl = document.getElementById(input.name.trim());
    inputEl.addEventListener("keyup", () => {
      isValidInput(input, fieldsInError);
      let loginBtn = document.getElementById('btn-login');
      loginBtn.disabled = !isValidForm(inputs, fieldsInError, fieldsInError != null);
    });

    inputEl.addEventListener("blur", () => {
      isValidInput(input, fieldsInError);
    })
  });

  loginBtn.addEventListener("click", () => {
   
    let values = getFormValues(inputs);
    fieldsInError = null;

    let messages = [i18n.spinner.connectingBank, i18n.spinner.verifyingUser, i18n.spinner.waitingConfirmation];
    showSpinner(messages);

    document.addEventListener('login-done', function (e) {
      addMerchantView(context);
    }, { once: true });

    if(autorun){
      if(context.aggregation_id){
        aggregationResume(context, adapter, values, context.aggregation_id);
      }else{
        let interval = setInterval(function(){
          if(context.aggregation_id){
            aggregationResume(context, adapter, values, context.aggregation_id);
            clearInterval(interval);
          }
        }, 500);
      }
    }
    else {
      startAggregation(context, adapter, values);
    }
  });

  const helpersLinks = document.querySelectorAll('.btn-link.link-helper');
  helpersLinks.forEach(link => {
    link.addEventListener("click", (e) => {

      let { id } = e.currentTarget;

      let container = document.querySelector('div.cd-sdk-helper-container');
      container.style.display = 'flex';

      let main = document.querySelector('div.cd-sdk-helper-container section.main');
      main.innerHTML = paintHelperByType(id);
    });
  });

  const BackHelperBtn = document.getElementById('btn-back-helper');

  BackHelperBtn.addEventListener("click", () => {

    let container = document.querySelector('div.cd-sdk-helper-container');
    container.style.display = 'none';
  });

  // Toggle password for every input
  const passwordBtns = document.querySelectorAll('.icon-pass');
  passwordBtns.forEach(pass => {
    pass.addEventListener("click", (e) => {
      const inputAssociated = document.getElementById(pass.getAttribute('data-input-id'));
      if (inputAssociated.type === 'text') {
        inputAssociated.type = 'password';
        pass.classList.add('icon-show');
        pass.classList.remove('icon-hide');
      } else {
        inputAssociated.type = 'text';
        pass.classList.remove('icon-show');
        pass.classList.add('icon-hide');
      }
    });
  });
}

function paintForm(adapter, inputs, fieldsInError) {

  let { settings: { demo, sdkButtonType }} = context;
  return `
  <form id=${adapter} novalidate>
    <div class="cd-sdk-form-header">
      <img src="${process.env.BANK_ADAPTER_BASE_URL}/api/adapter/${adapter}/logo.png">
      <h4>${i18n.enrollmentButton.loginForm.title}</h4>
      <div ${ !demo ? `style="display: none;"`: "" } class="demo">${i18n.enrollment.text.demo}</div>
    </div>
      
    ${paintFormInputs(inputs, fieldsInError)} 
    
    <div class="cd-sdk-privacy-row">
      ${i18n.bankDetailForm.privacyAndTerms.text} <a id="btn-terms" class="btn-link link-helper">${i18n.bankDetailForm.privacyAndTerms.terms.title}</a> ${i18n.bankDetailForm.privacyAndTerms.and} <a  id="btn-privacy" class="btn-link link-helper">${i18n.bankDetailForm.privacyAndTerms.privacy.title}</a>
    </div>

    <div class="cd-sdk-${sdkButtonType}-btns initial">
      <button type="button" name="btn-back" id="btn-back">${i18n.button.back}</button>
      <button type="button" name="btn-login" id="btn-login" class="sdk-btn-main">${i18n.button.login}</button>
    </div>

  </form>

  <!--END OF form-->`;
}

function paintHelperByType(type) {

  let { settings: { country } } = context;
  if (country) country = country.toLowerCase();

  switch (type) {
    case 'btn-privacy':
      return `${i18n.enrollmentButton.texts.privacy['privacy_text_' + country]}`;
    case 'btn-terms':
      return `${i18n.enrollmentButton.texts.terms['terms_text_' + country]}`;
    case 'btn-help':
      return `${i18n.enrollmentButton.texts.service_description['service_description_text_' + country]}`;
    case 'btn-security-guarantee':
      return `${i18n.enrollmentButton.texts.security_guarantee['security_guarantee_text_' + country]}`;
  }
}

