import locale from "../../config/locale/i18n";
import { hideSpinner, openModal, showSpinner } from "../../common/sdk.common";
import { getFormValues, isValidForm, isValidInput, paintFormInputs } from "../../common/form-builder.common";
import { decryptString, encryptObject } from "../../helpers/crypto.helper";
import addBankListView from "./addBankListView";
import { aggregationResume } from "../../actions/adapter.service";
import addMerchantView from "../common/addMerchantView";

let fieldsInError, context, i18n;

export default function addBankVerificationView(ctx) {

    let { settings: { language } } = ctx;

    i18n = locale(language);
    context = ctx;

    // Create HTML view
    createViewInHtml();
    // Listeners configuration
    setEventListeners();
}

function createViewInHtml() {

    let { settings: { sdkButtonType }, adapterInfo: { adapter_code, logo }, adapterVerification: { parameters } } = context;

    let html = `
    <form id="${adapter_code}" novalidate>
        <div class="cd-sdk-form-header">
            <img src="${process.env.BANK_ADAPTER_BASE_URL}${logo}">
            <h4>${i18n.verification.title}</h4>
        </div>
            
        ${paintFormInputs(parameters, fieldsInError)} 

        <div class="cd-sdk-${sdkButtonType}-btns initial">
            <button type="button" name="btn-cancel" id="btn-cancel">${i18n.button.cancel}</button>
            <button type="button" name="btn-verify" id="btn-verify" class="sdk-btn-main" disabled>${i18n.button.verify}</button>
        </div>
    </form>
  `;

    let container = document.querySelector(`#cd-sdk-container .cd-sdk-login-container form[id=${adapter_code}`);
    if (container) {
        container.scroll({ top: 0 });
        container.outerHTML = html;
        hideSpinner();
    }
}

function setEventListeners() {

    let { adapterVerification: { parameters: inputs }, adapterInfo: { adapter_code } } = context;
    // Back button go to home
    const backBtn = document.getElementById('btn-cancel');
    backBtn.addEventListener("click", () => {
        showSpinner(i18n.spinner.loading);
        addBankListView(context);
    });

    // Apply buttons listeners
    const verifyBtn = document.getElementById('btn-verify');
    verifyBtn.disabled = !isValidForm(inputs, '', false);
    verifyBtn.addEventListener("click", () => {
        let values = getFormValues(inputs);
        verify(values);
    });

    // Form validation when some value change
    const verificationForm = document.getElementById(adapter_code);
    verificationForm.addEventListener("change", () => verifyBtn.disabled = !isValidForm(inputs, fieldsInError, false));

    // Input validation when value change
    inputs.forEach(input => {
        const inputEl = document.getElementById(input.name.trim());
        inputEl.addEventListener("keyup", () => {
            isValidInput(input, fieldsInError);
            verifyBtn.disabled = !isValidForm(inputs, fieldsInError, fieldsInError != null);
        });

        inputEl.addEventListener("blur", () => {
            isValidInput(input, fieldsInError);
        })
    });
}

function verify(values) {

    showSpinner(i18n.spinner.loading);

    let { adapterVerification: { aggregation_id }, adapterInfo: { adapter_code } } = context;

    document.addEventListener('login-done', function (e) {
        addMerchantView(context);
      }, { once: true });

    aggregationResume(context, adapter_code, values, aggregation_id);

}