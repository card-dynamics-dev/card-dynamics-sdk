import locale from "../../config/locale/i18n";
import addMerchantView from './addMerchantView';
import { convertString } from "../../helpers/generic.helper";
import { hideSpinner, openModal, openCongratsModal, showSpinner } from "../../common/sdk.common";
import addBankListView from '../button/addBankListView';
import { responseCodesMap } from "../../config/responseCodes";
import EnrollmentService from "../../services/enrollment.service";


// Variables
let merchantData = null;
let enrollmentRequestData = null;

let context, i18n, enrollmentService;
export default function addVerificationView(ctx, merchant, enrollmentRequestForm, data) {

    merchantData = merchant;
    enrollmentRequestData = enrollmentRequestForm;

    let { settings, settings: { language } } = ctx;
    i18n = locale(language);

    context = ctx;
    enrollmentService = new EnrollmentService(settings);

    // Create HTML view
    createViewInHtml(merchant, data);
    // Listeners configuration
    setEventListeners(merchant, data);

    hideSpinner();

    if (data.message !== '') {
        console.log({
            title: '',
            body: convertString(data.message),
            btn: 1
        });
    }
    /*} catch (error) {
      window.location = '/';
    }*/
}

function createViewInHtml(merchant, data) {

    let validationType = data.validationType;

    let title = "", formHtml = "";
    if (validationType === 'CODE') {
        title = i18n.verification.code.title;
        formHtml = `
        <div class="cd-sdk-form-row-account">
            <div class="cd-sdk-form-row-account-container cd-sdk-input-outline" id="txt-code-input-cont">
                <label for="txt-code" class="cd-sdk-form-label">${i18n.verification.code.title}</label>
                <input type="text" id="txt-code" name="txt-code" class="cd-sdk-form-txt">
            </div>
            <span class="cd-sdk-txt-info" id="tooltip-code">${data.message}</span>
        </div>`;

    } else if (data.validationType === 'CONFIRMATION') {
        title = i18n.verification.email.title;
        formHtml = `
        <div class="cd-sdk-checkbox-simple">
              <input type="checkbox" id="ch-verification" name="ch-verification">
              <label for="ch-verification" class="checkbox-label">${i18n.verification.check.title}</label>
          </div>
        `;
    }

    let html = ` 
    <div class="cd-sdk-payment-methods-container">
        <div class="cd-sdk-container-header">
            <div class="cd-sdk-container-header-title">${title}</div>
        </div>

        <form id="verification-form">
        ${formHtml}
        <div class="cd-sdk-simple-btns initial">
            <button type="button" name="btn-cancel" id="btn-cancel">${i18n.button.cancel}</button>
            <button type="button" name="btn-verify" id="btn-verify" class="sdk-btn-main" disabled>${i18n.button.verify}</button>
        </div>
        </form>
    </div>`;

    let container = document.getElementById('cd-sdk-container');
    if (container) {
        container.scroll({ top: 0 });
        container.innerHTML = html;
        hideSpinner();
    }
    else {
        let container = document.querySelector('.cd-sdk-carddynamics .cd-sdk-enrollment div[role="main"].cd-sdk-payment-methods .cd-sdk-payment-methods-container');
        container.outerHTML = html;
    }
}

function setEventListeners(merchant, data) {
    // Back
    const backBtn = document.getElementById('btn-cancel');
    backBtn?.addEventListener("click", () => {    
        openModal({ 
            text: i18n.enrollment.text.abandon, 
            okLabel: i18n.button.abandon,
            okFn: () => {

                let { button } = context;
                if(!button){
                    location.hash = "";
                    addMerchantView(context);
                }
                else{
                    addBankListView(context);
                }
            },
            koLabel: i18n.button.close
        }, 'warning', 'confirm');
    });

    // Verify
    const verifyBtn = document.getElementById('btn-verify');
    verifyBtn?.addEventListener("click", () => verify(merchant, data));

    // Form validation when some value change
    const submitForm = document.getElementById('verification-form');
    submitForm.addEventListener("change", () => {
        verifyBtn.disabled = !isValidForm(false);
    });

    const inputCode = document.getElementById('txt-code');
    if (inputCode) {
        inputCode.addEventListener("keyup", () => {
            verifyBtn.disabled = !isValidForm(false);
        });
    }
}

function isValidForm(showInputError) {

    //Confirmation
    let verificationCheck = document.getElementById('ch-verification');
    if (verificationCheck && !verificationCheck.checked) return false;

    //Code
    const input = document.getElementById('txt-code');
    if (input) {
        const isValid = isValidInput(input, showInputError);
        return (isValid === true);
    }
    return true;
}

function isValidInput(input, showInputError = true) {
    const parent = document.getElementById(`${input.inputName}-input-cont`);
    const tooltipLabel = document.getElementById(`tooltip-${input.inputName}`);
    const errorLabel = document.getElementById(`error-txt-${input.inputName}`);
    if (input.value === '') {
        if (showInputError) parent.classList.add('form-error');
        if (showInputError && tooltipLabel) tooltipLabel.classList.add('tooltip-error');
        return false;
    }
    return true;
}

function verify(merchant, resultData) {

    showSpinner(i18n.spinner.loading);

    let { merchantService } = context;

    let message = i18n.error.default;

    const inputCode = document.getElementById('txt-code');

    const verifyBtn = document.getElementById('btn-verify');
    verifyBtn.disabled = true;

    const confirmationData = {
        "enrollmentId": resultData.enrollmentId,
        "validationType": resultData.validationType
    }
    if (resultData.validationType === 'CODE') {
        confirmationData.code = inputCode.value;
    }

    merchantService.enrollmentConfirmation(confirmationData).then(response => {
        let message = '';
        verifyBtn.disabled = false;
        response.json().then(data => {

            data.enrollmentId = resultData.enrollmentId;

            if (data.status !== 'PROCESSING') {
                try {
                    sendResult(false, 'CONFIRMATION', data);
                } catch (err) {
                    console.log('error sendResult', err);
                }
            }

            if (data.status === 'PROCESSING') {
                let counter = 0;

                let interval = setInterval(function () {
                    counter++;
                    if (counter > 10) {
                        clearInterval(interval);
                        hideSpinner();
                        openModal({ text: 'Lo sentimos. El comercio no está disponible en estos momentos. Intentelo de nuevo en unos minutos' }, 'error');
                    } else {
                        checkStatusEnrollmentConfirmation(enrollmentRequestData, data, merchant, counter, interval);
                    };
                }, 4000);
                return

            } else if (data.status === 'PENDING_VALIDATION') {

                addVerificationView(context, merchant, enrollmentRequestData, data);

            } else if (data.status === 'FAILURE') {

                // Si hay Failure mostraremos el error genérico a no ser que se encuentre dentro de los card registration errors.

                if (responseCodesMap.get('CARD_REGISTRATION_ERRORS').get(data.responseCode)) {
                    openCongratsModal(data.message, 'warning');
                }
                else{         
                    if (data.message) message = data.message;
                    openModal({ text: data.message}, 'error');
                    verifyBtn.disabled = false;
                }

            } else if (data.status === 'SUCCESS') {
                openCongratsModal();
                hideSpinner();
            } else {

                if (data.message) message = data.message;
                openModal({ text: convertString(message)}, 'error');
                verifyBtn.disabled = false;
            }
        });

    }).catch((err) => {
        try {
            sendResult(true, 'CONFIRMATION', requestForm, {});
        } catch (err) {
            console.log('error sendResult', err);
        }
        if (err.message) message = err.message;
        openModal({ text: convertString(message)}, 'error');
        verifyBtn.disabled = false;
    });
}

function checkStatusEnrollmentConfirmation(enrollmentRequestForm, data, merchant, counter, interval) {
    MerchantService.checkStatusEnrollmentConfirmation({ enrollmentRequestId: data.enrollmentId }).then(resStatus => {
        resStatus.json().then(statusData => {
            if (statusData.status !== "PROCESSING") {

                try {
                    sendResult(false, 'CHECK_STATUS_CONFIRMATION', merchant, enrollmentRequestForm, statusData);
                } catch (err) {
                    console.log('error sendResult', err);
                }

                hideSpinner();
                clearInterval(interval);
            }

            if (statusData.status === "SUCCESS") {

                openCongratsModal();

            } else if (statusData.status === 'PENDING_VALIDATION') {

                verificationView(merchantData, enrollentRequestForm, statusData);

            } else if (statusData.status === 'FAILURE') {

                // Si hay Failure mostraremos el error genérico a no ser que se encuentre dentro de los card registration errors.

                if (responseCodesMap.get('CARD_REGISTRATION_ERRORS').get(statusData.responseCode)) {
                    return openCongratsModal(statusData.message, 'warning');
                    //openCongratsModalPaymentMethodKo(statusData.message);
                }
                else{
                    if (statusData.message) message = statusData.message;
                    openModal({ text: convertString(message)}, 'error');
                }
            }

        })
            .catch(error => {
                try {
                    sendResult(true, 'CHECK_STATUS_CONFIRMATION', requestForm, {});
                } catch (err) {
                    console.log('error sendResult', err);
                }

                hideSpinner();
                continueBtn.disabled = false;
                if (error.message) message = error.message;
                openModal({ text: convertString(message) }, 'error');
            });
    });
}

function sendResult(error = false, type, request, response) {

    let customerId = context?.settings?.customerId;
    let secret_key = context?.settings?.secret_key;
    let urlResponse = context?.othersInfo?.urlResponse;

    let data = {
        type,
        customerId,
    };

    data.cdMerchantId = request.cdMerchantId;
    data.merchantName = merchantData.merchantDetails.merchantBrand;

    let requestForm = decryptObjectValues(request, secret_key, ['accountData', 'cardData']);
    if (requestForm && requestForm.cardData) {
        data.pan = mask(requestForm.cardData.pan, '*', 4, 4, 6);
    }
    else if (requestForm && requestForm.accountData) {
        data.accountNumber = mask(requestForm.accountData.number, '*', 4, 4, 6);
    }

    let enrollmentId = response.enrollmentId ? response.enrollmentId : '00000000-0000-0000-0000-000000000000';

    if (error) {

        Object.assign(data, {
            response: {},
            status: 'ERROR'
        });

    } else {
        Object.assign(data, {
            response,
            status: response.status
        });
    }

    data.enrollmentId = enrollmentId;
    data = encryptObject(data, secret_key);



    return enrollmentService.sendResult(enrollmentId, {
        enrollmentData: data,
        url: urlResponse
    });

}