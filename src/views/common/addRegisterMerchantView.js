import locale from "../../config/locale/i18n";
import datepicker from 'js-datepicker';
import addMerchantView from './addMerchantView';
import { convertString, replaceI18nValue } from "../../helpers/generic.helper";
import { hideSpinner, showSpinner } from "../../common/sdk.common";
import addPaymentMethodView from "./addPaymentMethodView";

import '../../assets/style/datepicker.min.css';

let countriesProvinces = [];
let provinces = [];
let validMerchantId = false;

let context, i18n;
export default async function addRegisterMerchantView(ctx, fieldsInError = '') {

  try {
    let { settings: { language }, merchant } = ctx;
    i18n = locale(language);
    context = ctx;

    countriesProvinces = await getLocalJSON('../assets/data/countries-provinces.json');
    provinces = await getLocalJSON('../assets/data/provinces.json');

    // Create HTML view
    createViewInHtml(fieldsInError);
    if(fieldsInError && fieldsInError != ''){
      isValidForm(merchant, fieldsInError, true);
    }

    hideSpinner();
  } catch (error) {
    console.error(error);
  }
}


function createViewInHtml(fieldsInError) {

  let { merchant, button } = context;

  let step = button ? i18n.enrollmentSteps.twoOf3 : i18n.enrollmentSteps.oneOf2;

  let html = `
  <div class="cd-sdk-container-header">
    <div class="cd-sdk-container-header-title">${i18n.enrollmentForm.title}</div>
    <span class="cd-sdk-container-header-steps">${step}</span>
  </div>
  <div class="cd-sdk-register-container">
    ${paintForm(merchant, fieldsInError)}
  </div>`;


  let container = document.getElementById('cd-sdk-container');
  if (container) {
    container.scroll({ top: 0 });
    container.innerHTML = html;
    hideSpinner();
  }
  else {

    let container = document.querySelector('.cd-sdk-carddynamics .cd-sdk-enrollment');
    container.outerHTML = `
        <div class="cd-sdk-enrollment">
          <!--header-->
          <header role="banner">
              <div class="cd-sdk-merchant-header-container">
                  <div class="cd-sdk-merchant-header-brand-container">
                      <img src="${merchant.merchantDetails.merchantLogo}" alt="">
                      <span>${merchant.merchantDetails.merchantBrand}</span>
                  </div>
              </div>
          </header>
          <!--main-->
          <div role="main" class="cd-sdk-register">
            ${html}
          </div>
        </div>`;
  }

  // Listeners configuration
  setEventListeners(merchant, fieldsInError);
  location.hash = 'cdMerchantId=' + merchant.cdMerchantId;
}

function setEventListeners(merchant, fieldsInError) {
  // Back button click
  const backButton = document.getElementById('btn-back');
  if (backButton) {
    backButton.addEventListener("click", () => {
      location.hash = "";
      addMerchantView(context);
    });
  }

  // Form validation when some value change
  const submitForm = document.getElementById('dynamic-form');
  const continueBtn = document.getElementById('btn-continue');
  continueBtn.disabled = !isValidForm(merchant, fieldsInError, true);
  submitForm.addEventListener("change", () => continueBtn.disabled = !isValidForm(merchant, null, true));

  let checkboxs = document.querySelectorAll('div.cd-sdk-checkbox-simple input[type=checkbox]');
  for(let checkbox of checkboxs) {
      checkbox.addEventListener("change", () => continueBtn.disabled = !isValidForm(merchant, null, true));
  }

  //Asteriks inputs
  let asteriksInputs = document.querySelectorAll('input.cd-sdk-form-txt.cd-sdk-txt-asteriks');
  for(let asteriksInput of asteriksInputs){
    asteriksInput?.addEventListener("input", async () => {
      continueBtn.disabled = !isValidForm(merchant, null, true);
      isValidInput({ inputName: asteriksInput.name.substr(0, asteriksInput.name.length - 11)}, null, true, asteriksInput.id);
    });

    asteriksInput?.addEventListener("focus", async () => {
      if(asteriksInput.value.indexOf('*') >= 0){
        asteriksInput.value = "";
      }
    });
  }

  
  setTimeout(() => {

    /** Autocomplete missing values */
    let postalcodeInput = merchant.form.find(x => x.issuerFieldsName === 'address-postal-code');
    let addressInput = merchant.form.find(x => x.issuerFieldsName === 'address-street');
    let cityInput = merchant.form.find(x => x.issuerFieldsName === 'address-city');

    if (postalcodeInput && !postalcodeInput.value) {
      let criteria = "";
      if (addressInput) criteria += document.getElementById(addressInput.inputName.trim()).value;
      if (cityInput) criteria += ` ${document.getElementById(cityInput.inputName.trim()).value}`;

      getPlaceDetails({address: criteria}, 'geocode', 'address-postal-code');
    }

    if(cityInput && !cityInput.value){
      let criteria = "";
      if (postalcodeInput) criteria += `${document.getElementById(postalcodeInput.inputName.trim()).value}`;

      getPlacePredictions(criteria, 'geocode', [], false, 'address-city');
    } 
  }, 500);
  // Input validation when value change
  merchant.form.forEach((input) => {
    const inputEl = document.getElementById(input.inputName.trim());

    inputEl?.addEventListener("input", () => {
      continueBtn.disabled = !isValidForm(merchant, null, true);
      isValidInput(input, null, true);
    });

    inputEl?.addEventListener("change", () => {
      continueBtn.disabled = !isValidForm(merchant, null, true);
      isValidInput(input, null, true);
    });

    //autocomplete
    if (input.issuerFieldsName === 'name') {
      autocomplete(document.getElementById(input.inputName.trim()), 'names');
    }
    else if (input.issuerFieldsName === 'surname' || input.issuerFieldsName === 'surname2') {
      autocomplete(document.getElementById(input.inputName.trim()), 'surnames');
    }

    //geo autocomplete
    if (input.issuerFieldsName === 'address-street') {
      geoautocomplete(input.inputName.trim(), 'address');
    }
    else if (input.issuerFieldsName === 'address-city') {
      geoautocomplete(input.inputName.trim(), 'geocode');
    }
    else if (input.issuerFieldsName === 'address-postal-code') {
      geoautocomplete(input.inputName.trim(), 'geocode');
    }
  });
  // Form submit
  continueBtn.addEventListener("click", () => submit(merchant));
  // Toggle password for every input
  const passwordBtns = document.querySelectorAll('.icon-pass');
  passwordBtns.forEach(pass => {
    pass.addEventListener("click", (e) => {
      const inputAssociated = document.getElementById(pass.getAttribute('data-input-id'));
      if (inputAssociated.type === 'text') {
        inputAssociated.type = 'password';
        pass.classList.add('icon-show');
        pass.classList.remove('icon-hide');
      } else {
        inputAssociated.type = 'text';
        pass.classList.remove('icon-show');
        pass.classList.add('icon-hide');
      }
    });
  });

  configureInputDate();

  let input = document.querySelectorAll('.inputDate')[0];

  if (input) {

    let dateInputMask = function dateInputMask(elm) {
      elm.addEventListener('keypress', function (e) {
        if (e.keyCode < 47 || e.keyCode > 57) {
          e.preventDefault();
        }

        let len = elm.value.length;

        // If we're at a particular place, let the user type the slash
        // i.e., 12/12/1212
        if (len !== 1 || len !== 3) {
          if (e.keyCode == 47) {
            e.preventDefault();
          }
        }

        // If they don't add the slash, do it for them...
        if (len === 2) {
          elm.value += '/';
        }

        // If they don't add the slash, do it for them...
        if (len === 5) {
          elm.value += '/';
        }

        // If they don't add the slash, do it for them...
        /*   if(len >= 10) {
             e.preventDefault();
           }*/

      });
    };
    dateInputMask(input);
  }

}

function paintForm(merchant, fieldsInError) {

  let { button, settings: { sdkButtonType } } = context;

  let btnBack = '';
  if (!button) btnBack += `<button type="button" name="btn-back" id="btn-back">${i18n.button.back}</button>`;

  let html = `
  <form id="dynamic-form" novalidate>

      <p class="account-form-desc">${i18n.enrollmentForm.description}</p>

      ${paintFormInputs(merchant.form, fieldsInError)}
      ${paintTCsChecks()}

      <div class="cd-sdk-${sdkButtonType}-btns initial mt-25">
        ${btnBack}
        <button type="button" name="btn-continue" id="btn-continue" class="sdk-btn-main">${i18n.button.continue}</button>
    </div>
  </form>
  <!--END OF form-->`;
  return html;
}

function paintFormInputs(form, fieldsInError) {
  let html = '';
  let { registerFormValue, personalInfo } = context;
  let dataValue = registerFormValue || personalInfo;
  form.forEach(input => {

    input.value = '';

    const issuerFieldsName = input.issuerFieldsName.split(',');
    if (registerFormValue) {
      input.value = dataValue[input.inputName.trim()] || '';
    }
    else if (issuerFieldsName.length > 1) {
      issuerFieldsName.forEach(inputFieldName => {
        if (input.value !== '') { input.value += ' '; }
        if (dataValue[inputFieldName.replace(/\s/g, '')]) {
          input.value += dataValue[inputFieldName.replace(/\s/g, '')];
        }
      });
    } else if (input.issuerFieldsName && input.issuerFieldsName.trim() !== "") {
      input.value = dataValue[input.issuerFieldsName.trim()] || '';
    }
    else {
      input.value = dataValue[input.inputName.trim()] || '';
    }


    const dataType = input.dataType.split('(');
    let dateFormat = 'dd-mm-yyyy';
    if (dataType[0].indexOf('Date') > -1 && dataType.length > 1) {
      dateFormat = dataType[1].substr(0, dataType[1].length - 1);
    }

    switch (dataType[0].trim().toLowerCase()) {
      case 'text':
        if(input.value && input.value.indexOf("*") >=0){
          html += createInputAsterisksText(input, fieldsInError);
        }
        else html += createInputText(input, fieldsInError);
        break;
      case 'email':
        html += createInputText(input, fieldsInError);
        break;
      case 'number':
        html += createInputNumber(input, fieldsInError);
        break;
      case 'password':
        html += createInputPassword(input, fieldsInError);
        break;
      case 'dropdown':
        html += createInputDropDown(input, fieldsInError);
        break;
      case 'date':
        html += createInputDate(input, dateFormat, fieldsInError);
        break;
      default:
        return '';
    }
  });
  return html;
}

function createInputText(input, fieldsInError) {
  let errorClass = '', errorTxtClass = '', errorInfo = '';

  let readOnly = setReadOnly(input);

  if (fieldsInError !== '') {
    const fields = Object.keys(fieldsInError);
    fields.forEach(field => {
      if (field.toString() === input.inputName.trim().toString()) {
        readOnly = '';
        errorClass = 'cd-sdk-form-error';
        errorTxtClass = 'cd-sdk-txt-error'
        errorInfo = convertString(fieldsInError[field]);
      }
    });
  }

  let regexValidation = input.regexValidation ? input.regexValidation : "";

  let tooltip = !!input.toolTip ? `<span class="cd-sdk-txt-info" id="tooltip-${input.inputName.trim()}">${input.toolTip}</span>` : '';

  let html = `
  <div class="cd-sdk-form-row-account">
    <div class="cd-sdk-form-row-account-container ${errorClass}" id="${input.inputName.trim()}-input-cont">
      <label for="${input.inputName.trim()}" class="cd-sdk-form-label">${input.label}</label>
      <input type="text" class="cd-sdk-form-txt" ${readOnly} minlength="${input.minSize}" maxlength="${input.maxSize}" value="${input.value}" id="${input.inputName.trim()}" name="${input.inputName.trim()}" pattern="${regexValidation}">
    </div>
    ${tooltip}
    <span class="${errorTxtClass}" id="error-txt-${input.inputName.trim()}">${errorInfo}</span>
  </div>`;
  return html;
}

function createInputAsterisksText(input, fieldsInError) {
  let errorClass = 'cd-sdk-form-error', errorTxtClass = 'cd-sdk-txt-error', errorInfo = 'Reempaza los astriscos';

  if (fieldsInError !== '') {
    const fields = Object.keys(fieldsInError);
    fields.forEach(field => {
      if (field.toString() === input.inputName.trim().toString()) {
        readOnly = '';
        errorClass = 'cd-sdk-form-error';
        errorTxtClass = 'cd-sdk-txt-error'
        errorInfo = convertString(fieldsInError[field]);
      }
    });
  }


  let index = 0, values = [];
  values[index] = "";

  let chars = Array.from(input.value);
  for(let char of chars){
    if(char === '*'){
      if(values[index] && values[index].indexOf('*') < 0){
        index += 1;
        values[index] = "";
      }
      values[index] += char;
    }
    else{
      if(values[index] && values[index].indexOf('*') >= 0){
        index += 1;
        values[index] = "";
      }
      values[index] += char;
    }
  }

  let inputHtml = "";
  values.forEach((splitValue, index) => {
    let readOnly = "";
    let astrisksClass = "cd-sdk-txt-asteriks";
    let regexValidation = "[^\\*]$";
    if(splitValue.indexOf('*') < 0){
      readOnly = "readonly";
      astrisksClass = "";
      regexValidation = "";
    }
    inputHtml += `<input attr-asteriks="true" type="text" class="cd-sdk-form-txt ${astrisksClass}" ${readOnly} value="${splitValue}" id="${input.inputName.trim()}-asteriks-${index}" name="${input.inputName.trim()}-asteriks-${index}" pattern="${regexValidation}"></input>`;
  })

  let tooltip = !!input.toolTip ? `<span class="cd-sdk-txt-info" id="tooltip-${input.inputName.trim()}">${input.toolTip}</span>` : '';

  let html = `
  <div class="cd-sdk-form-row-account">
    <div class="cd-sdk-form-row-account-container ${errorClass}" id="${input.inputName.trim()}-input-cont">
      <label for="${input.inputName.trim()}" class="cd-sdk-form-label">${input.label}</label>
      <div class="cd-sdk-form-txt-container">${inputHtml}</div>
    </div>
    ${tooltip}
    <span class="${errorTxtClass}" id="error-txt-${input.inputName.trim()}">${errorInfo}</span>
  </div>`;
  return html;
}

function createInputNumber(input, fieldsInError) {
  let errorClass = '';
  let errorInfo = '';

  let readOnly = setReadOnly(input);

  if (fieldsInError !== '') {
    const fields = Object.keys(fieldsInError);
    fields.forEach(field => {
      if (field.toString() === input.inputName.trim().toString()) {
        readOnly = '';
        errorClass = 'cd-sdk-cd-sdk-form-error';
        errorInfo = convertString(fieldsInError[field]);
      }
    });
  }

  let regexValidation = input.regexValidation ? input.regexValidation : "";

  let tooltip = !!input.toolTip ? `<span class="cd-sdk-txt-info" id="tooltip-${input.inputName.trim()}">${input.toolTip}</span>` : '';
  let html = `
  <div class="cd-sdk-form-row-account">
    <div class="cd-sdk-form-row-account-container ${errorClass}" id="${input.inputName.trim()}-input-cont">
      <label for="${input.inputName.trim()}" class="cd-sdk-form-label">${input.label}</label>
      <input type="number" class="cd-sdk-form-txt" ${readOnly} minlength="${input.minSize}" maxlength="${input.maxSize}" value="${input.value}" id="${input.inputName.trim()}" name="${input.inputName.trim()}" pattern="${regexValidation}">
    </div>
    ${tooltip}
    <span id="error-txt-${input.inputName.trim()}">${errorInfo}</span>
  </div>`;
  return html;
}

function createInputPassword(input, fieldsInError) {
  let errorClass = '';
  let errorInfo = '';
  if (fieldsInError !== '') {
    const fields = Object.keys(fieldsInError);
    fields.forEach(field => {
      if (field.toString() === input.inputName.trim().toString()) {
        errorClass = 'cd-sdk-cd-sdk-form-error';
        errorInfo = convertString(fieldsInError[field]);
      }
    });
  }

  let regexValidation = input.regexValidation ? input.regexValidation : "";

  let tooltip = !!input.toolTip ? `<span class="cd-sdk-txt-info" id="tooltip-${input.inputName.trim()}">${input.toolTip}</span>` : '';
  let html = `
  <div class="cd-sdk-form-row-account">
    <div class="cd-sdk-form-row-account-container ${errorClass}" id="${input.inputName.trim()}-input-cont">
      <label for="${input.inputName.trim()}" class="cd-sdk-form-label">${input.label}</label>
      <input type="password" class="cd-sdk-form-txt cd-sdk-form-pass" minlength="${input.minSize}" maxlength="${input.maxSize}" value="${input.value}" id="${input.inputName.trim()}" name="${input.inputName.trim()}" pattern="${regexValidation}">
      <button type="button" data-input-id="${input.inputName.trim()}" class="icon-pass icon-show"></button>
    </div>
    ${tooltip}
    <span id="error-txt-${input.inputName.trim()}">${errorInfo}</span>
  </div>`;
  return html;
}

function createInputDropDown(input, fieldsInError) {
  let errorClass = '';
  let errorInfo = '';

  let readOnly = setReadOnly(input);

  let { personalInfo, settings: { country } } = context;

  if (fieldsInError !== '') {
    const fields = Object.keys(fieldsInError);
    fields.forEach(field => {
      if (field.toString() === input.inputName.trim().toString()) {
        readOnly = '';
        errorClass = 'cd-sdk-form-error';
        errorInfo = convertString(fieldsInError[field]);
      }
    });
  }
  const phonesList = countriesProvinces.map(x => {
    return {
      code: x.code,
      val: x.prefix_phone,
      name: x.prefix_phone
    };
  });
  sortList(phonesList);
  const countryList = countriesProvinces.map(x => {
    return {
      val: x.code,
      name: x.name_es
    };
  });
  sortList(countryList);
  const provinceList = provinces
    .filter(x => x.country === personalInfo['address-country'])
    .map(x => {
      return {
        val: x.short ? x.short : x.name,
        name: x.english ? x.english : x.name
      };
    });
  sortList(provinceList);

  let dropDown = `<option></option>`;
  let tooltip = !!input.toolTip ? `<span class="cd-sdk-txt-info" id="tooltip-${input.inputName.trim()}">${input.toolTip}</span>` : '';
  if (input.dropDown !== '') {
    if ((input.issuerFieldsName === 'phone-prefix')) {

      dropDown = !!input.dropDown ? input.dropDown.split(',').map(item => {

        let selected = "";
        let itemValue = item.split("|");
        if (itemValue && itemValue.length === 2) {

          if (!input.value && country && country.toLowerCase() === itemValue[0].toLowerCase()) selected = 'selected';
          if (JSON.stringify(input.value).toLowerCase() === JSON.stringify(itemValue[1]).toLowerCase()) selected = 'selected';
          return `<option ${selected}>${itemValue[1]}</option>`;
        }
      }) : [];
    }
    else {
      dropDown = !!input.dropDown ? input.dropDown.split(',').map(txt => {
        if (input.value && JSON.stringify(input.value).toLowerCase() === JSON.stringify(txt).toLowerCase()) {
          return `<option selected>${txt}</option>`;
        }
        return `<option>${txt}</option>`;
      }) : [];
    }
  } else {
    if (input.issuerFieldsName === 'country') {
      countryList.forEach(country => {
        const selected = JSON.stringify(input.value).toLowerCase() === JSON.stringify(country.val).toLowerCase() ? 'selected' : '';
        dropDown += `<option value="${country.val}" ${selected}>${country.name}</option>`;
      });
    } else if (input.issuerFieldsName === 'address-province') {
      provinceList.forEach(province => {
        const selected = JSON.stringify(input.value).toLowerCase() === JSON.stringify(province.val).toLowerCase() ? 'selected' : '';
        dropDown += `<option value="${province.val}" ${selected}>${province.name}</option>`;
      });
    } else if (input.issuerFieldsName === 'phone-prefix') {
      phonesList.forEach(prefix => {
        let selected = "";
        if (!input.value && country && country.toLowerCase() === prefix.code.toLowerCase()) selected = 'selected';
        if (JSON.stringify(input.value).toLowerCase() === JSON.stringify(prefix.val).toLowerCase()) selected = 'selected';
        dropDown += `<option value="${prefix.val}" ${selected}>${prefix.name}</option>`;
      });
    }
  }
  let html = `
  <div class="cd-sdk-form-row-account">
    <div class="cd-sdk-form-row-account-container ${errorClass}" id="${input.inputName.trim()}-input-cont">
      <label for="${input.inputName.trim()}" class="cd-sdk-form-label">${input.label}</label>
      <div class="cd-sdk-form-select-container">
        <select class="cd-sdk-form-select" ${readOnly} id="${input.inputName.trim()}" name="${input.inputName.trim()}">
          ${dropDown}
        </select>
      </div>
    </div>
    ${tooltip}
    <span id="error-txt-${input.inputName.trim()}">${errorInfo}</span>
  </div>`;
  return html;
}

function createInputDate(input, dateFormat, fieldsInError) {

  let errorClass = '';
  let errorInfo = '';

  let dateValue = '';

  if (input.value !== '') {
    let date = new Date(input.value);
    if (date instanceof Date) {
      dateValue = input.value.split('-').join('/');
    }
  }

  let readOnly = setReadOnly(input);

  if (fieldsInError !== '') {
    const fields = Object.keys(fieldsInError);
    fields.forEach(field => {
      if (field.toString() === input.inputName.trim().toString()) {
        readOnly = '';
        errorClass = 'cd-sdk-form-error';
        errorInfo = convertString(fieldsInError[field]);
      }
    });
  }

  let tooltip = !!input.toolTip ? `<span class="cd-sdk-txt-info" id="tooltip-${input.inputName.trim()}">${input.toolTip}</span>` : '';
  let html = `
  <div class="cd-sdk-form-row-account">
    <div class="cd-sdk-form-row-account-container ${errorClass}" id="${input.inputName.trim()}-input-cont">
      <label for="${input.inputName.trim()}" class="cd-sdk-form-label">${input.label}</label>
      <input type="text" class="cd-sdk-form-txt inputDate" ${readOnly} value="${dateValue}" id="${input.inputName.trim()}" name="${input.inputName.trim()}" attr-format="${dateFormat}">
    </div>
    ${tooltip}
    <span id="error-txt-${input.inputName.trim()}">${errorInfo}</span>
  </div>`;
  return html;
}

function setReadOnly(input) {
  let readOnly = '';
  if (input.readOnly && input.value) {
    readOnly = 'readonly';
    if (input.regexValidation) {
      if (!new RegExp(input.regexValidation).test(input.value)) {
        readOnly = '';
      }
    }
    if ((input.maxSize && input.value.length > input.maxSize) || (input.minSize && input.value.length < input.minSize)) {
      readOnly = '';
    }
  }
  return readOnly;
}

function configureInputDate() {
  let inputs = document.getElementsByClassName('inputDate');
  var options = {
    year: "numeric",
    month: "2-digit",
    day: "2-digit"
  };




  for (let i = 0; i < inputs.length; i++) {
    if (!inputs[i].hasAttribute('readonly')) {
      const picker = datepicker('#' + inputs[i].id, {
        overlayButton: "Ok",
        format: 'dd/mm/yyyy',
        formatter: (input, date, instance) => {
          const value = date.toLocaleDateString("es", options)
          input.value = value
        },
        customDays: ['Lu.', 'Ma.', 'Mi.', 'Ju.', 'Vi.', 'Sa.', 'Do.'],
        customMonths: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
      });

      if (inputs[i].value !== '') {
        const date = inputs[i].value.split('/');
        picker.setDate(new Date(date[2], parseInt(date[1]) - 1, date[0]), true);
      }
    }
  }
}

function changeCountryDropdown() {
  const countryDropdown = document.getElementById('address-country');
  countryDropdown.addEventListener('change', (ev) => {
    const provinceList = provinces
      .filter(x => x.country === ev.target.value)
      .map(x => {
        return {
          val: x.short ? x.short : x.name,
          name: x.english ? x.english : x.name
        };
      });
    sortList(provinceList);
  });
}

function paintTCsChecks() {

  let { registerFormValue, merchant, button } = context;
  let { merchantDetails, form } = merchant;

  const checked = registerFormValue ? 'checked' : '';
  let html = '';
  if((merchantDetails.privacyPolicy || merchantDetails.termsConditions || merchantDetails.additionalConditionsUrl) 
      && (!button || !form.some(x => x.issuerFieldsName === 'password'))){
    html += '<div class="cd-sdk-privacy-row">';

    if (merchantDetails.privacyPolicy) {
      let privacyPolicy = validateUrl(merchantDetails.privacyPolicy);
      if (privacyPolicy.indexOf("http://") === -1 && privacyPolicy.indexOf("https://") === -1) {
        privacyPolicy = 'https://' + privacyPolicy;
      }
  
      html += `
      <div class="cd-sdk-checkbox-simple">
        <input type="checkbox" id="ch-privacy" name="ch-privacy" ${checked}>
        <label for="ch-privacy" class="checkbox-label">${i18n.enrollmentForm.privacy.text} <a href="${privacyPolicy}" target="_blank" class="btn-link">${i18n.enrollmentForm.privacy.title}</a></label>
      </div>`;
    }

    if (merchantDetails.termsConditions) {
      let termsConditions = validateUrl(merchantDetails.termsConditions);
      if (termsConditions.indexOf("http://") === -1 && termsConditions.indexOf("https://") === -1) {
        termsConditions = 'https://' + termsConditions;
      }
  
      html += `
      <div class="cd-sdk-checkbox-simple">
        <input type="checkbox" id="ch-terms" name="ch-terms" ${checked}>
        <label for="ch-terms" class="checkbox-label">${i18n.enrollmentForm.terms.text} <a href="${termsConditions}" target="_blank" class="btn-link">${i18n.enrollmentForm.terms.title}</a></label>
      </div>`;
    }

    if (merchantDetails.additionalConditionsUrl) {
      let additionalConditionsUrl = validateUrl(merchantDetails.additionalConditionsUrl);
      if (additionalConditionsUrl.indexOf("http://") === -1 && additionalConditionsUrl.indexOf("https://") === -1) {
        additionalConditionsUrl = 'https://' + additionalConditionsUrl;
      }
  
      html += `
      <div class="cd-sdk-checkbox-simple">
        <input type="checkbox" id="ch-additional" name="ch-additional" ${checked}>
        <label for="ch-additional" class="checkbox-label">${i18n.enrollmentForm.additional.text} <a href="${additionalConditionsUrl}" target="_blank" class="btn-link">${i18n.enrollmentForm.additional.title}</a></label>
      </div>`;
    }

    html += '</div>';
  }
  return html;
}

function isValidForm(merchant, fieldsInError, showInputError) {
  // Validate dynamic inputs
  let isValid = true;
  for (const input of merchant.form) {

    let isValidField = false;
    let astriksChilds = document.querySelectorAll(`[id^='${input.inputName.trim()}-asteriks-']`);
    if(astriksChilds && astriksChilds.length > 0){
      for(let astriksChild of astriksChilds){
        if(astriksChild && astriksChild.hasAttribute('attr-asteriks')){
          isValidField = isValidInput(input, fieldsInError, showInputError, astriksChild.id);
          if (isValidField === false) {
            return false;
          }
        }
      }
    }
    else{
      isValidField = isValidInput(input, fieldsInError, showInputError);
    }

    if (isValidField === false) isValid = false;
  }
  if (isValid === false) return false;
  // Validate checks
  let privacyCheck = document.getElementById('ch-privacy');
  let termsCheck = document.getElementById('ch-terms');
  let additionalCheck = document.getElementById('ch-additional');
  if ((privacyCheck && !privacyCheck.checked) || (termsCheck && !termsCheck.checked) || (additionalCheck && !additionalCheck.checked)) return false;
  return true;
}

function isValidInput(input, fieldsInError, showInputError, astriksInputId = null) {
  
  let child = null;
  if(astriksInputId){ child = document.getElementById(astriksInputId); }
  else { child = document.getElementById(input.inputName.trim()); }

  const parent = document.getElementById(`${input.inputName.trim()}-input-cont`);
  const tooltipLabel = document.getElementById(`tooltip-${input.inputName.trim()}`);
  const errorLabel = document.getElementById(`error-txt-${input.inputName.trim()}`);
  if (child && parent) {
    
    if (fieldsInError && parent.classList.contains('cd-sdk-form-error')) return false;
    // Regex ALL
    
    if (child.value && child.pattern){
      const regex = new RegExp(child.pattern);
      if(!regex.test(child.value)) {
        if(astriksInputId) child.classList.add('cd-sdk-txt-error');
        if (showInputError) parent.classList.add('cd-sdk-form-error');
        if (showInputError && tooltipLabel) tooltipLabel.classList.add('cd-sdk-txt-error');
        else if (showInputError && errorLabel) {
          errorLabel.classList.add('cd-sdk-txt-error');
          errorLabel.innerHTML = i18n.inputValidations?.regex;
        }
        return false;
      } else {
        parent.classList.remove('cd-sdk-form-error');
        if(astriksInputId) child.classList.remove('cd-sdk-txt-error');
        if (showInputError && tooltipLabel) tooltipLabel.classList.remove('cd-sdk-txt-error');
        if (showInputError && errorLabel) {
          errorLabel.innerHTML = '';
          errorLabel.classList.remove('cd-sdk-txt-error');
        }
      }
    } 

    // Required ALL
    if (!child.value || child.value === undefined || child.value === null || child.value === '') {
      if (child.minLength > 0) {
        if (showInputError) parent.classList.add('cd-sdk-form-error');
        if (showInputError && tooltipLabel) tooltipLabel.classList.add('cd-sdk-txt-error');
        if (showInputError && errorLabel && !tooltipLabel) {
          errorLabel.classList.add('cd-sdk-txt-error');
          errorLabel.innerHTML = replaceI18nValue(i18n.inputValidations?.minLength, child.minLength)
        }

        return false;
      } else {
        parent.classList.remove('cd-sdk-form-error');
        if (showInputError && tooltipLabel) tooltipLabel.classList.remove('cd-sdk-txt-error');
        if (showInputError && errorLabel) {
          errorLabel.innerHTML = '';
          errorLabel.classList.remove('cd-sdk-txt-error');
        }
      }
    } else {

      if (child.minLength && child.minLength > -1 && child.minLength > child.value.length) {
        if (showInputError) parent.classList.add('cd-sdk-form-error');
        if (showInputError && tooltipLabel) tooltipLabel.classList.add('cd-sdk-txt-error');
        if (showInputError && errorLabel && !tooltipLabel) {
          errorLabel.classList.add('cd-sdk-txt-error');
          errorLabel.innerHTML = replaceI18nValue(i18n.inputValidations?.minLength, child.minLength);
        }

        return false;
      }
      else if (child.maxLength && child.maxLength > -1 && child.maxLength < child.value.length) {
        if (showInputError) parent.classList.add('cd-sdk-form-error');
        if (showInputError && tooltipLabel) tooltipLabel.classList.add('cd-sdk-txt-error');
        else if (showInputError && errorLabel) {
          errorLabel.classList.add('cd-sdk-txt-error');
          errorLabel.innerHTML = replaceI18nValue(i18n.inputValidations?.maxLength, child.maxLength);
        }

        return false;
      } else {
        parent.classList.remove('cd-sdk-form-error');
        if (showInputError && tooltipLabel) tooltipLabel.classList.remove('cd-sdk-txt-error');
        if (showInputError && errorLabel) {
          errorLabel.innerHTML = '';
          errorLabel.classList.remove('cd-sdk-txt-error');
        }
      }
    }
  }
  return true;
}

function submit(merchant) {
  let registrationForm = {};
  // Get form values in JSON format
  let formData = new FormData(document.getElementById('dynamic-form'));
  formData.forEach((value, key) => {
    if(document.getElementById(key).hasAttribute('attr-asteriks')){
      key = key.substring(0, key.length - 11);
      if(registrationForm[key]) registrationForm[key] += value;
      else registrationForm[key] = value;
    }
    else if (document.getElementById(key).hasAttribute('attr-format')) {
      const dateFormat = document.getElementById(key).getAttribute('attr-format');
      const birthdate = value.split('/');
      if(birthdate && birthdate.length >= 3){
        registrationForm[key] = dateFormat.replace('dd', birthdate[0]).replace('mm', birthdate[1]).replace('yyyy', birthdate[2]);
      }
    } else {
      if (key === 'addressCountry') registrationForm['address-country-name'] = value;
      else if (key === 'addressLocation') registrationForm['address-province-name'] = value;
      else if (!(key === 'ch-terms' || key === 'ch-privacy')) {
        registrationForm[key] = value;
      }
    }
  });

  context.registerFormValue = registrationForm;
  addPaymentMethodView(context, merchant, registrationForm, validMerchantId);
}

function validateUrl(url) {
  if (url !== '' && url.indexOf("http://") === -1 && url.indexOf("https://") === -1) {
    url = 'https://' + url;
  }
  return url;
}

function getLocalJSON(path) {
  return fetch(path)
    .then(res => res.json())
    .then(data => data)
    .catch(_ => []);
}

function sortList(list) {
  list.sort(function (a, b) {
    return a.name.localeCompare(b.name);
  });
}

async function autocomplete(input, type) {

  let currentFocus;
  input?.addEventListener("input", async function (e) {
    e.stopPropagation();
    closeAllLists();
    if (!this.value) { return false; }
    currentFocus = -1;

    let value = this.value;
    let finalValue = null;
    let values = this.value.split(" ");
    if (values.length > 1) {
      value = values[values.length - 1];
      for (let i = 0; i < values.length - 1; i++) {
        if (i === 0) finalValue = values[i];
        else finalValue = `${finalValue} ${values[i]}`;
      }
    }

    /** create container */
    let container = document.getElementById("autocomplete-list-" + this.id);

    if (container) {
      container.innerHTML = "";
    }
    else {
      container = document.createElement("DIV");
      container.setAttribute("id", "autocomplete-list-" + this.id);
      container.setAttribute("class", "autocomplete-list");
      this.parentNode.appendChild(container);
    }

    let filename = value.substr(0, 1).toLowerCase();
    if (!filename) return false;

    let data = await getLocalJSON(`../assets/data/${type}/${filename}.json`);

    data = data.filter(x => (x.toLowerCase()).startsWith(value.toLowerCase()));

    let length = data.length > 5 ? 5 : data.length;
    for (let i = 0; i < length; i++) {
      let item = document.createElement("DIV");
      item.innerHTML = "<strong>" + data[i].substr(0, value.length) + "</strong>";
      item.innerHTML += data[i].substr(value.length);
      item.innerHTML += "<input type='hidden' value='" + data[i] + "'>";
      item.addEventListener("click", function (e) {
        let newValue = this.getElementsByTagName("input")[0].value;
        if (finalValue) {
          input.value = `${finalValue} ${newValue}`
        }
        else {
          input.value = newValue;
        }
        input.focus();
        closeAllLists();
      });
      container.appendChild(item);
    }
  });

  input?.addEventListener("keydown", function (e) {

    let x = document.querySelectorAll(`div[id="autocomplete-list-${this.id}"] div`);
    if (!x) return;
    if (e.key == "ArrowDown") {
      if(currentFocus < x.length - 1) currentFocus++;
      addActive(x, currentFocus);
    } else if (e.key == "ArrowUp") {
      if(currentFocus > 0) currentFocus--;
      addActive(x, currentFocus);
    } else if (e.key == "Enter" || e.key == "Tab") {
      e.preventDefault();
      if (currentFocus > -1) {
        if (x) x[currentFocus].click();
      }
    }
  });


  document.addEventListener("click", function (e) {
    closeAllLists(e.target, input);
  });
}

let autocompleteService, geocoder;
function geoautocomplete(inputId, type) {

  let { settings: { country } } = context;
  let countryRestrict = [];
  if (country) countryRestrict = [country];

  let input = document.getElementById(inputId);

  //EventListeners
  let currentFocus;
  input.addEventListener('input', function () {
    currentFocus = -1;
    getPlacePredictions(this, type, countryRestrict, true);
  });

  input.addEventListener("keydown", function (e) {

    let x = document.querySelectorAll(`div[id="autocomplete-list-${this.id}"] div`);
    if (!x) return;
    if (e.key == "ArrowDown") {
      if(currentFocus < x.length - 1) currentFocus++;
      addActive(x, currentFocus);
    } else if (e.key == "ArrowUp") {
      if(currentFocus > 0) currentFocus--;
      addActive(x, currentFocus);
    } else if (e.key == "Enter" || e.key == "Tab") {
      e.preventDefault();
      if (currentFocus > -1) {
        if (x) x[currentFocus].click();
      }
    }
  });

  document.addEventListener("click", function (e) {
    closeAllLists(e.target, input);
  });
}

//geo autocomplte helpers
// Get place predictions
function getPlacePredictions(input, type, countryRestrict, showResults, autocompleteInput) {

  let criteria = input.value || input;

  if (!autocompleteService) autocompleteService = new google.maps.places.AutocompleteService();
  autocompleteService.getPlacePredictions({
    input: criteria,
    componentRestrictions: { country: countryRestrict },
    types: [type]
  }, (predictions) => {
    predictionsFn(predictions)
  });

  // Place search callback
  function predictionsFn(predictions) {

    if(!predictions){
      return;
    }
    else if(showResults){

      let container = document.getElementById("autocomplete-list-" + input.id);

      if (container) {
        container.innerHTML = "";
      }
      else {
        container = document.createElement("DIV");
        container.setAttribute("id", "autocomplete-list-" + input.id);
        container.setAttribute("class", "autocomplete-list");
        input.parentNode.appendChild(container);
      }

      for (let prediction of predictions) {
        let div = `<div data-placeid="${prediction.place_id}" data-name="${prediction.terms[0].value}">
            <strong>${prediction.description.substr(0, criteria.length)}</strong>${prediction.description.substr(criteria.length)}
          </div>`;

        container.innerHTML += div;
      }

      let items = document.querySelectorAll(`div[id="autocomplete-list-${input.id}"] div`);
      for (let item of items) {
        item.onclick = function () {
          if (this.dataset.placeid) {
            let placeId = this.dataset.placeid;
            getPlaceDetails({ placeId }, type, autocompleteInput);
          }
        }
      }
    }
    else{
      if(predictions && predictions.length > 0){
        let prediction = predictions[0];
        if(prediction.place_id){
          let placeId = prediction.place_id;
          getPlaceDetails({ placeId }, type, autocompleteInput);
        }
      }
    }
  }
}

// Autocomplete helpers
  // Get place details
  function getPlaceDetails(criteria, type, autocompleteInput, called) {

    if (!geocoder) geocoder = new google.maps.Geocoder();

    geocoder
      .geocode(criteria)
      .then(({ results }) => {

        if (!results || (results && results.length === 0)) return;

        let place = results[0];
        if (!place || (place && !place.address_components)) return;

        let inputs = context?.merchant?.form;

        for (const component of place.address_components) {

          const componentType = component.types[0];

          switch (componentType) {
            case "street_number": {
              let input = inputs.find(i => i.issuerFieldsName === 'address-number');
              if (input && type === 'address' && (!autocompleteInput || autocompleteInput === 'address-number')) {
                let el = document.getElementById(input.inputName.trim());
                el.value = component.short_name;
              }
              break;
            }

            case "route": {
              let input = inputs.find(i => i.issuerFieldsName === 'address-street');
              if (input && type === 'address' && (!autocompleteInput || autocompleteInput === 'address-street')) {
                let el = document.getElementById(input.inputName.trim());
                el.value = component.long_name;
              }
              break;
            }

            case "postal_code": {
              let input = inputs.find(i => i.issuerFieldsName === 'address-postal-code');
              if (input && (type === 'address' || type === 'geocode') && (!autocompleteInput || autocompleteInput === 'address-postal-code')) {
                let el = document.getElementById(input.inputName.trim());
                el.value = component.long_name;
              }
              break;
            }

            case "locality": {
              let input = inputs.find(i => i.issuerFieldsName === 'address-city');
              if (input && (type === 'address' || type === 'geocode') && (!autocompleteInput || autocompleteInput === 'address-city')) {
                let el = document.getElementById(input.inputName.trim());
                el.value = component.long_name;
              }
              break;
            }

            case "administrative_area_level_2": {
              let input = inputs.find(i => i.issuerFieldsName === 'address-province');
              if (input && (type === 'address' || type === 'geocode') && (!autocompleteInput || autocompleteInput === 'address-province')) {
                let el = document.getElementById(input.inputName.trim());
                el.value = component.long_name;
              }
              break;
            }
            case "country": {
              let input = inputs.find(i => i.issuerFieldsName === 'address-country');
              if (input && (type === 'address' || type === 'geocode') && (!autocompleteInput || autocompleteInput === 'address-country')) {
                let el = document.getElementById(input.inputName.trim());
                el.value = component.long_name;
              }
              break;
            }
          }
        }

      })
      .catch(_ => { });
  }

function addActive(x, currentFocus) {
  if (!x) return false;
  removeActive(x);
  if (currentFocus >= x.length) currentFocus = 0;
  if (currentFocus < 0) currentFocus = (x.length - 1);
  if (x[currentFocus]) x[currentFocus].classList.add("autocomplete-active");
}

function removeActive(x) {
  for (let item of x) {
    item.classList.remove("autocomplete-active");
  }
}

function closeAllLists(elmnt, input) {
  let x = document.getElementsByClassName("autocomplete-list");
  for (let i of x) {
    if (elmnt != i && elmnt != input) {
      i.parentNode.removeChild(i);
    }
  }
}