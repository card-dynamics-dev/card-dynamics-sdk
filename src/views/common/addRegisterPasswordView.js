import locale from "../../config/locale/i18n";
import addMerchantView from './addMerchantView';
import { convertString, replaceI18nValue } from "../../helpers/generic.helper";
import { hideSpinner, showSpinner } from "../../common/sdk.common";
import addRegisterMerchantView from "./addRegisterMerchantView";

let context, i18n;
export default function addRegisterPasswordView(ctx) {
  try {

    let { settings: { language } } = ctx;
    i18n = locale(language);
    context = ctx;

    document.addEventListener('login-result', function (e) {
      let { detail } = e;
      Object.assign(context, detail, { resultDone: true });
    }, { once: true });

    createViewInHtml();
    hideSpinner();

  } catch (error) {
    console.error(error);
  }
}


function createViewInHtml() {

  let { merchant: { cdMerchantId, form, merchantDetails} } = context;
  let passwordInput = form.find(x => x.issuerFieldsName === 'password');
  let confirmPassword = Object.assign({}, passwordInput, { 
    inputName: `confirm-${passwordInput.issuerFieldsName.trim()}`,
    issuerFieldsName: `confirm-${passwordInput.issuerFieldsName.trim()}`,
    label: i18n.messages.confirmPassword 
  });

  let inputs = [passwordInput, confirmPassword];

  let html = `
  <div class="cd-sdk-container-header">
    <div class="cd-sdk-container-header-title">${i18n.enrollmentPassword.title}</div>
    <span class="cd-sdk-container-header-steps">${i18n.enrollmentSteps.oneOf3}</span>
  </div>
  <div class="cd-sdk-register-container">
    ${paintForm(inputs)}
  </div>`;


  let container = document.getElementById('cd-sdk-container');
  if (container) {
    container.scroll({ top: 0 });
    container.innerHTML = html;
    hideSpinner();
  }

  // Listeners configuration
  setEventListeners(inputs);
  location.hash = 'cdMerchantId=' + cdMerchantId;
}

function setEventListeners(inputs) {
  // Back button click
  const backButton = document.getElementById('btn-back');
  if (backButton) {
    backButton.addEventListener("click", () => {
      location.hash = "";
      addMerchantView(context);
    });
  }

  // Form validation when some value change
  const submitForm = document.getElementById('dynamic-form');
  const continueBtn = document.getElementById('btn-continue');
  submitForm.addEventListener("change", () => continueBtn.disabled = !isValidForm(inputs, null, false));

  let checkboxs = document.querySelectorAll('div.cd-sdk-checkbox-simple input[type=checkbox]');
  for(let checkbox of checkboxs) {
      checkbox.addEventListener("change", () => continueBtn.disabled = !isValidForm(inputs, null, false));
  }

  // Input validation when value change
  inputs.forEach((input) => {
    const inputEl = document.getElementById(input.issuerFieldsName.trim());

    inputEl.addEventListener("input", () => {
      continueBtn.disabled = !isValidForm(inputs, null, false);
      let isValid = isValidInput(input, null, true);
      if(isValid){
        continueBtn.disabled = !isEqualsPasswords(input);
      }
    });

    inputEl.addEventListener("change", () => {
      continueBtn.disabled = !isValidForm(inputs, null, false);
      let isValid = isValidInput(input, null, true);
      if(isValid){
        continueBtn.disabled = !isEqualsPasswords(input);
      }
    });
  });
  // Form submit
  continueBtn.addEventListener("click", () => submit());
  // Toggle password for every input
  const passwordBtns = document.querySelectorAll('.icon-pass');
  passwordBtns.forEach(pass => {
    pass.addEventListener("click", (e) => {
      const inputAssociated = document.getElementById(pass.getAttribute('data-input-id'));
      if (inputAssociated.type === 'text') {
        inputAssociated.type = 'password';
        pass.classList.add('icon-show');
        pass.classList.remove('icon-hide');
      } else {
        inputAssociated.type = 'text';
        pass.classList.remove('icon-show');
        pass.classList.add('icon-hide');
      }
    });
  });
}

function paintForm(inputs) {

  let { registerFormValue, button, settings: { target, sdkButtonType }, merchant: { merchantDetails } } = context;
  const checked = registerFormValue ? 'checked' : '';
  const disabled = registerFormValue ? '' : 'disabled';
  let privacyPolicy = validateUrl(merchantDetails.privacyPolicy);
  let termsConditions = validateUrl(merchantDetails.termsConditions);

  let btnBack = '';
  if (!button) btnBack += `<button type="button" name="btn-back" id="btn-back">${i18n.button.back}</button>`;

  let html = `
  <form id="dynamic-form" novalidate>

      <p class="account-form-desc">${ replaceI18nValue(i18n.enrollmentPassword.description, merchantDetails.merchantBrand)}</p>

      ${paintFormInputs(inputs)}

      <div class="cd-sdk-privacy-row">
          <div class="cd-sdk-checkbox-simple">
              <input type="checkbox" id="ch-privacy" name="ch-privacy" ${checked}>
              <label for="ch-privacy" class="checkbox-label">${i18n.enrollmentForm.privacy.text} <a href="${privacyPolicy}" target="_blank" class="btn-link">${i18n.enrollmentForm.privacy.title}</a></label>
          </div>
          <div class="cd-sdk-checkbox-simple">
              <input type="checkbox" id="ch-terms" name="ch-terms" ${checked}>
              <label for="ch-terms" class="checkbox-label">${i18n.enrollmentForm.terms.text} <a href="${termsConditions}" target="_blank" class="btn-link">${i18n.enrollmentForm.terms.title}</a></label>
          </div>
          ${paintAdditionalCheck(merchantDetails)}
      </div>

      <div class="cd-sdk-${sdkButtonType}-btns initial">
        ${btnBack}
        <button type="button" name="btn-continue" id="btn-continue" class="sdk-btn-main" ${disabled}>${i18n.button.continue}</button>
    </div>
  </form>
  <!--END OF form-->`;
  return html;
}

function paintFormInputs(inputs) {
  return inputs.reduce((html, input) => html += createInputPassword(input), "");
}

function createInputPassword(input, fieldsInError) {
  let tooltip = !!input.toolTip ? `<span class="cd-sdk-txt-info" id="tooltip-${input.issuerFieldsName.trim()}">${input.toolTip}</span>` : '';
  let html = `
  <div class="cd-sdk-form-row-account">
    <div class="cd-sdk-form-row-account-container" id="${input.issuerFieldsName.trim()}-input-cont">
      <label for="${input.issuerFieldsName.trim()}" class="cd-sdk-form-label">${input.label}<span class="required"> *</span></label>
      <input type="password" class="cd-sdk-form-txt cd-sdk-form-pass" minlength="${input.minSize}" maxlength="${input.maxSize}" id="${input.issuerFieldsName.trim()}" name="${input.issuerFieldsName.trim()}" pattern="${input.regexValidation}">
      <button type="button" data-input-id="${input.issuerFieldsName.trim()}" class="icon-pass icon-show"></button>
    </div>
    ${tooltip}
    <span id="error-txt-${input.issuerFieldsName.trim()}"></span>
  </div>`;
  return html;
}

function paintAdditionalCheck(merchantDetails) {

  let { registerFormValue, settings: { target } } = context;
  const checked = registerFormValue ? 'checked' : '';
  let html = '';
  if (merchantDetails.additionalConditionsUrl) {
    let additionalConditionsUrl = validateUrl(merchantDetails.additionalConditionsUrl);
    if (additionalConditionsUrl.indexOf("http://") === -1 && additionalConditionsUrl.indexOf("https://") === -1) {
      additionalConditionsUrl = 'https://' + additionalConditionsUrl;
    }

    html = `
    <div class="cd-sdk-checkbox-simple">
      <input type="checkbox" id="ch-additional" name="ch-additional" ${checked}>
      <label for="ch-additional" class="checkbox-label">${i18n.enrollmentForm.additional.text} <a href="${additionalConditionsUrl}" target="_blank" class="btn-link">${i18n.enrollmentForm.additional.title}</a></label>
    </div>`;
  }
  return html;
}

function isValidForm(inputs, fieldsInError, showInputError) {
  // Validate dynamic inputs
  let isValid = true;
  for (const input of inputs) {
    let isValidField = isValidInput(input, fieldsInError, showInputError);
    if (isValidField === false) isValid = false;
  }
  if (isValid === false) return false;
  // Validate checks
  let privacyCheck = document.getElementById('ch-privacy');
  let termsCheck = document.getElementById('ch-terms');
  let additionalCheck = document.getElementById('ch-additional');
  if ((privacyCheck && !privacyCheck.checked) || (termsCheck && !termsCheck.checked) || (additionalCheck && !additionalCheck.checked)) return false;
  return true;
}


function isValidInput(input, fieldsInError, showInputError) {
  const child = document.getElementById(input.issuerFieldsName.trim());
  const parent = document.getElementById(`${input.issuerFieldsName.trim()}-input-cont`);
  const tooltipLabel = document.getElementById(`tooltip-${input.issuerFieldsName.trim()}`);
  const errorLabel = document.getElementById(`error-txt-${input.issuerFieldsName.trim()}`);

  if (child && parent) {
    
    // fieldsInError
    if (fieldsInError) {
      if (showInputError) parent.classList.add('cd-sdk-form-error');
      if (showInputError && errorLabel) {
        errorLabel.classList.add('cd-sdk-txt-error');
        errorLabel.innerHTML = convertString(fieldsInError[input.issuerFieldsName.trim()]);
      }
      else if (showInputError && tooltipLabel) tooltipLabel.classList.add('cd-sdk-txt-error');

      return false;
    }

    // Regex ALL
    const regex = new RegExp(input.regexValidation);
    if (child.value && input.regexValidation && !regex.test(child.value)) {
      if (showInputError) parent.classList.add('cd-sdk-form-error');
      if (showInputError && tooltipLabel) tooltipLabel.classList.add('cd-sdk-txt-error');
      else if (showInputError && errorLabel) {
        errorLabel.classList.add('cd-sdk-txt-error');
        errorLabel.innerHTML = i18n.inputValidations?.regex;
      }
      return false;
    } 
    else {
      parent.classList.remove('cd-sdk-form-error');
      if (showInputError && tooltipLabel) tooltipLabel.classList.remove('cd-sdk-txt-error');
      if (showInputError && errorLabel) {
        errorLabel.innerHTML = '';
        errorLabel.classList.remove('cd-sdk-txt-error');
      };
    }
    // Required ALL
    if (!child.value || child.value === undefined || child.value === null || child.value === '') {
      if (child.minLength > 0) {
        if (showInputError) parent.classList.add('cd-sdk-form-error');
        if (showInputError && tooltipLabel) tooltipLabel.classList.add('cd-sdk-txt-error');

        return false;
      } else {
        parent.classList.remove('cd-sdk-form-error');
        if (showInputError && tooltipLabel) tooltipLabel.classList.remove('cd-sdk-txt-error');
      }
    } 
    else {

      if (child.minLength && child.minLength > -1 && child.minLength > child.value.length) {
        if (showInputError) parent.classList.add('cd-sdk-form-error');
        if (showInputError && tooltipLabel) tooltipLabel.classList.add('cd-sdk-txt-error');
        if (showInputError && errorLabel && !tooltipLabel) {
          errorLabel.classList.add('cd-sdk-txt-error')
          errorLabel.innerHTML = replaceI18nValue(i18n.inputValidations?.minLength, child.minLength);
        }

        return false;
      }
      else if (child.maxLength && child.maxLength > -1 && child.maxLength < child.value.length) {
        if (showInputError) parent.classList.add('cd-sdk-form-error');
        if (showInputError && tooltipLabel) tooltipLabel.classList.add('cd-sdk-txt-error');
        else if (showInputError && errorLabel) {
          errorLabel.classList.add('cd-sdk-txt-error');
          errorLabel.innerHTML = replaceI18nValue(i18n.inputValidations?.maxLength, child.maxLength);
        }

        return false;
      } else {
        parent.classList.remove('cd-sdk-form-error');
        if (showInputError && tooltipLabel) tooltipLabel.classList.remove('cd-sdk-txt-error');
        if (showInputError && errorLabel) {
          errorLabel.innerHTML = '';
          errorLabel.classList.remove('cd-sdk-txt-error');
        }
      }
    }
  }
  return true;
}

function submit() {
  
  let messages = [i18n.spinner.gettingInfo, i18n.spinner.stillWaiting, i18n.spinner.processingData, i18n.spinner.thanksForWaiting];
  showSpinner(messages);
  let passwordInput = document.getElementById('password');

  let interval = setInterval(function(){
    if(context.resultDone){
      clearInterval(interval);
      context.personalInfo.password = passwordInput?.value
      addRegisterMerchantView(context);
    }
  }, 500);

  if(context.resultDone){
    clearInterval(interval);
    context.personalInfo.password = passwordInput?.value
    addRegisterMerchantView(context);
  }
}

function validateUrl(url) {
  if (url !== '' && url.indexOf("http://") === -1 && url.indexOf("https://") === -1) {
    url = 'https://' + url;
  }
  return url;
}

function isEqualsPasswords(input){

  const parent = document.getElementById(`${input.issuerFieldsName.trim()}-input-cont`);
  const errorLabel = document.getElementById(`error-txt-${input.issuerFieldsName.trim()}`);

  let values = [];
  for(let id of ['password', 'confirm-password']){
    let _child = document.getElementById(id);
    let _parent = document.getElementById(`${id}-input-cont`);
    let _errorLabel = document.getElementById(`error-txt-${id}`);

    if(_child.value){ values.push(_child.value)};

    _parent.classList.remove('cd-sdk-form-error');
    _errorLabel.innerHTML = '';
    _errorLabel.classList.remove('cd-sdk-txt-error');
  }

  if(values && values.length === 2){
    if(values[0] !== values[1]){
      parent.classList.add('cd-sdk-form-error');
      if (errorLabel) {
        errorLabel.classList.add('cd-sdk-txt-error')
        errorLabel.innerHTML = i18n.messages.notIdenticalPasswords;
      }
  
      return false;
    } else {
      parent.classList.remove('cd-sdk-form-error');
      if (errorLabel) {
        errorLabel.innerHTML = '';
        errorLabel.classList.remove('cd-sdk-txt-error');
      }
      return true;
    }
  }

  return false;
}
