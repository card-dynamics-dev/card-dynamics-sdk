import { hideSpinner, openCongratsModal, openModal, showSpinner } from "../../common/sdk.common";
import locale from "../../config/locale/i18n";
import { responseCodesMap } from "../../config/responseCodes";
import { decryptObjectValues, decryptString, encryptObject, encryptObjectValues, integrityCheck } from "../../helpers/crypto.helper";
import { convertString, copyContentToClipboard, getOS, mask, removeSpace } from "../../helpers/generic.helper";
import addRegisterMerchantView from './addRegisterMerchantView';

import Swiper, { Navigation, Pagination, EffectCoverflow } from 'swiper';

// import Swiper styles
import 'swiper/css';
import 'swiper/css/navigation';
import 'swiper/css/pagination';
import addMerchantView from "./addMerchantView";
import addVerificationView from "./addVerificationView";
import EnrollmentService from "../../services/enrollment.service";

// Variables
let merchantData = null;
let previousData = null;
let methodSelected = 'creditCards';
let creditSliderIndex = 0;
let bankSliderIndex = 0;
let validMerchantId = false;

/**
 *
 * @param {cdMerchantId: number, promotions: any[], form: any[], merchantDetails: object} merchantItem
 * @param {enrollmentId: string, customerId: string, hasPaymentMethod: boolean} registerData
 * @param valid
 */

let i18n, context, carrouselState, enrollmentService;

export default function addPaymentMethodView(ctx, merchantItem, registerData, valid) {

    context = ctx;
    let { settings, settings: { language } } = ctx;
    i18n = locale(language);

    carrouselState = {};

    validMerchantId = valid;
    merchantData = merchantItem;
    previousData = registerData;

    enrollmentService = new EnrollmentService(settings);

    // Create HTML view
    createViewInHtml(merchantData);
    // Set default payment method
    methodSelected = getMethodByDefault(merchantData.merchantDetails.paymentMethod);
    // Listeners configuration
    setEventListeners();
}

function createViewInHtml(merchant) {

    let { paymentMethods, settings: { sdkButtonType }, button } = context;

    let closeModal = "";
    let step = i18n.enrollmentSteps.threeOf3;
    if (!button) {
        closeModal = `<span class="cd-sdk-btn-close"><button id="modal-congrats-btn-close"><span class="icon-close"></span></button></span>`;
        step = i18n.enrollmentSteps.twoOf2;
    }

    let html = `
    <div class="cd-sdk-payment-methods-container">

        <div class="cd-sdk-container-header">
            <div class="cd-sdk-container-header-title">${i18n.enrollmentPayment.title}</div>
            <span class="cd-sdk-container-header-steps">${step}</span>
        </div>

        <p class="account-form-desc">${i18n.enrollmentPayment.description}</p>
        <!-- Tabs -->
        ${paintTabs(merchant.merchantDetails.paymentMethod)}
        <!-- END OF Tabs -->

        <!--Credit card block-->
        ${printCreditCards(paymentMethods.creditCards, merchant.merchantDetails.paymentMethod)}
        <!--END OF Credit card block-->
        
        <!--Bank account block-->
        ${printBankAccounts(paymentMethods.bankAccounts, merchant.merchantDetails.paymentMethod)}
        <!--END OF Bank account block-->
        
        <!--Additional check block-->
        ${paintAdditionalCheck(merchant)}
        <!--END OF Additional check block-->

        <div class="cd-sdk-${sdkButtonType}-btns initial">
            <button type="button" name="btn-back" id="btn-back">${i18n.button.back}</button>
            <button type="button" name="btn-continue" id="btn-continue" class="sdk-btn-main">${i18n.button.register}</button>
        </div>
    </div>

    <!--modal congratulations-->
    <div style="display: none" class="cd-sdk-popup cd-sdk-congrats">
        <div class="cd-sdk-popup-container">
            ${closeModal}
            <span class="cd-sdk-popup-icon"></span>
            <div class="cd-sdk-popup-title">${i18n.congratulations.title}</div>
            <div class="cd-sdk-popup-text">${i18n.congratulations.description}</div>

            <!-- paint copy text !-->
            ${paintEmailBox2()}

            <!-- paint download box !-->
            ${paintDownloadBox2(merchant.merchantDetails)}
        </div>
    </div>
    <!--END OF modal congratulations-->`;

    let container = document.getElementById('cd-sdk-container');
    if (container) {
        container.scroll({ top: 0 });
        container.innerHTML = html;
        hideSpinner();
    }
    else {
        let container = document.querySelector('.cd-sdk-carddynamics .cd-sdk-enrollment div[role="main"]');
        container.outerHTML = `
        <div role="main" class="cd-sdk-payment-methods">
            ${html}
        </div>`;
    }
}

function paintTabs(paymentMethod) {
    let bankAccounts = context.paymentMethods?.bankAccounts;
    let creditCards = context.paymentMethods?.creditCards;

    return +paymentMethod === 3 && bankAccounts && bankAccounts.length > 0 && creditCards && creditCards.length > 0  ? `
    <ul class="account-tabs" id="paymentTabs">
      <li class="active" id="creditCard">
          ${i18n.enrollmentPayment.tabs.cards}
      </li>
      <li id="accounts">
        ${i18n.enrollmentPayment.tabs.accounts}
      </li>
    </ul>` : '';
}

function printCreditCards(creditCards, paymentMethod) {

    if(!creditCards || (creditCards && creditCards.length == 0)){
        return "";
    }

    const show = +paymentMethod === 3 || +paymentMethod === 1 ? 'show' : '';
    let { button, merchant: { merchantDetails }, adapterInfo, othersInfo, othersInfo: { hiddenCVV } } = context;

    let slides = '';

    creditCards.forEach((card, key) => {

        const cardBackground = button ? othersInfo.cardBackground : card.cardBackground;

        let cvv = "";
        if(merchantDetails && !(merchantDetails.hiddenInputCVV === true)){
        cvv = `<div class="cd-sdk-form-row-account">
            <div class="cd-sdk-form-row-account-container" id="txt-cvv${key}-input-cont">
                <label for="txt-cvv${key}" class="cd-sdk-form-label">${i18n.enrollmentPayment.cvv.title}<span class="required"> *</span></label>
                <input type="${hiddenCVV ? 'password' : 'number'}" id="txt-cvv${key}" name="txt-cvv${key}" minlength="3" maxlength="4" class="cd-sdk-form-txt cvv" value="" pattern="^[0-9]{3,4}$" required>
            </div>
        </div>`;
        }

        let currency = "";
        if (Array.isArray(card.currency) && card.currency.length > 1) {
            let options = "";
            card.currency.forEach((curr, el) => {
                options += `<option value="${curr}">${curr}</option>`;
            })

            currency = `
            <div class="cd-sdk-form-row-account">
                <div class="cd-sdk-form-row-account-container " id="select-currency-cont">
                    <label for="select-currency${key}" class="cd-sdk-form-label">Currency</label>
                    <div class="cd-sdk-form-select-container">
                        <select id="select-currency${key}" name="select-currency${key}"  value="${card.currency[0]}" class="cd-sdk-form-select currency">
                        ${options}
                        </select>
                    </div>
                </div>
            </div>`
        }

        let expDate = "";
        if (!card.expDate) {
            expDate = `
            <div class="cd-sdk-form-row-account">
                <div class="cd-sdk-form-row-account-container" id="txt-expdate${key}-input-cont">
                    <label for="txt-expdate${key}" class="cd-sdk-form-label">${i18n.enrollmentPayment.expdate.title}<span class="required"> *</span></label>
                    <input type="text" placeholder="MM/YY" id="txt-expdate${key}" name="txt-expdate${key}" minlength="5" maxlength="5" class="cd-sdk-form-txt expdate" value="" pattern="^(0[1-9]|1[0-2])\/?([0-9]{2})$" required>
                </div>
            </div>`;
        }

        slides += `
        <div class="swiper-slide">
            <div class="credit-card">
                <div class="credit-card-wrapper" style="background-image:url(${cardBackground || 'assets/img/default-card-background.png'});">
                    <div class="credit-card-data">
                        <span class="credit-card-desc">**** ${card.pan.slice(card.pan.length - 4)} </span>
                        <span class="credit-card-expdate">${card.expDate ? card.expDate.substring(0, card.expDate.length - 2) + '/' + card.expDate.slice(-2) : ''}</span>
                    </div>

                    <div class="cd-sdk-credit-card-details">
                        ${cvv}  
                        ${expDate}
                        ${currency}
                    </div>
                </div>
            </div>
        </div>`;
    });

    let html = `
    <div class="swiper cardSwiper ${show}">
        <div class="swiper-wrapper">
            ${slides}
        </div>
        <div class="swiper-button-next"></div>
        <div class="swiper-button-prev"></div>
        <div class="swiper-pagination"></div>
    </div>`;

    return html;
}

function printBankAccounts(bankAccounts, paymentMethod) {

    if(!bankAccounts || (bankAccounts && bankAccounts.length == 0)){
        return "";
    }
    const show = +paymentMethod === 2 ? 'show' : '';
    let accountBackground, accountLogo;

    let { othersInfo } = context;
    if (othersInfo && othersInfo.accountBackground) {
        accountBackground = othersInfo.accountBackground;
    }

    if (othersInfo && othersInfo.accountLogo) {
        accountLogo = validateUrl(othersInfo.accountLogo);
    }


    let slides = '';

    bankAccounts.forEach((account, key) => {

        let currency = "";
        if (Array.isArray(account.currency) && account.currency.length > 1) {
            let options = "";
            account.currency.forEach((curr, el) => {
                options += `<option value="${curr}">${curr}</option>`;
            })

            currency = `<div class="cd-sdk-form-row-account">
            <div class="cd-sdk-form-row-account-container " id="select-currency-cont">
                <label for="select-currency${key}" class="cd-sdk-form-label">currency</label>
                        <div class="cd-sdk-form-select-container">
                            <select id="select-currency${key}" name="select-currency${key}"  value="${account.currency[0]}" class="cd-sdk-form-select currency">
                            ${options}
                            </select>
                        </div>
                </div>
            </div>`
        }

        let maskValue = mask(account.number, '*', 4, 4, 4);
        let accountImg = accountLogo ? `<img class="bank-account-logo" src="${accountLogo}" alt="" />` : "";

        slides += `
        <div class="swiper-slide">
            <div class="bank-account" style="${accountBackground ? `background-color: ${accountBackground};` : ''}">
                ${accountImg}
                <div class="bank-account-number">${maskValue}</div>
            </div>

            <div class="cd-sdk-account-details">
                ${currency}
            </div>
        </div>`;
    });

    let html = `
    <div class="swiper accountSwiper ${show}">
        <div class="swiper-wrapper">
            ${slides}
        </div>
        <div class="swiper-button-next"></div>
        <div class="swiper-button-prev"></div>
        <div class="swiper-pagination"></div>
    </div>`;

    return html;
}

function setEventListeners() {
    // Back button click
    const backButton = document.getElementById('btn-back');
    backButton.addEventListener("click", (e) => {
        addRegisterMerchantView(context, '');
    });

    // Submit button click
    const continueButton = document.getElementById('btn-continue');
    if (methodSelected === 'creditCards') continueButton.disabled = !validForm();
    else { continueButton.disabled = false; }

    continueButton.addEventListener("click", () => submit());

    // Tabs toggle
    var tabs = document.getElementById("paymentTabs");
    var cardSwiper = document.querySelector("div.swiper.cardSwiper");
    var accountSwiper = document.querySelector("div.swiper.accountSwiper");
    if (tabs && cardSwiper && accountSwiper) {
        const creditCard = document.getElementById("creditCard");
        creditCard.addEventListener("click", (e) => {
            e.preventDefault();
            tabs.children[1].classList.remove("active");
            tabs.children[0].classList.add("active");
            cardSwiper.classList.add("show");
            accountSwiper.classList.remove("show");
            methodSelected = 'creditCards';
            continueButton.disabled = !validForm();

            if (!carrouselState.cards) {
                initializeCreditCardsCarrousel();
            }
        });
        const accounts = document.getElementById("accounts");
        accounts.addEventListener("click", (e) => {
            e.preventDefault();
            tabs.children[0].classList.remove("active");
            tabs.children[1].classList.add("active");
            cardSwiper.classList.remove("show");
            accountSwiper.classList.add("show");
            methodSelected = 'bankAccounts';
            continueButton.disabled = false;

            if (!carrouselState.accounts) {
                initializeBankAccountsCarrousel();
            }
        });
    }

    //Validate cvv inputs
    const cardsCvv = document.querySelectorAll('.cvv');
    cardsCvv.forEach(input => {
        const inputEl = document.getElementById(input.id.trim());
        inputEl.addEventListener("keyup", () => {
            continueButton.disabled = !validForm(input);
        });
    });

    //Validate exp date inputs
    const inputExpdate = document.querySelectorAll('input.expdate');
    if (inputExpdate.length > 0) {

        inputExpdate.forEach(input => {
            const inputEl = document.getElementById(input.id.trim());

            inputEl.addEventListener("input", (element) => {
                
                let ele = element.currentTarget;
                let value = ele.value;
                if(isNaN(element.data)) {
                    ele.value = value.substring(0, value.length - 1);
                    return;
                }

                let ele2 = value.split('/').join('');
                if (ele2.length < 4 && ele2.length > 0) {
                    let finalVal = ele2.match(/.{1,2}/g).join('/');
                    ele.value = finalVal;
                }
                continueButton.disabled = !validForm(input);

            });
        });

    }

    // Additional check change
    let additionalCheck = document.getElementById('ch-additional');
    if (additionalCheck) {
        additionalCheck.addEventListener("change", () => continueButton.disabled = !validForm());
    }
    
    const appBtnModal = document.getElementsByClassName('btn-app');
    for (let i = 0; i < appBtnModal.length; i++) {
        appBtnModal[i].addEventListener("click", (e) => {

        });
    }
    const webBtnModal = document.getElementById('btn-web');
    if (webBtnModal) {
        webBtnModal.addEventListener("click", (e) => {

        });
    }

    // Modal email copy btn click
    const copyEmailModal = document.getElementById('modal-email-btn-copy');
    if (copyEmailModal) copyEmailModal.addEventListener("click", () => copyContentToClipboard('email-copy-txt'));
    // Modal congrats close btn click
    const closeCongratsModal = document.getElementById('modal-congrats-btn-close');

    if (closeCongratsModal) {
        closeCongratsModal.addEventListener("click", () => {
            location.hash = "";
            delete context.registerFormValue;
            addMerchantView(context);
        });
    }

    // Modal congrats copy btn click
    const copyCongratsModal = document.getElementById('modal-congrats-btn-copy');
    if (copyCongratsModal) copyCongratsModal.addEventListener("click", () => copyContentToClipboard('congrats-copy-txt'));

}

function initializeCreditCardsCarrousel() {

    var swiper = new Swiper(".cardSwiper", {
        modules: [Navigation, Pagination],
        slidesPerView: "auto",
        centeredSlides: true,
        pagination: {
            el: ".swiper-pagination",
            clickable: true,
        },
        navigation: {
            nextEl: ".swiper-button-next",
            prevEl: ".swiper-button-prev",
        }
    });

    swiper.on('slideChange', function ({ activeIndex }) {
        creditSliderIndex = activeIndex;
        const continueButton = document.getElementById('btn-continue');
        continueButton.disabled = !validForm();
    });
}

function initializeBankAccountsCarrousel() {
    var swiper = new Swiper(".accountSwiper", {
        modules: [Navigation, Pagination],
        slidesPerView: "auto",
        centeredSlides: true,
        spaceBetween: 20,
        pagination: {
            el: ".swiper-pagination",
            clickable: true,
        },
        navigation: {
            nextEl: ".swiper-button-next",
            prevEl: ".swiper-button-prev",
        }
    });

    swiper.on('slideChange', function ({ activeIndex }) {
        bankSliderIndex = activeIndex;
    });
}

function getMethodByDefault(methodOpt) {
    if (methodOpt === 2) {
        initializeBankAccountsCarrousel();;
        return 'bankAccounts';
    }
    else {
        // Carousel settings
        initializeCreditCardsCarrousel();
        return 'creditCards';
    }
}

function submit() {

    showSpinner();

    let { paymentMethods, registerFormValue, button, settings: { secret_key, provider }, merchantService } = context;
    let message = i18n.error.default;

    // Prepare request data
    let requestForm = {
        cdMerchantId: merchantData.cdMerchantId,
        registrationData: registerFormValue,
        //cardData: definitiveCardData,
        promoId: merchantData.promoSelected ? merchantData.promoSelected.promoId : null,
        enrollmentOrigin: button ? 1 : 0
    };

    const index = methodSelected === 'creditCards' ? creditSliderIndex : bankSliderIndex;
    const paymentData = Object.assign({}, paymentMethods[methodSelected][index]);

    // If currencies come like Array (bank-adapters), I take selected:
    if (Array.isArray(paymentData.currency) && paymentData.currency.length > 1) paymentData.currency = document.getElementById('select-currency' + index).value;
    if (Array.isArray(paymentData.currency) && paymentData.currency.length === 1) paymentData.currency = paymentData.currency[0];


    if (methodSelected === 'creditCards') {

        const cvv = document.getElementById('txt-cvv' + index);
        if(cvv){
            paymentData.cvv = cvv.value;
        }

        if (!paymentData.expDate) {
            paymentData.expDate = document.getElementById('txt-expdate' + index).value.replace('/', '');
        }

        let cardData = { ...paymentData };
        if (cardData.cardBackground) delete cardData.cardBackground;

        // Integrity check & cryptography
        requestForm.cardData = cardData;
        requestForm.integrityCheck = integrityCheck(requestForm, ['registrationData', 'cardData']);
        requestForm = encryptObjectValues(requestForm, secret_key, ['registrationData', 'cardData']);
    }
    else {
        // Integrity check & cryptography
        requestForm.accountData = paymentData;
        requestForm.integrityCheck = integrityCheck(requestForm, ['registrationData', 'accountData']);
        requestForm = encryptObjectValues(requestForm, secret_key, ['registrationData', 'accountData']);
    }

    /*let btnConfig = SharedDataService.getButtonConfig();
    if (btnConfig) {
        if (btnConfig.idProcess) {
            requestForm.idProcess = parseInt(btnConfig.idProcess);
        }
        if (btnConfig.idSession) {
            requestForm.idSession = btnConfig.idSession;
        }
        if (btnConfig.buttonOrigin) {
            requestForm.buttonOrigin = btnConfig.buttonOrigin;
        }
    }
    */


    // Send POST req
    const continueBtn = document.getElementById('btn-continue');
    continueBtn.disabled = true;

    /*
    const event = new CustomEvent('registration', { detail: "OK" });
    window.dispatchEvent(event);
    */

    merchantService.registerWithPayment(requestForm).then(response => {
        continueBtn.disabled = false;
        let message = i18n.error.default;

        response.json().then(data => {

            if (data.status !== 'PROCESSING') {
                try {
                    sendResult(false, 'ENROLLMENT', requestForm, data);
                } catch (err) {
                    console.log('error sendResult', err);
                }
            }

            if (data.status === 'PROCESSING') {

                let counter = 0;

                let interval = setInterval(function () {
                    counter++;
                    if (counter > 10) {
                        clearInterval(interval);
                        hideSpinner();
                        openModal({ text: i18n.messages.merchantNotAvailable }, 'error');
                    } else {
                        checkStatusRegistrationWithPayment(requestForm, data, counter, interval);
                    };
                }, 4000);
                return

            } else if (data.status === 'PENDING_VALIDATION') {
                addVerificationView(context, merchantData, requestForm, data);
            } else if (data.status === 'FAILURE') {

                if (responseCodesMap.get('CARD_REGISTRATION_ERRORS').get(data.responseCode)) {
                    hideSpinner();
                    openCongratsModal(i18n.error.registerPaymmentKo, 'warning');
                }

                else if (data.fieldsInError && data.fieldsInError !== null) {

                    hideSpinner();
                    let existUser = Object.keys(data.fieldsInError).some(key => 'email' === key);
                    if (existUser && data.responseCode === 'REGISTER_FIELD_DUPLICATED' && methodSelected === 'creditCards' && merchantData.replacement) {

                        openModal({
                                text: i18n.messages.enrollReplacement,
                                okLabel: i18n.button.update,
                                okFn: () => {
    
                                    let { othersInfo, settings: { secret_key, issuerId } } = context;
    
                                    let data = encryptObject({
                                        ids: [merchantData.cdMerchantId],
                                        newCard: paymentData,
                                        othersInfo
                                    }, secret_key);
                                    window.location = `./replacement.html?is_id=${issuerId}&data=${encodeURIComponent(data)}`
                                },
                                koLabel: i18n.button.back,
                                koFn: () => {
                                    addRegisterMerchantView(context, data.fieldsInError)
                                }
                            }, 'warning', 'confirm');
                       
                    }
                    else {
                        openModal({
                            text: i18n.messages.checkErrorFields,
                            okFn: () => addRegisterMerchantView(context, data.fieldsInError)
                        }, 'error');
                    }
                } else {

                    hideSpinner();
                    continueBtn.disabled = false;
                    if (data.message) message = data.message;
                    openModal({ text: convertString(message) }, 'error');
                }

            } else if (data.status === 'SUCCESS') {

                if (provider === 'ps') {

                    setTimeout(() => {
                        hideSpinner();
                        window.close('', '_parent', '');

                    }, 4000);

                } else {

                    if(button){

                        let inputs = merchantData.form.filter(x => x.issuerFieldsName === 'email' || x.issuerFieldsName === 'password');
                        let resultData = {};
                        if(inputs && inputs.length > 0){
                            resultData.status = "success";
                            inputs.forEach(input => {
                                resultData[input.issuerFieldsName] = registerFormValue[input.inputName];
                            });
                        }
                        const event = new CustomEvent('sdkcallback', { detail: {
                            result: resultData,
                            close: false 
                        }});
                        document.dispatchEvent(event);
                        hideSpinner();
                    }
                    else{
                        hideSpinner()
                        if (data.extra) {

                            openCongratsModal();
                            let url = data.extra.claimUrl;
                            url ? document.querySelector('#btn-web').setAttribute('href', url) : '';
                            return;
                        }
                        else{
                            openCongratsModal();
                        }
                    }
                }

            } else {
                hideSpinner();
                continueBtn.disabled = false;
                if (data.message) message = data.message;
                openModal({ text: convertString(message) }, 'error');
            }
        });

    }).catch((err) => {
        try {
            sendResult(true, 'ENROLLMENT', requestForm, {});
        } catch (err) {
            console.log('error sendResult', err);
        }
        hideSpinner();
        continueBtn.disabled = false;
        if (err.message) message = err.message;
        openModal({ text: convertString(message) }, 'error');
    });
}

function checkStatusRegistrationWithPayment(requestForm, data, counter, interval) {
    MerchantService.checkStatusRegistrationWithPayment({ enrollmentRequestId: data.enrollmentId }).then((resStatus) => {
        let message = '';
        resStatus.json().then({ enrollmentRequestId: data.enrollmentId })
            .then(statusData => {
                if (statusData.status !== "PROCESSING") {
                    try {
                        sendResult(false, 'CHECK_STATUS_ENROLLMENT', requestForm, statusData);
                    } catch (err) {
                        console.log('error sendResult', err);
                    }
                    hideSpinner();
                    clearInterval(interval);
                    if (statusData.status === "SUCCESS") {

                        if (provider === 'ps') {

                            setTimeout(() => {

                                window.close('', '_parent', '');

                            }, 4000);


                        } else {


                            if (data.extra) {

                                openCongratsModal();
                                let url = data.extra.claimUrl;
                                url ? document.querySelector('#btn-web').setAttribute('href', url) : '';
                                return;
                            }

                            openCongratsModal();

                        }
                    }
                }
                if (statusData.status === "SUCCESS" && counter < 3) {

                    if (provider === 'ps') {
                        setTimeout(() => {

                            window.close('', '_parent', '');

                        }, 4000);
                    } else {

                        hideSpinner();

                        if (data.extra) {

                            openCongratsModal();
                            let url = data.extra.claimUrl;
                            url ? document.querySelector('#btn-web').setAttribute('href', url) : '';
                            return;
                        }

                        openCongratsModal();

                    }

                }
                if (statusData.status === 'PENDING_VALIDATION') {

                    addVerificationView(context, merchantData, requestForm, statusData);

                } else if (statusData.status === 'FAILURE' || statusData.status === 'ERROR') { //Revisar cuando usamos solo cd apis

                    if (responseCodesMap.get('CARD_REGISTRATION_ERRORS').get(statusData.responseCode)) {
                        return openCongratsModal(statusData.message, 'warning');
                    }

                    if (statusData.fieldsInError && statusData.fieldsInError !== null) {

                        openModal({
                            text: 'Revisa los campos marcados en rojo',
                            okFn: () => addRegisterMerchantView(context, data.fieldsInError)
                        }, 'error');

                    } else {
                        if (statusData.message) message = statusData.message;
                        openModal({ text: convertString(message) }, 'error');
                    }

                }

            })
            .catch(error => {
                try {
                    sendResult(true, 'CHECK_STATUS_ENROLLMENT', requestForm, {});
                } catch (err) {
                    console.log('error sendResult', err);
                }
                hideSpinner();
                //continueBtn.disabled = false;
                if (error.message) message = error.message;
                openModal({ text: convertString(message) }, 'error');
            });
    });
}

function sendResult(error = false, type, request, response) {

    let customerId = context?.settings?.customerId;
    let secret_key = context?.settings?.secret_key;
    let urlResponse = context?.othersInfo?.urlResponse;

    let data = {
        type,
        customerId,
    };

    data.cdMerchantId = request.cdMerchantId;
    data.merchantName = merchantData.merchantDetails.merchantBrand;

    let requestForm = decryptObjectValues(request, secret_key, ['accountData', 'cardData']);
    if (requestForm && requestForm.cardData) {
        data.pan = mask(requestForm.cardData.pan, '*', 4, 4, 6);
    }
    else if (requestForm && requestForm.accountData) {
        data.accountNumber = mask(requestForm.accountData.number, '*', 4, 4, 6);
    }

    let enrollmentId = response.enrollmentId ? response.enrollmentId : '00000000-0000-0000-0000-000000000000';

    if (error) {

        Object.assign(data, {
            response: {},
            status: 'ERROR'
        });

    } else {
        Object.assign(data, {
            response,
            status: response.status
        });
    }

    data.enrollmentId = enrollmentId;
    data = encryptObject(data, secret_key);



    return enrollmentService.sendResult(enrollmentId, {
        enrollmentData: data,
        url: urlResponse
    });

}

function paintEmailBox2() {
    if (previousData.email) {
        let { registerFormValue: { email } } = context;
        return `<div class="cd-sdk-input-box-container">
            <input type="text" id="cd-sdk-input-copy-txt" value="${email}" class="form-txt">
            <button type="button" id="cd-sdk-btn-copy-txt" class="cd-sdk-btn-copy"><span class="icon-copy"></span></button>
        </div>`
    }
    return '';
}

function paintDownloadBox2(details) {

    let iOSStoreUrl = '';
    let androidStoreUrl = '';
    let html = '';

    let { button, provider } = context;
    if (!button || (button && provider != 'ps')) {
        //TODO
    }

    let os = getOS();
    if (os === 'iOS' || os === 'Android') {

        if (details.iOSStoreUrl || details.androidStoreUrl) {
            html += `<p class="download-p">${i18n.congratulations.enjoy.appTitle}</p>`;
        }

        html += `<div class="logo-container">`;
        if (os === 'iOS' && details.iOSStoreUrl) {
            iOSStoreUrl = validateUrl(details.iOSStoreUrl);
            html += `<a href="${iOSStoreUrl}" target="_blank" class="btn-app" data-app="IOS">
            <img src="assets/img/app-store.svg" alt="Download on the App Store">
            </a>`;
        }
        else if (os === 'Android' && details.androidStoreUrl) {
            androidStoreUrl = validateUrl(details.androidStoreUrl);
            html += `<a href="${androidStoreUrl}" target="_blank" class="btn-app" data-app="Android">
            <img src="assets/img/google-play.svg" alt="Download from Google Play">
            </a>`;
        }
        html += `</div>`;
    }
    else if (details.webUrl) {
        const webUrl = validateUrl(details.webUrl);
        html += `
      <p class="download-p">${i18n.congratulations.enjoy.title}</p>
      <a href="${webUrl}" id="btn-web" class="modal-btn type3" target="_blank">${i18n.congratulations.button.login}</a> `;
    }

    return html;
}

function paintAdditionalCheck(merchant) {
    let html = '';
    if (merchant.merchantDetails.cardConditionsUrl) {
        const cardConditionsUrl = validateUrl(merchant.merchantDetails.cardConditionsUrl);
        html = `
      <div class="cd-sdk-privacy-row">
        <div class="cd-sdk-checkbox-simple">
          <input type="checkbox" id="ch-additional" name="ch-additional">
          <label for="ch-additional" class="checkbox-label">${i18n.enrollmentPayment.additional.text} <a href="${cardConditionsUrl}" target="_blank" class="btn-link">${i18n.enrollmentPayment.additional.title}</a></label>
        </div>
      </div>`;
    }
    return html;
}

function validForm(currentInput = null) {
    let valid = true;
    let inputs = document.querySelectorAll('.cd-sdk-form-txt');

    let index = methodSelected === 'creditCards' ? creditSliderIndex : bankSliderIndex;
    if(inputs && inputs.length > 0){
        inputs.forEach(function (input) {
            if (input.id === 'txt-expdate' + index || input.id === 'txt-cvv' + index) {
                let showError = currentInput && currentInput.id === input.id;
                let validField =  isValidInput(input, showError);
                if (validField === false) valid = false;
            }
        });
    }

    let selects = document.querySelectorAll('.cd-sdk-form-select currency');
    if (selects && selects.length > 0) {
        selects.forEach(function(input){
            if (input.id === 'select-currency' + index) {
                let showError = currentInput && currentInput.id === input.id;
                let validField = isValidInput(input, showError)
                if (validField === false) valid = false;
            }
        });
    }

    let additionalCheck = document.getElementById('ch-additional');
    if (additionalCheck && !additionalCheck.checked) valid = false;

    return valid;
}

function isValidInput(input, showError = false){
    let value = input.value;
    let length = removeSpace(value).length;

    if (length > input.maxLength || length < input.minLength) {
        if(showError){
            input.parentElement.classList.add('cd-sdk-form-error');
        }
        return false;
    }
    else {
        input.parentElement.classList.remove('cd-sdk-form-error');
        return true;
    }
}

function validateUrl(url) {
    if (url && url.indexOf("http://") === -1 && url.indexOf("https://") === -1) {
        url = 'https://' + url;
    }
    return url;
}

