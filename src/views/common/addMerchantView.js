import { hideSpinner, loadFont, openModal, showSpinner } from "../../common/sdk.common";
import locale from "../../config/locale/i18n";
import { decryptString } from "../../helpers/crypto.helper";
import MerchantService from "../../services/merchant.service";
import SdkService from "../../services/sdk.service";
import addRegisterMerchantView from "./addRegisterMerchantView";
import addDetailMerchantView from "../bankRegister/addMerchantDetailView";

import '../../assets/style/common.scss';
import '../../assets/style/common.responsive.scss';
import '../../assets/style/enrollment.scss';
import '../../assets/style/enrollment.responsive.scss';
import SdkException from "../../common/sdk-exception";
import addRegisterPasswordView from "./addRegisterPasswordView";

let originalMerchantList = [];
let filteredList = [];
let validMerchantId = false;

let i18n, context;

export default async function addMerchantView(ctx) {
  try {

    /*window.addEventListener('beforeunload', function (e) {
      return e.returnValue = '';
    }, { capture: true }); */

    context = ctx;
    let { data, settings, settings: { cdMerchantId, language, issuerId }, button } = ctx;

    i18n = locale(language);

    if(!context.google){
      let script = document.createElement('script');
      script.src = `https://maps.googleapis.com/maps/api/js?key=${process.env.GOOGLE_API_KEY}&libraries=places&callback=init`;
      script.async = true;

      window.init = function() {};
      document.head.appendChild(script);

      context.google = true;
    }
    
    //Enrollment with data
    if (!button) {
      let sdkService = new SdkService();
      let sdkResult = await sdkService.getIssuerSKey(issuerId);

      if (sdkResult.status == 200) {
        let sdkData = Buffer.from(sdkResult.data, 'base64').toString('utf-8');
        try {

          Object.assign(context.settings, JSON.parse(sdkData));

          if (data) {
            const { defaultPaymentMethods, defaultPersonalInfo, defaultOthersInfo } = decryptString(data, context.settings.secret_key);

            if (defaultPaymentMethods) {
              context.paymentMethods = defaultPaymentMethods
            }

            if (defaultPersonalInfo) {
              context.personalInfo = defaultPersonalInfo;
            }

            if (defaultOthersInfo) {
              context.othersInfo = defaultOthersInfo;
            }
          }

        } catch {
          throw new SdkException('settingError')
        }
      }
      else {
        throw new SdkException('settingError')
      }

      init();
    }

    let merchantIds = [], defaultMerchants = [];
    if(context.othersInfo && context.othersInfo.merchantIds){
      merchantIds = context.othersInfo.merchantIds;
    }


    if (location.hash.search('cdMerchantId') > -1) {
      cdMerchantId = location.hash.split("=")[1];
    }

    let merchantService = new MerchantService(settings);

    //let result = await merchantService.getMerchantList2(button, cdMerchantId);

    // Get merchant list
    merchantService.getMerchantList2(button, cdMerchantId).then((list) => {
      let promoId = 'no-promo';
      originalMerchantList = list;
      filteredList = list;

      if (cdMerchantId && cdMerchantId !== '') {
        list.forEach(group => {
          group.merchants.forEach(merchant => {

            /** default merchant */
            if(merchantIds.some(id => id == merchant.cdMerchantId)){
              defaultMerchants.push(merchant);
            }
            if (merchant.cdMerchantId === cdMerchantId) {

              if(merchant.promotions && merchant.promotions.length > 0){
                merchant.promotions.forEach(promo => {
                  promoId = promo.promoId;
                });
              }
              validMerchantId = true;
            }
          });
        });
      }
      else if(merchantIds.length > 0){
        list.forEach(group => {
          group.merchants.forEach(merchant => {
            if(merchantIds.some(id => id == merchant.cdMerchantId)){
              defaultMerchants.push(merchant);
            }
          });
        });
      }

      if (validMerchantId) {
        openRegister(context, cdMerchantId, promoId);
      } else if (location.hash.search('cdMerchantId') > -1) {
        const merchantHash = location.hash.split("=")[1];
        openRegister(context, merchantHash, promoId);
      } else {

        // Create HTML view
        createViewInHtml();

        // Paint merchant list
        let dataList = list;
        if(defaultMerchants && defaultMerchants.length > 0) { 
          dataList = [{ 
            merchantType: null,
            merchants: defaultMerchants
          }];
        }

        paintMerchantList(dataList);

        // Listeners configuration
        setEventListeners();
      }
    });

    context.merchantService = merchantService;

  } catch (error) {
      console.log(error)
      hideSpinner();
      let message = 'Ooops! algo fue mal.';
      if (error && error.data && error.data.message) message = error.data.message;
      openModal({ text: message }, 'error')
  }
    
}

function init() {

  let cssStyle = "<style>";

  let cssOverride = context?.othersInfo?.cssOverride;
  let header = context?.othersInfo?.header;
  let loader = context?.othersInfo.loader;

  if(!context.button){
    let fontUrl = 'https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800';
    if(cssOverride && cssOverride.fontUrl){
      fontUrl = cssOverride.fontUrl;
    }

    loadFont(fontUrl);
  }

  if (cssOverride){
    
    if(cssOverride.colors && Object.keys(cssOverride.colors).length > 0 ) {
      let colorValues = "";

      if(cssOverride.colors.primary && !cssOverride.colors.accent){
        cssOverride.colors.accent = cssOverride.colors.primary;
      }
      
      for (const [key, value] of Object.entries(cssOverride.colors)) {
        colorValues += `--${key}: ${value};`;
      }
      cssStyle += `:root { ${ colorValues }}`;
    }

    if(cssOverride.style){
      cssStyle += cssOverride.style;
    }
  }


  cssStyle += "</style>";

  let hcontent = `<h1>Enrollment from bank</h1>`;
  if (header?.logo) {
    hcontent = `<img src="${header?.logo}">`;
  }
  else if(header?.text){
    hcontent = `<h1>${header?.text}</h1>`;
  }

  let mobileLoader = "assets/img/loading-mobile.gif";
  let webLoader = "assets/img/loading-web.gif";
  let textVisibilityStyle = "";
  let loaderHeight = null;
  let loaderClasses = "";

  if(loader){
    if(loader.mobile){
      mobileLoader = loader.mobile;
    }
    if(loader.web){
      webLoader = loader.web;
    }

    if(loader.textVisibility === false){
      textVisibilityStyle = `style="display: none;"`;
    }

    if(loader.box === false){
      loaderClasses = 'cd-sdk-spinner-without-box';
    }
  }

  document.body.outerHTML = `
        <body>
          ${cssStyle}
          <div class="cd-sdk-carddynamics">
            <div class="cd-sdk-enrollment">
              <header role="banner" style="color: ${ header?.color || 'white' }; background: ${header?.background || 'var(--primary)'}; border-bottom: 1px solid var(--primary);">
              <div class="cd-sdk-header-wrapper">${hcontent}</div>
              </header>
            </div>

            <div class="cd-sdk-iframe-container" style="display: none;">
              <span class="cd-sdk-btn-close"><button><span class="icon-close"></span></button></span>
              <iframe scrolling="no"></iframe>
            </div>

            <div style="display: flex;" class="cd-sdk-spinner ${loaderClasses}">
              <div class="cd-sdk-spinner-container">
                <img class="mobile" src="${mobileLoader}" />
                <img class="web" src="${webLoader}" />
                <div ${textVisibilityStyle}>${i18n.spinner.loading}</div>
              </div>
            </div>

            <div style="display: none" class="cd-sdk-popup cd-sdk-alerts">
              <div class="cd-sdk-popup-container">
                <span class="cd-sdk-popup-icon"></span>
                <div class="cd-sdk-popup-text"></div>
                <div class="cd-sdk-simple-btns">
                    <button type="button">${i18n.button.cancel}</button>
                    <button type="button">${i18n.button.accept}</button>
                </div>
              </div>
            </div>
          </div>
        </body>`;
}

function createViewInHtml() {

  let url = context?.othersInfo?.url;
  const html = `
    <!--main-->
    <div role="main">

        <div class="cd-sdk-nav-container${!url || url === '' ? '-hidden' : ''}">
          <a href="${url}" target="_self" class="cd-sdk-nav-btn icon-back">${i18n.button.backToHome}</a>
        </div>

        <div  class="cd-sdk-search-container">
          <input type="text" id="txt-search" name="txt-search" class="search-form-txt" placeholder="${i18n.merchants.search}">
          <button type="button" id="btn-search" name="btn-search" class="btn-search"><span class="icon-search"></span></button>
        </div>
         
        <!--list-->
        <div class="cd-sdk-merchants-container" id="cd-sdk-merchants-container">
        </div>
        <!--END OF list-->
    </div>`;

  let container = document.querySelector('.cd-sdk-carddynamics div.cd-sdk-enrollment');
  container.innerHTML += html;
}

function paintMerchantList(list) {
  let container = document.getElementById('cd-sdk-merchants-container');
  if (list && list.length > 0) {
    list.forEach(group => {
      // Add title group
      if (group.merchants.length > 0 && list.length >= 1) {

        let items = "";
        group.merchants.forEach(merchant => {
          // Only set merchant if the promotion list is empty
          if (!merchant.promotions || merchant.promotions.length <= 0) {
            items += paintItemInList(merchant);
          } else {
            merchant.promotions.forEach(promo => {
              items += paintItemInList(merchant, promo);
            });
          }
        });

        let merchantType = "";
        if(group.merchantType){
          merchantType = `<h2>${group.merchantType.toUpperCase()}</h2>`;
        }

        container.innerHTML += `
        <div class="cd-sdk-merchants-category-container">
          ${merchantType}
          <div class="cd-sdk-merchants-cards-container">
            ${items}
          </div>
        </div>
        `;
      }

      group.merchants.forEach(merchant => {
        // Only set merchant if the promotion list is empty
        if (!merchant.promotions || merchant.promotions.length <= 0) {
          paintItemInList(merchant);
        } else {
          merchant.promotions.forEach(promo => {
            paintItemInList(merchant, promo);
          });
        }
      });
    });
  }
  else {
    container.innerHTML = `
      <p class="empty-list">${i18n.merchants.noResults}</p>
    `;
  }

  hideSpinner();
}


function paintItemInList(merchant, promo = null) {
  return `
  <div class="cd-sdk-merchant-card">
    <div class="cd-sdk-merchant-card-content">
      <img src="${merchant.merchantDetails.merchantLogo}" alt="" class="card-logo">
      <div class="cd-sdk-merchant-card-detail">
        <h3 class="cd-sdk-merchant-card-content-title">${merchant.merchantDetails.merchantBrand}</h3>
        <p class="cd-sdk-merchant-card-content-highlight">${merchant.merchantDetails.enrollmentTitle}</p>
        <p class="cd-sdk-merchant-card-content-desc">${merchant.merchantDetails.enrollmentShort}</p>
        <a class="cd-sdk-merchant-card-content-link button-detail web" data-merchantId="${merchant.cdMerchantId}" data-promoId="${promo ? promo.promoId : 'no-promo'}">${i18n.enrollment.merchants.details}</a>
      </div>
    </div>
    <button type="button" data-merchantId="${merchant.cdMerchantId}" data-promoId="${promo ? promo.promoId : 'no-promo'}" name="btn-apply" class="cd-sdk-merchant-card-content-link button-detail mobile">${i18n.enrollment.merchants.details}</button>
    <button type="button" data-merchantId="${merchant.cdMerchantId}" data-promoId="${promo ? promo.promoId : 'no-promo'}" name="btn-apply" class="btn-card">${i18n.button.register}</button>
  </div>`;
}

function setEventListeners() {
  // Detail buttons listeners
  clickDetailBtn();
  // Apply buttons listeners
  clickApplyBtn();
  // Reactive searcher
  const searcher = document.getElementById('txt-search');
  searcher.addEventListener('keyup', delay((e) => {
    if (e.target.value === undefined || e.target.value === null || e.target.value === '') {
      filteredList = originalMerchantList;
    } else {

      console.log('event', 'Buscar_Merchant', {
        'event_category': 'Lista_merchants',
        'event_label': 'Búsqueda'
      });
      filteredList = originalMerchantList.map(group => {
        return {
          merchantType: group.merchantType,
          merchants: group.merchants.filter(merchant => {
            const txt = e.target.value.toLowerCase();
            const title = merchant.merchantDetails.enrollmentTitle.toLowerCase();
            const brand = merchant.merchantDetails.merchantBrand.toLowerCase();
            return title.includes(txt) || brand.includes(txt);
          })
        };
      });
    }
    // Remove previous childs
    const container = document.getElementById('cd-sdk-merchants-container');
    container.innerHTML = '';
    // Paint new list
    paintMerchantList(filteredList);
    // Detail buttons listeners
    clickDetailBtn();
    // Apply buttons listeners
    clickApplyBtn();
  }, 350));
}

function clickDetailBtn() {
  const detailButton = document.querySelectorAll(".button-detail");
  for (let i = 0; i < detailButton.length; i++) {
    detailButton[i].addEventListener('click', e => {
      const merchantId = e.target.getAttribute('data-merchantId');
      const promoId = e.target.getAttribute('data-promoId');
      openMerchantDetails(merchantId, promoId);
    });
  }
}

function clickApplyBtn() {
  const applyMerchant = document.querySelectorAll(".btn-card");
  for (let i = 0; i < applyMerchant.length; i++) {
    applyMerchant[i].addEventListener('click', (e) => {
      let { paymentMethods } = context;
      if (paymentMethods) {
        const merchantId = e.target.getAttribute('data-merchantId');
        const promoId = e.target.getAttribute('data-promoId');
        openRegister(context, merchantId, promoId);
      } else {
        openModal({text: i18n.error.registerWithoutCard}, 'error');
      }
    });
  }
}

function openMerchantDetails(merchantId, promoId) {
  // Show merchant detail view
  const merchantSelected = getMerchantByPromoId(merchantId, promoId);
  if (merchantSelected) {
    showSpinner(i18n.spinner.loading);
    addDetailMerchantView(context, merchantSelected);
  }
}

function openRegister(context, merchantId, promoId) {
  // Show merchant register view
  const merchantSelected = getMerchantByPromoId(merchantId, promoId);
  // localStorage.setItem('merchantSelected',JSON.stringify(merchantSelected));
  if (merchantSelected) {

    showSpinner(i18n.spinner.loading);
    context.merchant = merchantSelected;
    let passwordInput = merchantSelected.form.find(x => x.issuerFieldsName === 'password');

    if(context.button && passwordInput){
      addRegisterPasswordView(context, validMerchantId, '');
    }
    else if(context.button && context.loginDone){
      document.addEventListener('login-result', function (e) {
        let { detail } = e;
        Object.assign(context, detail, { resultDone: true });
        addRegisterMerchantView(context, '');
      }, { once: true });
    }
    else {
      addRegisterMerchantView(context, '');
    }
  }
}

function getMerchantByPromoId(merchantId, promoId) {
  let merchantSelected;
  for (const group of originalMerchantList) {
    for (const merchant of group.merchants) {
      if (promoId !== 'no-promo') {
        let promoSelected = null;
        if(merchant.promotions && merchant.promotions.length > 0){
          promoSelected =merchant.promotions.find(promo => '' + promo.promoId === '' + promoId);
        }

        if (promoSelected && merchant.cdMerchantId.toString() === merchantId.toString()) {
          merchantSelected = merchant;
          merchantSelected.promoSelected = promoSelected;
          break;
        }
      } else if (merchant.cdMerchantId === +merchantId) {
        merchantSelected = merchant;
        break;
      }
    }
    if (merchantSelected) {
      break;
    }
  }
  return merchantSelected;
}

function delay(callback, ms) {
  var timer = 0;
  return function () {
    var context = this,
      args = arguments;
    clearTimeout(timer);
    timer = setTimeout(function () {
      callback.apply(context, args);
    }, ms || 0);
  };
}