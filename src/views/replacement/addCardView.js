import locale from '../../config/locale/i18n';
import { mask, removeSpace } from '../../helpers/generic.helper';
import SdkService from '../../services/sdk.service';
import { hideSpinner, openModal, showSpinner } from '../../common/sdk.common';
import addMerchantListView from './addMerchantListView';
import { decryptString } from '../../helpers/crypto.helper';

import '../../assets/style/common.scss';
import '../../assets/style/replacement.scss';
import '../../assets/style/common.responsive.scss';
import '../../assets/style/replacement.responsive.scss';
import SdkException from '../../common/sdk-exception';
import { initViewHtml } from '../../helpers/component.helper';
import addScannerView from './addScannerView';

let context, i18n;

export default async function addCardView(ctx) {

  try{
    context = ctx;
    let { data, settings: { language, issuerId } } = ctx;
    i18n = locale(language);

    context.step = 1;

    initViewHtml(i18n);

    let sdkService = new SdkService();
    let sdkResult = await sdkService.getIssuerSKey(issuerId);

    if (sdkResult.status == 200) {
      let sdkData = Buffer.from(sdkResult.data, 'base64').toString('utf-8');

        Object.assign(context.settings, JSON.parse(sdkData));

        // Replacement with data
        if (data) {

            try{
              const replacementData = decryptString(data, context.settings.secret_key);
              if(typeof replacementData === 'string') replacementData = JSON.parse(replacementData);
              Object.assign(context, replacementData);
            }
            catch(err){
              throw new SdkException(i18n.error.decryptData);
            }
        }

        init();

        createViewHtml();
        setEventListeners();
        addMerchantListView(context);


    }
    else{
      console.log("sdkResult", sdkResult)
      throw sdkResult.data;
    }
  }
  catch(err){
    console.log(err);
    hideSpinner();
    if(err instanceof SdkException || (err && err.message)){
      openModal({ text: err.message }, 'error');
    }
    else{
      openModal({text: i18n.error.generic }, 'error');
    }
  }
}

function init() {

  let cssOverride = context?.othersInfo?.cssOverride;
  let header = context?.othersInfo?.header;
  let loader = context?.othersInfo?.loader

  let cssStyle = "";
  if (cssOverride){
    
    cssStyle = "<style>";
    if(cssOverride.colors && Object.keys(cssOverride.colors).length > 0 ) {
      let colorValues = "";

      if(cssOverride.colors.primary && !cssOverride.colors.accent){
          cssOverride.colors.accent = cssOverride.colors.primary;
      }

      for (const [key, value] of Object.entries(cssOverride.colors)) {
        colorValues += `--${key}: ${value};`;
      }

      cssStyle += `:root { ${ colorValues }}`;
    }

    if(cssOverride.style){
      cssStyle += cssOverride.style;
    }
    cssStyle += "</style>";
  }

  let hcontent = `<h1>${i18n.replacement.title}</h1>`;
  if (header?.logo) {
    hcontent = `<img src="${header?.logo}">`;
  }
  else if (header?.text) {
    hcontent = `<h1>${header?.text}</h1>`;
  }

  let mobileLoader = "assets/img/loading-mobile.gif";
  let webLoader = "assets/img/loading-web.gif";
  let textVisibilityStyle = "";
  let loaderClasses = "";

  if(loader){
    if(loader.mobile){
      mobileLoader = loader.mobile;
    }
    if(loader.web){
      webLoader = loader.web;
    }

    if(loader.textVisibility === false){
      textVisibilityStyle = `style="display: none;"`;
    }

    if(loader.box === false){
      loaderClasses = 'cd-sdk-spinner-without-box';
    }

  }

  document.body.outerHTML = `
        ${cssStyle}
        <body class="cd-sdk-carddynamics">
     
          <div class="cd-sdk-replacement">
            <header role="banner" style="color: ${header?.color || 'white'}; background: ${header?.background || 'var(--primary)'}; border-bottom: 1px solid var(--primary);">
              <div class="cd-sdk-header-wrapper">${hcontent}</div>
            </header>
            <div role="main"></div>
          </div>

          <div style="display: flex;" class="cd-sdk-spinner ${loaderClasses}">
            <div class="cd-sdk-spinner-container">
              <img class="mobile" src="${mobileLoader}" />
              <img class="web" src="${webLoader}" />
              <div ${textVisibilityStyle}>${i18n.spinner.loading}</div>
            </div>
          </div>

          <div style="display: none" class="cd-sdk-popup cd-sdk-alerts">
            <div class="cd-sdk-popup-container">
              <span class="cd-sdk-popup-icon"></span>
              <div class="cd-sdk-popup-text"></div>
              <div class="cd-sdk-simple-btns">
                  <button type="button">${i18n.button.cancel}</button>
                  <button type="button">${i18n.button.accept}</button>
              </div>
            </div>
          </div>
        </body>`;
}

function createViewHtml() {

  let { oldCard } = context;

  context.replacementReason = "OC";

  let ocHtml = "";
  let cardsTitle = `<h3 class="cd-sdk-card-description">${i18n.replacement.cardDetail.title}</h3>`;
  if (oldCard) {
    context.replacementReason = "OP";
    ocHtml = paintOldCardView();
  }
  else {
    cardsTitle = `<h3 class="cd-sdk-card-description cd-sdk-cards">${i18n.replacement.cardForm.title}</h3>`;
    ocHtml = paintFormOldCardView();
  }

  let html = `
        <!--main-->
        <div role="main">

            <!-- Cards Container-->
            ${cardsTitle}
            <div class="cd-sdk-card-container">
              ${ocHtml}
              ${paintNewCardView()}
            </div>

            <div class="cd-sdk-merchants-container">
            </div>
        </div>`;

  let conatiner = document.querySelector('body.cd-sdk-carddynamics div.cd-sdk-replacement div[role=main]');
  conatiner.outerHTML = html;
}

function paintOldCardView() {
  let { oldCard: { pan, expDate, cardBackground } } = context;

  if (pan) {
    pan = cardMask(pan);
    if (pan[0] == 5) {
      if (!cardBackground) cardBackground = 'https://cd-logos.s3.amazonaws.com/cards/default-mastercard.png';
    }
    else if (pan[0] == 4) {
      if (!cardBackground) cardBackground = 'https://cd-logos.s3.amazonaws.com/cards/default-visa.png';
    }
  }

  return `
  <div class="cd-sdk-credit-card">
      <h2 class="card-title">${i18n.replacement.cardDetail.previousCard}</h2>
      <div class="card-box-container" style="background: url(${cardBackground || 'https://cd-example-cards.s3.eu-west-1.amazonaws.com/bg-card-default.png'}); background-size: cover;">
          <div class="card-box-data">     
            <span>${mask(removeSpace(pan), '*', 4, 0, 4)}</span>
            <span>${expDate}</span>
          </div>
      </div>
  </div>
  `;
}

function paintFormOldCardView() {

  let blinkCardResult = context.blinkCardResult;
  let cardNumber = "";
  let expDate = "";
  if(blinkCardResult){
    if(blinkCardResult.cardNumber) cardNumber = cardMask(blinkCardResult.cardNumber);
    if(blinkCardResult.expiryDate.year && blinkCardResult.expiryDate.month) {
      if(blinkCardResult.expiryDate.month.length === 1){
        blinkCardResult.expiryDate.month = `0${blinkCardResult.expiryDate.month}`;
      }

      expDate = `${blinkCardResult.expiryDate.month}/${(blinkCardResult.expiryDate.year).toString().substring(2)}`;
    }
  }

  return `
  <div class="cd-sdk-card-form">
    <div class="cd-sdk-credit-card">
        <h2 class="card-title">${i18n.replacement.cardForm.oldCard}</h2>
        <div class="card-box-container" <div class="card-box-container" style="background: url(https://cd-example-cards.s3.eu-west-1.amazonaws.com/bg-card-default.png);">
            <div class="card-box-data">     
              ${i18n.replacement.cardForm.scanCard}
            </div>
        </div>
    </div>

    <form autocomplete="off" class="active">
      <div class="cd-sdk-card-form-row">
        <label for="txt-number" class="cd-sdk-card-form-label">${i18n.replacement.cardForm.cardNumber}</label>
        <input   type="text" value="${cardNumber}"  id="txt-number" name="txt-number" class="cd-sdk-card-form-txt" placeholder="0000 0000 0000 0000" minlength="13" maxlength="19" required>
      </div>
      <div class="cd-sdk-card-form-row">
        <label for="txt-expiry" class="cd-sdk-card-form-label">${i18n.replacement.cardForm.expirationDate}</label>
        <input type="text" value="${expDate}" id="txt-expiry" name="txt-expiry" class="cd-sdk-card-form-txt" placeholder="${i18n.replacement.cardForm.expiryHint}" minlength="5" maxlength="5" required>
      </div>
      <div class="checkbox-bl-type2">
        <input type="checkbox" class="card-owner" name="owner" id="owner" value="false" required>
        <label for="owner" class="checkbox-label">${i18n.replacement.cardForm.owner}</label>
      </div>

      <div class="cd-sdk-simple-btns">
        <button id="btn-add" name="btn-add" class="form-btn" disabled>${i18n.button.add}</button>
      </div>
    </form>
  </div>`;
}

function paintNewCardView() {

  let { newCard: { expDate, pan, cardBackground } } = context;

  if (pan) {
    if (pan[0] == 5) {
      if (!cardBackground) cardBackground = 'https://cd-logos.s3.amazonaws.com/cards/default-mastercard.png'
    }
    else if (pan[0] == 4) {
      if (!cardBackground) cardBackground = 'https://cd-logos.s3.amazonaws.com/cards/default-visa.png';
    }
  }

  return `
    <div class="cd-sdk-arrows-container">
        <div class="arrows2">
          <span></span>
          <span></span>
        </div>
    </div>
    <div class="cd-sdk-credit-card">
    <h2 class="card-title">${i18n.replacement.cardDetail.newCard}</h2>
    <div class="card-box-container" style="background: url(${cardBackground || 'https://cd-example-cards.s3.eu-west-1.amazonaws.com/bg-card-default.png'}); background-size: cover;">
        <div class="card-box-data">     
          <span>${mask(removeSpace(pan), '*', 4, 0, 4)}
          </span>
          <span>${expDate}</span>
        </div>
    </div>
</div>`;
}

function setEventListeners() {

  const oldCardContainer = document.querySelector('.cd-sdk-card-form .cd-sdk-credit-card');
  const oldCardFormContainer = document.querySelector('.cd-sdk-card-form form');


  oldCardContainer?.addEventListener('click', function () {
    if (context.step === 1) {

      showSpinner();
      addScannerView(context);
    }
  });

  let form = document.querySelector('.cd-sdk-card-form form');
  if (form) {
    form.addEventListener('change', function () {
      let valid = validForm();
      const sendButton = document.getElementById("btn-add");
      const updateBtn = document.querySelector('.cd-sdk-simple-btns button#update-btn');
      if (valid) {
        sendButton.removeAttribute("disabled");
      } else {
        sendButton.setAttribute("disabled", "disabled");
        if(updateBtn) updateBtn.classList.remove("step-1__valid");
      }
    });
  }


  let expiryInput = document.getElementById("txt-expiry");
  expiryInput?.addEventListener("input", function (e) {

    let el = e.currentTarget;
    let value = el.value;
    if(isNaN(e.data)) {
        el.value = value.substring(0, value.length - 1);
        return;
    }

    if (value && value.length === 2) {
      el.value = `${value}/`
    }
  })

  let numberInput = document.getElementById("txt-number");
  numberInput?.addEventListener('input', (e) => {

    let value = e.target.value;
    if (e.data && value && value.length < 16) {
      let copyValue = value.slice();
      if (removeSpace(copyValue).length % 4 === 0) {
        numberInput.value = `${value} `;
      }
    }

    let cardContainer = document.querySelector(".cd-sdk-credit-card .card-box-container");

    if (value.length === 1) {

      if (value == 4) { //Visa 
        cardContainer.style.background = 'url(https://cd-logos.s3.amazonaws.com/cards/default-visa.png)';
      }
      else if (value == 5) { //Mastercard
        cardContainer.style.background = 'url(https://cd-logos.s3.amazonaws.com/cards/default-mastercard.png)';
      }
      else {
        cardContainer.style.background = 'url(https://cd-example-cards.s3.eu-west-1.amazonaws.com/bg-card-default.png)';
      }

      cardContainer.style.backgroundSize = 'cover';
    }

  });

  const addButton = document.getElementById("btn-add");
  addButton?.addEventListener('click', (e) => {

    e.preventDefault();


    let pan = removeSpace(numberInput.value);
    let expDate = expiryInput.value;

    if (parseInt(expDate, 10) < 1 || parseInt(expDate, 10) > 12) {
      openModal({ text: i18n.error.wrongMonth }, 'error');
      return;
    }

    context.oldCard = {
      pan,
      expDate
    }

    let cardBoxData = document.querySelector('.cd-sdk-card-form .cd-sdk-credit-card .card-box-data');
    cardBoxData.outerHTML = `
      <div class="card-box-data">     
        <span>${mask(removeSpace(pan), '*', 4, 0, 4)}</span>
        <span>${expDate}</span>
      </div>`;

    oldCardFormContainer.classList.toggle('active');

    const updateBtn = document.querySelector('.cd-sdk-simple-btns button#update-btn');
    if(updateBtn) updateBtn.classList.add("step-1__valid");
  });
}

function cardMask(value) {
  let resultValue = '';
  Array.from(value).forEach(char => {
    resultValue += char;
    if (removeSpace(resultValue).length % 4 === 0) {
      resultValue = `${resultValue} `;
    }
  });

  return resultValue;
}

function validForm() {
  let valid = true;
  let inputs = document.querySelectorAll('.cd-sdk-card-form form input');
  inputs.forEach(function (input) {
    switch (input.type) {
      case 'text': {
        let value = input.value;

        let length = removeSpace(value).length;
        if (length > input.maxLength || length < input.minLength) {
          input.parentElement.classList.add('error');
          valid = false;
        }
        else {
          input.parentElement.classList.remove('error');
        }
        break;
      }
      case 'checkbox': {
        if (!input.checked) {
          input.parentElement.classList.add('error');
          valid = false;
        }
        else {
          input.parentElement.classList.remove('error');
        }
      }
    }
  });

  return valid;
}