import locale from '../../config/locale/i18n';
import { hideSpinner, openModal } from '../../common/sdk.common';


import '../../assets/style/replacement.scss';

import SdkException from '../../common/sdk-exception';

import * as BlinkCardSDK from "@microblink/blinkcard-in-browser-sdk";
import addCardView from './addCardView';


let context, i18n;

let cameraFeed, cameraFeedback, drawContext; 

export default async function addScannerView(ctx) {

  try{
    context = ctx;
    let { data, settings: { language, issuerId } } = ctx;
    i18n = locale(language);

    createViewHtml();
    setEventListeners();


  }
  catch(err){
    console.log(err);
    hideSpinner();
    if(err instanceof SdkException || (err && err.message)){
      openModal({ text: err.message }, 'error');
    }
    else{
      openModal({text: i18n.error.generic }, 'error');
    }
  }
}


function createViewHtml() {
  document.body.outerHTML = `
    <body class="cd-sdk-scanner">
      <div id="screen-scanning" class="hidden">
          <video id="camera-feed" playsinline></video>
          <canvas id="camera-feedback"></canvas>
      </div>
    </body>`;
}



function setEventListeners() {

  // UI elements for scanning feedback
  cameraFeed = document.getElementById("camera-feed");
  cameraFeedback = document.getElementById("camera-feedback");
  drawContext = cameraFeedback.getContext("2d");


  // Check if browser is supported
  if (!BlinkCardSDK.isBrowserSupported() ){
    alert( "This browser is not supported by the SDK!" );
    return;
  }

  let key = "sRwAAAYJbG9jYWxob3N0r/lOPgo/w35CpOFWKOIYxy5dsX44CdWJKoRaEk3xJClQ53IRaHazWPA1Or8OEQ7XWiYSd6BBjBV5VyqclfEwGeHbsiTpY/BGIkLjWNJ4PnYfz9RN/GpEH5s9GSue9ZKePjUREUufvXFs2nwzxFJwkRg9zvlLuv4znY+dpaJFFAy28iLShWpshgM=";
  if(window.location.hostname == 'integrado.card-dynamics.io'){
    key = "sRwAAAYaaW50ZWdyYWRvLmNhcmQtZHluYW1pY3MuaW8YC+oG4Z6otR+scLh9s+dGLkoCdvNUyfoRWPwhGMR1+xTjqzSmXc8Hn5h7uOec0qMPAG3gFmv9umxCGXv1CQUGXB9YssiuCzl8oUEiNxru6u4yr52QU1bMHZdTUZZVrApGHqpeKkuC4kRgMbNWPGFcfi46sw2leX/WhDj6Hskbd7ohy+FoytF7qA=="
  }
  const loadSettings = new BlinkCardSDK.WasmSDKLoadSettings( key );

  loadSettings.engineLocation = "../../assets/resources/";
  loadSettings.workerLocation = "../../assets/resources/BlinkCardWasmSDK.worker.min.js";

  BlinkCardSDK.loadWasmModule(loadSettings).then(
    (sdk) => {
      startScan(sdk);
    },
    (error) => {
      console.log("Failed to load SDK!", error);
      addCardView(context);
    });
}

/**
 * Scan payment card.
 */
async function startScan(sdk)
{

  document.getElementById("screen-scanning")?.classList.remove("hidden");

  const blinkCardRecognizer = await BlinkCardSDK.createBlinkCardRecognizer(sdk);

  const callbacks = {
    onQuadDetection: (quad) => drawQuad(quad),
    onFirstSideResult: async () => {
      
      const blinkCardResult = await blinkCardRecognizer.getResult();
      if (blinkCardResult.state !== BlinkCardSDK.RecognizerResultState.Empty){

        const firstAndLastName = blinkCardResult.owner;
        const cardNumber = blinkCardResult.cardNumber;
        const dateOfExpiry = {
          year: blinkCardResult.expiryDate.year,
          month: blinkCardResult.expiryDate.month
        };

        context.blinkCardResult = blinkCardResult;
      }

      addCardView(context);
    }
  };

  const recognizerRunner = await BlinkCardSDK.createRecognizerRunner(sdk, [blinkCardRecognizer], false, callbacks);

  const videoRecognizer = await BlinkCardSDK.VideoRecognizer.createVideoRecognizerFromCameraStream(cameraFeed, recognizerRunner);

  await videoRecognizer.recognize();

  videoRecognizer?.releaseVideoFeed();
  recognizerRunner?.delete();
  blinkCardRecognizer?.delete();
  clearDrawCanvas();
}

/**
 * Utility functions for drawing detected quadrilateral onto canvas.
 */
function drawQuad(quad)
{
  clearDrawCanvas();

  // Based on detection status, show appropriate color and message
  setupColor(quad);

  applyTransform(quad.transformMatrix);
  drawContext.beginPath();
  drawContext.moveTo(quad.topLeft.x, quad.topLeft.y);
  drawContext.lineTo(quad.topRight.x, quad.topRight.y);
  drawContext.lineTo(quad.bottomRight.x, quad.bottomRight.y);
  drawContext.lineTo(quad.bottomLeft.x, quad.bottomLeft.y);
  drawContext.closePath();
  drawContext.stroke();
}

/**
 * This function will make sure that coordinate system associated with detectionResult
 * canvas will match the coordinate system of the image being recognized.
 */
function applyTransform(transformMatrix)
{
  const canvasAR = cameraFeedback.width / cameraFeedback.height;
  const videoAR = cameraFeed.videoWidth / cameraFeed.videoHeight;

  let xOffset = 0;
  let yOffset = 0;
  let scaledVideoHeight = 0;
  let scaledVideoWidth = 0;

  if (canvasAR > videoAR)
  {
    // pillarboxing: https://en.wikipedia.org/wiki/Pillarbox
    scaledVideoHeight = cameraFeedback.height;
    scaledVideoWidth = videoAR * scaledVideoHeight;
    xOffset = (cameraFeedback.width - scaledVideoWidth) / 2.0;
  } else

  {
    // letterboxing: https://en.wikipedia.org/wiki/Letterboxing_(filming)
    scaledVideoWidth = cameraFeedback.width;
    scaledVideoHeight = scaledVideoWidth / videoAR;
    yOffset = (cameraFeedback.height - scaledVideoHeight) / 2.0;
  }

  // first transform canvas for offset of video preview within the HTML video element (i.e. correct letterboxing or pillarboxing)
  drawContext.translate(xOffset, yOffset);
  // second, scale the canvas to fit the scaled video
  drawContext.scale(
  scaledVideoWidth / cameraFeed.videoWidth,
  scaledVideoHeight / cameraFeed.videoHeight);


  // finally, apply transformation from image coordinate system to
  // https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/setTransform
  drawContext.transform(
  transformMatrix[0],
  transformMatrix[3],
  transformMatrix[1],
  transformMatrix[4],
  transformMatrix[2],
  transformMatrix[5]);

}

function clearDrawCanvas()
{
  cameraFeedback.width = cameraFeedback.clientWidth;
  cameraFeedback.height = cameraFeedback.clientHeight;

  drawContext.clearRect(0, 0, cameraFeedback.width, cameraFeedback.height);
}

function setupColor(displayable)
{
  let color = "#FFFF00FF";

  if (displayable.detectionStatus === 0)
  {
    color = "#FF0000FF";
  } else
  if (displayable.detectionStatus === 1)
  {
    color = "#00FF00FF";
  }

  drawContext.fillStyle = color;
  drawContext.strokeStyle = color;
  drawContext.lineWidth = 5;
}
