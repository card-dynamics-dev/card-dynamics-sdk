import axios from 'axios';
import { hideSpinner, openModal, showSpinner } from '../../common/sdk.common';
import locale from '../../config/locale/i18n';
import { encryptObject, encryptObjectValues, integrityCheck } from '../../helpers/crypto.helper';
import { mask, removeSlash, removeSpace } from '../../helpers/generic.helper';
import ReplacementService from '../../services/replacement.service';


let selectedItems = [];
let availableMerchantList = [];
let checkedByDefault = false;

let context, i18n;
export default async function addMerchantListView(ctx) {

    context = ctx;
    let { settings, settings: { language }} = ctx;
    i18n = locale(language);

    try{

      context.replacementService = new ReplacementService(settings);
      availableMerchantList = await getAvailableMerchantList();

      createViewHtml();
      setEventListeners();
    }
    catch(err){
      console.log(err);
    }
}

function createViewHtml(){

  let html = `<div class="cd-sdk-merchants-container">
  <form>`;
  if(!checkedByDefault){
    html += `

    <h3 class="cd-sdk-card-description">${i18n.replacement.merchants.title}</h3>

    <div  class="cd-sdk-search-container">
        <input type="text" id="txt-search" name="txt-search" class="search-form-txt" placeholder="${i18n.merchants.search}">
        <button type="button" id="btn-search" name="btn-search" class="btn-search"><span class="icon-search"></span></button>
    </div>
    
      <div class="checkbox-bl-type2 check-all">
        <input type="checkbox" id="ch-all" class="checkAll" name="ch-all">
        <label for="ch-all" class="checkbox-label select-all" name="checkme">${i18n.replacement.merchants.selectAll}</label>
      </div>`;
  }

  html += `<fieldset class="merchants-list" id="merchants-list"></fieldset>
      <div class="cd-sdk-simple-btns">
        <button id="update-btn" name="update-btn">${i18n.replacement.update}</button>
      </div>
    </form>
  </div>
  `;

  let conatiner = document.querySelector('body.cd-sdk-carddynamics div.cd-sdk-replacement div[role=main] div.cd-sdk-merchants-container');
  conatiner.outerHTML = html;

  if(availableMerchantList && availableMerchantList.length > 0){
    paintMerchantListViewHtml(availableMerchantList.slice()); 
  }
}

function paintMerchantListViewHtml(merchantList){
  let html = "";
  merchantList.forEach(merchant => {

    let icon = merchant.cdMerchantLogoUrl || 'assets/img/dummy.svg';
    let checked = selectedItems.some(x => x.cdMerchantId === merchant.cdMerchantId) ? "checked" : '';

    html += `
    <div class="checkbox-bl-type2">
      <input type="checkbox" id="${merchant.cdMerchantId}" class="checkbox" name="ch-${merchant.cdMerchantId}" ${checked}>
      <label for="${merchant.cdMerchantId}" class="checkbox-label">
        <img src="${icon}" alt="">
        ${merchant.cdMerchantBrandNames}
        <span class="spacer"></span>
      </label>
    </div>
    `;

  });

  const merchantListContainer = document.getElementById('merchants-list');
  merchantListContainer.innerHTML = html;

  setInputEventListeners();
}

function setInputEventListeners(){
  let checkboxInputs = document.querySelectorAll(`.checkbox`);
  checkboxInputs.forEach(function(input){
    input.addEventListener("click", function(e){
      onCheckInput(e.target);
    });
  });
}

function setEventListeners(){

  if(!checkedByDefault){

    let checkAll = document.getElementById("ch-all");
    checkAll?.addEventListener('change', function(e){
      e.preventDefault();
      let checked = e.target.checked;
      selectedItems = []
      if(checked){
        selectedItems = availableMerchantList.slice();
      }
      let checkboxInputs = document.querySelectorAll(`.checkbox`);
      for(let checkbox of checkboxInputs){
        checkbox.checked = checked;
      }
      btnSaveToggle(selectedItems.length)
    });

    let searchInput = document.getElementById("txt-search");
    searchInput?.addEventListener("input", () => {
      let merchantList = availableMerchantList.slice();
      merchantList = merchantList.filter(item => prepareStringForComparison(item.cdMerchantBrandNames).indexOf(prepareStringForComparison(searchInput.value)) > -1);


      if(merchantList.length === 0){
        let html = `
        <div class="nm-found">
            ${i18n.merchants.noResults}
        </div>`;
        const merchantListContainer = document.getElementById('merchants-list');
        merchantListContainer.innerHTML = html;
      }
      else{
        paintMerchantListViewHtml(merchantList);
      }
    });
  }

  let btnSave = document.getElementById("update-btn");
  btnSave.addEventListener('click', async function(e){
    e.preventDefault();
    context.step = 2;
    await change();

    
    let cardsTitle = document.querySelector('h3.cd-sdk-card-description.cd-sdk-cards');
    if(cardsTitle) cardsTitle.innerHTML = i18n.replacement.cardDetail.title;
  });

  const sendButton = document.getElementById("btn-send");
  if(sendButton){
    sendButton.addEventListener('click', async function(e){    
      btnSaveToggle(selectedItems.length)
      });
  }
 

  
   document.addEventListener('input', (e)=>{ 
    btnSaveToggle(selectedItems.length) 
  });



  btnSaveToggle(selectedItems.length)

}

function btnSaveToggle(selectedItems){
  let updateBtn = document.getElementById("update-btn");

  let { newCard, oldCard } = context;

  let { replacementReason } = context;
  if(replacementReason === "OP"){
    if(selectedItems > 0 && newCard && newCard.pan && newCard.expDate && oldCard && oldCard.pan && oldCard.expDate){
      if(!updateBtn.classList.contains('step-1__valid step-2__valid')){
        updateBtn.classList.add('step-1__valid', 'step-2__valid');
      }
    }
    else{
      updateBtn.classList.remove('step-1__valid', 'step-2__valid');
    }
  }
  else if(replacementReason === "OC"){
    if(selectedItems > 0){
      updateBtn.classList.add('step-2__valid');
    }
    else{
      updateBtn.classList.remove('step-2__valid');
    }
  }
}

function onCheckInput(el){
  if(el.checked){
    let item = availableMerchantList.find(x => x.cdMerchantId == el.id);
    if(item){
      item.responseCode = 999;
      selectedItems.push(item);
    }
  } 
  else{
    let index = selectedItems.findIndex(x => x.cdMerchantId  == el.id);
    selectedItems.splice(index, 1);
  }

  let checkAll = document.getElementById("ch-all");
  if(checkAll){
    checkAll.checked = availableMerchantList.length === selectedItems.length ? true : false;
  }

  btnSaveToggle(selectedItems.length);
}

/* *** File Management *** */
function getMerchantListFromCsvFile() {
  let textValue = getTextFromCsvFile();
  if(textValue){
    return textValue.split(";");
  }
  return [];
}

function getTextFromCsvFile() {
  const fileName = sessionStorage.getItem('fileName') + '.csv';
  const rawFile = new XMLHttpRequest();

  rawFile.open("GET", fileName, false);
  rawFile.onreadystatechange = function () {
    if (rawFile.readyState === 4) {
      if (rawFile.status === 200 || rawFile.status === 0) {
        return rawFile.responseText;
      }
    }
  };

  rawFile.send(null);
  return rawFile.responseText;
}
/* ***** *** Fin *** ***** */

function createMerchantListToUpdateViewHtml(merchantList){

  let html = "";

  if (merchantList && merchantList.length > 0) {
    merchantList.forEach(merchant => {

      html += `
      <div class="cd-sdk-merchant-state state-${merchant.responseCode}">
          <div>
            <img src="${merchant.cdMerchantLogoUrl || 'assets/img/dummy.svg' }" alt="">
            ${ merchant.cdMerchantBrandNames }
          </div>

          <div class="cd-sdk-merchant-state-value merchant-${merchant.cdMerchantId}">
            <span class="ic-state icon-pending"></span>
            <span class="state-desc">${i18n.replacement.state.pending}</span>
          </div>
      </div>`;
    });
  }
  else{
    html = '<p> No hay comercios. </p>';
  }


    const container = document.querySelector('div[role=main] div.cd-sdk-merchants-container');
    container.outerHTML = `<div class="cd-sdk-merchants-container">
      <h3 class="cd-sdk-card-description">${i18n.replacement.state.title}</h3>
      <div class="cd-sdk-merchants-state">
        ${html}
      </div>
    </div>`;
  
}

function updateMerchantListStateViwewHtml(merchantList){

  if (merchantList && merchantList.length > 0) {
    merchantList.forEach(merchant => {

      let stateHtml = getMerchantHtmlState(merchant);
      let stateContainer = document.querySelector(`.cd-sdk-merchant-state-value.merchant-${merchant.cdMerchantId}`);
      stateContainer.outerHTML = stateHtml;

    });
  }

}

function getMerchantHtmlState(merchant){

  let stateString;
  let stateClass;

  switch (merchant.responseCode && merchant.responseCode.toString()) {
    case "000":
      stateClass = "icon-updated";
      stateString = i18n.replacement.state.updated;
      break;
    case "302":
      stateClass = "icon-denied";
      stateString = i18n.replacement.state.denied;
      break;
    case "301":
    case "304":
    case "305":
    case "701":
    case "702":
    case "703":
    case "501": //fallos configuración 501 no conllevan reintentar
      stateClass = "icon-denied";
      stateString = i18n.replacement.state.denied;
      break;
    case "601": //psp no respondido (llamado al updateresult) en tiempo o PSP no responde llamada de replacement 601, processing que no conlleva reintentar
        stateClass = "icon-pending";
        stateString = i18n.replacement.state.pending;
        break;
    case "705": //fallos en psp  y conllevan reintentar
      stateClass = "icon-denied";
      stateString = i18n.replacement.state.denied;
      break;
    default:
      stateClass = "icon-pending";
      stateString = i18n.replacement.state.pending;
      break;
  }

  let stateHtml = "";
  if(merchant.responseCode){
    stateHtml = `<div class="cd-sdk-merchant-state-value merchant-${merchant.cdMerchantId}">
        <span class="ic-state ${stateClass}"></span>
        <span class="state-desc">${stateString}</span>
      </div>`;
  }

  return stateHtml;
}

async function getAvailableMerchantList(){

  let { replacementService, ids } = context;
  let merchantListResult = await replacementService.getMerchantList();

  if(!merchantListResult){
    openModal({ text: `Ooops! Vaya, algo salió mal al recuperar los comercios, inténtalo de nuevo.`}, 'error');
    return;
  }

  availableMerchantList = merchantListResult.data;

  if(merchantListResult.status != 200){
    openModal({ text: availableMerchantList.message || `Ooops! Vaya, algo salió mal al recuperar los comercios, inténtalo de nuevo.`}, 'error');
    return;
  }

  let customMerchantList = [];
  if (sessionStorage.getItem('fileName')) {
    customMerchantList = getMerchantListFromCsvFile();
    checkedByDefault = true;
  }
  else if(ids && ids.length > 0){
    customMerchantList = ids;
    checkedByDefault = true;
  }

  if(checkedByDefault){
    availableMerchantList = availableMerchantList.filter(x => customMerchantList.some(i => i == x.cdMerchantId));
    selectedItems = availableMerchantList.slice();
    checkedByDefault = true;
  }

  hideSpinner();
  return availableMerchantList;
}

async function change(){
    showSpinner(i18n.spinner.loading);
    let { newCard, oldCard, replacementService } = context;

    if(!(newCard && newCard.pan && newCard.expDate)){
      hideSpinner();
      openModal({ text: 'New card pan and expiry date is required.'}, 'warning');
      return;
    }

    if(!(oldCard && oldCard.pan && oldCard.expDate)){
      hideSpinner();
      openModal({ text: 'Old card pan and expiry date is required.' }, 'warning');
      return;
    }
    
    let data = await buildData(selectedItems, newCard, oldCard);
    let changeResult = await replacementService.change(data);
    let changeRequestData = changeResult.data;

    if(changeResult.status != 200){
      hideSpinner();
      openModal({ text: changeRequestData.message}, 'error');
      return;
    }

    createMerchantListToUpdateViewHtml(selectedItems);

    let { cdChangeRequestId } = changeResult.data;

    let interval = setInterval(async()=>{
      selectedItems = await checkStatus(cdChangeRequestId, interval);
      updateMerchantListStateViwewHtml(selectedItems);
    }, 1000);

    setTimeout(()=>{ clearInterval(interval) }, 60000); // límite de 1 minuto para llamar al checkStatus

    selectedItems = await checkStatus(cdChangeRequestId, interval);
    createMerchantListToUpdateViewHtml(selectedItems);

    hideSpinner();
}

async function buildData(merchantList, newCard, oldCard){

  let { replacementReason, replacementService, settings: { secret_key } } = context;
  const issuerChangeRequestId = Date.now();

  let requestData = {
    issuerChangeRequestId,
    ncExpDate:  removeSlash(newCard.expDate),
    ncPan: removeSpace(newCard.pan),
    ocExpDate: removeSlash(oldCard.expDate),
    ocPan: removeSpace(oldCard.pan),
    merchantList,
    replacementReason
  }

  if(replacementReason === "OC"){
      let newResult = await replacementService.new();
      let newResultData = newResult.data;

      if(newResult.status != 200){
        openModal({ text: newResultData.message}, 'error');
        return;
      }

      let operationId = newResultData.operationId;
      let tokenResult = await replacementService.tokenize({ operationId, pan: requestData.ocPan });
      let tokenResultData = tokenResult.data;

      if(tokenResult.status != 200){
        openModal({ text: tokenResultData.message }, 'error');
        return;
      }

      delete requestData.ocPan;
      requestData.ocToken = tokenResultData.token;
  }

  requestData.integrityCheck = integrityCheck(requestData, ['ocToken','ocPan', 'ocExpDate', 'ncPan', 'ncExpDate']);

  return encryptObjectValues(requestData, secret_key, ['ocToken','ocPan', 'ocExpDate','ncPan', 'ncExpDate', 'merchantList'])

}

async function checkStatus(cdChangeRequestId, interval){

    let { replacementService } = context;
    let result = await replacementService.checkStatus(cdChangeRequestId);
    let resultData = result.data;

    if(result.status != 200){
      clearInterval(interval);
      sendResult('FAILURE', 'REPLACEMEMNT_CHECK_STATUS', resultData)
      openModal(`<p> Check Status Error: ${resultData.message}</p>`);
      return;
    }

    selectedItems.forEach(async function(item){
      item.responseCode = 999;
      let resItem = resultData.find(x => x.cdMerchantId == item.cdMerchantId);
      if(resItem){
        item.responseCode = resItem.responseCode;

        if(!item.sended){
          await sendResult('SUCCESS', 'REPLACEMEMNT_CHECK_STATUS', resItem, item)
        }

        item.sended = true;
      }
    });

    
    if(!selectedItems.some(i => i.responseCode === 999)){
      clearInterval(interval);
    }

    /*selectedItemsOk  = result.data.filter((el)=> el.responseCode === '000');

    if(selectedItemsOk.length === selectedItems.length){
        clearInterval(interval);
    }*/

    return selectedItems;
}

function sendResult(status, type, response, item){

  let { replacementService, replacementReason, newCard: { pan: ncPan, expDate: ncExpDate }, oldCard: { pan: ocPan, expDate: ocExpDate } } = context;

  let customerId = context?.settings?.customerId;
  let secret_key = context?.settings?.secret_key;
  let urlResponse = context?.othersInfo?.urlResponse;

  let data = { 
    type,
    customerId,
    response,
    status,
    ncPan: mask(ncPan, '*', 4, 4, 6),
    ncExpDate,
    ocPan: mask(ocPan, '*', 4, 4, 6),
    ocExpDate,
    replacementReason
  };

  if(item){
    Object.assign(data, {
      cdMerchantId: item.cdMerchantId,
      cdMerchantBrandNames: item.cdMerchantBrandNames
    })
  }

  data = { replacementData: encryptObject(data, secret_key), url: urlResponse }

  return replacementService.sendResult(data);
}

function prepareStringForComparison(string) {
  return (string)
    ? string.toLocaleLowerCase().normalize('NFD')
    .replace(/\s/g,'').replace('-','').normalize()
    : '';
};

