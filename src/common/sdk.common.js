const { copyContentToClipboard } = require("../helpers/generic.helper");

let interval;
const sdkCommon = {
    showSpinner: (message = null) => {
        let spinner = document.querySelector('div.cd-sdk-spinner');
        if (spinner) {

            let textDiv = document.querySelector('div.cd-sdk-spinner div.cd-sdk-spinner-container div');
            
            if(message && typeof message === "string"){
                textDiv.innerHTML = message;
                textDiv.style.display = 'flex';
            }
            else if(message && message instanceof Array && message.length > 0){
                let index = 0;
                textDiv.innerHTML = message[index];
                interval = setInterval(() => {

                    index += 1;
                    textDiv.innerHTML = message[index];

                    if(index === message.length - 1) clearInterval(interval);

                }, 5000);
                textDiv.style.display = 'flex';
            }
            else{
                textDiv.style.display = 'none';
            }

            spinner.style.display = 'flex';
        }
    },

    hideSpinner: () => {
        let spinner = document.querySelector('div.cd-sdk-spinner');
        if (spinner) {
            setTimeout(function(){ 
                spinner.style.display = 'none'; 
                clearInterval(interval);
            }, 500);
        }
    },

    openModal: (options, state = "success", type = 'info') => {

        let popup = document.querySelector('div.cd-sdk-popup.cd-sdk-alerts');
        if (popup && popup.style.display === 'none') {

            popup.style.display = 'flex';

            let icon = document.querySelector('div.cd-sdk-popup.cd-sdk-alerts span.cd-sdk-popup-icon');
            if(icon){
                switch(state){
                    case 'error': icon.className = `cd-sdk-popup-icon icon-close ${state}`; break;
                    case 'warning': icon.className = `cd-sdk-popup-icon icon-info ${state}`; break;
                    default: icon.className = `cd-sdk-popup-icon icon-updated ${state}`;
                }
            }

            let close = () => {
                let popup = document.querySelector('div.cd-sdk-popup.cd-sdk-alerts');
                popup.style.display = 'none'; 
            }

            let { text, okLabel, okFn, koLabel, koFn } = options;

            let textDiv = document.querySelector('div.cd-sdk-popup.cd-sdk-alerts div.cd-sdk-popup-text');
            if(textDiv && text) textDiv.innerHTML = text;


            let acceptBtn = document.querySelector('div.cd-sdk-popup.cd-sdk-alerts div.cd-sdk-simple-btns button:last-child');
            if(acceptBtn){
                if(okLabel) acceptBtn.innerHTML = okLabel;

                acceptBtn.addEventListener('click', () => {
                    if(okFn) okFn();
                    close();
                });
            }

            let cancelBtn = document.querySelector('div.cd-sdk-popup.cd-sdk-alerts div.cd-sdk-simple-btns button:first-child');
            if(cancelBtn){
                if(type === 'info'){ cancelBtn.style.display = 'none' }
                if(koLabel) cancelBtn.innerHTML = koLabel;

                cancelBtn.addEventListener('click', () => {
                    if(koFn) koFn();
                    close();
                });
            }
        }
    },

    openCongratsModal: (message, state = 'success') => {
        {

            let popup = document.querySelector('div.cd-sdk-popup.cd-sdk-congrats');
            if (popup && popup.style.display === 'none') {
        
                popup.style.display = 'flex';
        
                let icon = document.querySelector('div.cd-sdk-popup span.cd-sdk-popup-icon');
                if(icon){
                    switch(state){
                        case 'error': icon.className = `cd-sdk-popup-icon icon-close ${state}`; break;
                        case 'warning': icon.className = `cd-sdk-popup-icon icon-info ${state}`; break;
                        default: icon.className = `cd-sdk-popup-icon icon-updated ${state}`;
                    }
                }

                let text = document.querySelector('div.cd-sdk-popup-text');
                if(text && message){
                    text.innerHTML = message;
                }
        
                let copyBtn = document.querySelector('div.cd-sdk-input-box-container button#cd-sdk-btn-copy-txt');
                if (copyBtn) {
                    copyBtn.addEventListener('click', async function (e) {
                        let copyInput = document.querySelector('div.cd-sdk-input-box-container input#cd-sdk-input-copy-txt');
                        if (copyInput) {
                            await copyContentToClipboard(copyInput.value);
                        }
                    });
                }
            }
        }
    },

    loadFont: (fontUrl) => {
        let link = document.createElement('link');
        link.type = 'text/css';
        link.rel = 'stylesheet';

        document.head.appendChild(link);

        link.href = fontUrl;
    }
}

module.exports = sdkCommon