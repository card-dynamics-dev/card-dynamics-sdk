//const { default: SharedDataService } = require("../services/shared-data-service");
function paintFormInputs(inputs, fieldsInError, appearance) {
    let html = '';
    inputs.forEach(input => {
        switch (input.type) {
            case 'string':
            case 'email':
                html += createInputText(input, appearance);
                break;
            case 'number':
                html += createInputNumber(input, appearance);
                break;
            case 'password':
                html += createInputPassword(input);
                break;
            case 'phonenumber':
                html += createInputPhonenumber(input);
                break
            case 'integer':
                html += createInputInteger(input);
                break;
            case 'dropdown':
                html += createInputDropDown(input, fieldsInError);
                break;
            case 'select':
                html += createInputSelectButton(input);
                break;
            case 'date':
                html += createInputDate(input, dateFormat, fieldsInError);
                break;
            case 'boolean':
                html += createInputBoolean(input);
            default:
                return '';
        }
    });
    return html;
}

function isValidForm(inputs, fieldsInError, showInputError) {
    // Validate dynamic inputs
    let isValid = true;
    for (const input of inputs) {
        let isValidField = isValidInput(input, fieldsInError, showInputError);
        let el = document.getElementById(input.name.trim());
        if(el && !el.value) isValidField = false;
        if (isValidField === false) isValid = false;
    }

    // Validate checks
    let privacyCheck = document.getElementById('ch-privacy');
    //let termsCheck = document.getElementById('ch-terms');
    let additionalCheck = document.getElementById('ch-additional');

    if (privacyCheck && !privacyCheck.checked) isValid = false;
    if (additionalCheck && !additionalCheck.checked) isValid = false;

    return isValid;
}

function isValidInput(input, fieldsInError, showInputError = true) {
    const child = document.getElementById(input.name.trim());
    const parent = document.getElementById(`${input.name.trim()}-input-cont`);
    const tooltipLabel = document.getElementById(`tooltip-${input.name.trim()}`);
    const errorLabel = document.getElementById(`error-txt-${input.name.trim()}`);
    if (errorLabel) errorLabel.innerHTML = '';
    if (child && parent) {
        let regex = new RegExp(child.pattern);

        if ((child.pattern && !regex.test(child.value)) || //Regex
            (child.required && (!child.value || child.value === undefined || child.value === null || child.value === '')) || //required
            (child.value && child.min && child.max && (child.min > child.value.length || child.max < child.value.length))) //length
        {
            if (showInputError) parent.classList.add('cd-sdk-form-error');
            if (showInputError && tooltipLabel) tooltipLabel.classList.add('tooltip-error');
            if (showInputError && errorLabel && fieldsInError && fieldsInError[input.name]) errorLabel.innerHTML = fieldsInError[input.name];
            return false;
        }
        else {
            parent.classList.remove('cd-sdk-form-error');
            if (showInputError && tooltipLabel) tooltipLabel.classList.remove('tooltip-error');
            if (showInputError && errorLabel && fieldsInError && fieldsInError[input.name]) errorLabel.innerHTML = '';
            return true;
        }
    }
    return true;
}

function getFormValues(inputs) {
    let values = {};
    for (const input of inputs) {
        let inputName = input.name.trim();
        let inputEl = document.getElementById(inputName);
        if(inputEl && inputEl.type === 'checkbox'){
            values[inputEl.name] = inputEl.checked;
        }
        else if (inputEl && inputEl.value) {
            let value = inputEl.value;
            if (inputEl.type === "number") value = parseInt(value);
            values[inputEl.name] = value;
        }
    }
    return values;
}

function createInputText(input, appearance = null) {
    let value = input.default ? input.default : "";
    let required = input.required ? "required" : "";
    let tooltip = (!!input.help && input.help !== input.label) ? `<span class="cd-sdk-txt-info" id="tooltip-${input.name.trim()}">${input.help}</span>` : '';
    let outlineClass = (appearance === 'outline') ? 'cd-sdk-input-outline' : '';
    
    let html = `
  <div class="cd-sdk-form-row-account">
    <div class="cd-sdk-form-row-account-container ${outlineClass}" id="${input.name.trim()}-input-cont">
      <label for="${input.name.trim()}" class="cd-sdk-form-label">${input.label}</label>
      <input type="text" class="cd-sdk-form-txt" minlength="${input.min_size}" maxlength="${input.max_size}" value="${value}" placeholder="${input.placeholder}" id="${input.name.trim()}" name="${input.name.trim()}" pattern="${input.regex}" ${required} autocomplete="off">
    </div>
    ${tooltip}
    <span class="txt-error" id="error-txt-${input.name.trim()}"></span>
  </div>`;
    return html;
}

function createInputBoolean(input){

    let tooltip = (!!input.help && input.help !== input.label) ? `<span class="cd-sdk-txt-info" id="tooltip-${input.name.trim()}">${input.help}</span>` : '';
    let html = `
      <div class="cd-sdk-form-row-account">
        <div class="cd-sdk-form-row-account-container cd-sdk-form-row-account-inline" id="${input.name.trim()}-input-cont">
          <span class="cd-sdk-form-label">${input.label}</span>
          <label for="${input.name.trim()}" class="cd-sdk-switch">
            <input type="checkbox" id="${input.name.trim()}" name="${input.name.trim()}">
            <span class="cd-sdk-slider cd-sdk-round"></span>
          </label>
        </div>
        ${tooltip}
        <span class="txt-error" id="error-txt-${input.name.trim()}"></span>
      </div>`;
    return html;
}

function createInputNumber(input, appearance = null) {
    let tooltip = (!!input.help && input.help !== input.label) ? `<span class="cd-sdk-txt-info" id="tooltip-${input.name.trim()}">${input.help}</span>` : '';
    let outlineClass = (appearance === 'outline') ? 'cd-sdk-input-outline' : '';
    let html = `
      <div class="cd-sdk-form-row-account">
        <div class="cd-sdk-form-row-account-container ${outlineClass}" id="${input.name.trim()}-input-cont">
          <label for="${input.name.trim()}" class="cd-sdk-form-label">${input.label}</label>
          <input type="number" class="cd-sdk-form-txt" min="${input.min_size}" max="${input.max_size}" placeholder="${input.placeholder}" id="${input.name.trim()}" name="${input.name.trim()}" pattern="${input.regex}">
        </div>
        ${tooltip}
        <span class="txt-error" id="error-txt-${input.name.trim()}"></span>
      </div>`;
    return html;
}

function createInputInteger(input) {
    let regex = "^[0-9]*$"
    let required = input.required ? "required" : "";
    let tooltip = (!!input.help && input.help !== input.label) ? `<span class="cd-sdk-txt-info" id="tooltip-${input.name.trim()}">${input.help}</span>` : '';
    let html = `
      <div class="cd-sdk-form-row-account">
        <div class="cd-sdk-form-row-account-container" id="${input.name.trim()}-input-cont">
          <label for="${input.name.trim()}" class="cd-sdk-form-label">${input.label}</label>
          <input type="number" class="cd-sdk-form-txt" min="${input.min_size}" max="${input.max_size}" id="${input.name.trim()}" name="${input.name.trim()}" pattern="${regex}" ${required}>
        </div>
        ${tooltip}
        <span class="txt-error" id="error-txt-${input.name.trim()}"></span>
      </div>`;
    return html;
}

function createInputPhonenumber(input) {
    let regex = "^[0-9]*$";
    let required = input.required ? "required" : "";
    let tooltip = (!!input.help && input.help !== input.label) ? `<span class="cd-sdk-txt-info" id="tooltip-${input.name.trim()}">${input.help}</span>` : '';
    let html = `
      <div class="cd-sdk-form-row-account">
        <div class="cd-sdk-form-row-account-container" id="${input.name.trim()}-input-cont">
          <label for="${input.name.trim()}" class="cd-sdk-form-label">${input.label}</label>
          <input type="text" class="cd-sdk-form-txt" min="${input.min_size}" max="${input.max_size}" placeholder="${input.placeholder}" id="${input.name.trim()}" name="${input.name.trim()}" pattern="${regex}" ${required}>
        </div>
        ${tooltip}
        <span class="txt-error" id="error-txt-${input.name.trim()}"></span>
      </div>`;
    return html;
}

function createInputPassword(input) {
    let required = input.required ? "required" : "";
    let tooltip = (!!input.help && input.help !== input.label) ? `<span class="cd-sdk-txt-info" id="tooltip-${input.name.trim()}">${input.help}</span>` : '';
    let html = `
      <div class="cd-sdk-form-row-account">
        <div class="cd-sdk-form-row-account-container" id="${input.name.trim()}-input-cont">
          <label for="${input.name.trim()}" class="cd-sdk-form-label">${input.label}</label>
          <input type="password" class="cd-sdk-form-txt cd-sdk-form-pass" minlength="${input.min_size}" maxlength="${input.max_size}" placeholder="${input.placeholder}" id="${input.name.trim()}" name="${input.name.trim()}" pattern="${input.regex}" ${required}>
          <button type="button" data-input-id="${input.name.trim()}" class="icon-pass icon-show"></button>
        </div>
        ${tooltip}
        <span class="txt-error" id="error-txt-${input.name.trim()}"></span>
      </div>`;
    return html;
}

function createInputDropDown(input, fieldsInError) {
    let errorClass = '';
    let errorInfo = '';
    if (fieldsInError !== '') {
        const fields = Object.keys(fieldsInError);
        fields.forEach(field => {
            if (field.toString() === input.inputName.trim().toString()) {
                errorClass = 'cd-sdk-form-error';
                errorInfo = fieldsInError[field];
            }
        });
    }
    const phonesList = countriesProvinces.map(x => {
        return {
            val: x.prefix_phone,
            name: x.prefix_phone
        };
    });
    sortList(phonesList);
    const countryList = countriesProvinces.map(x => {
        return {
            val: x.code,
            name: x.name_es
        };
    });
    sortList(countryList);
    const provinceList = provinces
        // A REVISAR .filter(x => x.country === SharedDataService.defaultPersonalInfo['address-country'])
        .map(x => {
            return {
                val: x.short ? x.short : x.name,
                name: x.english ? x.english : x.name
            };
        });
    sortList(provinceList);

    let dropDown = `<option></option>`;
    let tooltip = !!input.toolTip ? `<span class="cd-sdk-txt-info" id="tooltip-${input.inputName.trim()}">${input.toolTip}</span>` : '';
    if (input.dropDown !== '') {
        dropDown = !!input.dropDown ? input.dropDown.split(',').map(txt => {
            if (JSON.stringify(input.value).toLowerCase() === JSON.stringify(txt).toLowerCase()) {
                return `<option selected>${txt}</option>`;
            }
            return `<option>${txt}</option>`;
        }) : [];
    } else {
        if (input.issuerFieldsName === 'country') {
            countryList.forEach(country => {
                const selected = JSON.stringify(input.value).toLowerCase() === JSON.stringify(country.val).toLowerCase() ? 'selected' : '';
                dropDown += `<option value="${country.val}" ${selected}>${country.name}</option>`;
            });
        } else if (input.issuerFieldsName === 'address-province') {
            provinceList.forEach(province => {
                const selected = JSON.stringify(input.value).toLowerCase() === JSON.stringify(province.val).toLowerCase() ? 'selected' : '';
                dropDown += `<option value="${province.val}" ${selected}>${province.name}</option>`;
            });
        } else if (input.issuerFieldsName === 'phone-prefix') {
            phonesList.forEach(prefix => {
                const selected = JSON.stringify(input.value).toLowerCase() === JSON.stringify(prefix.val).toLowerCase() ? 'selected' : '';
                dropDown += `<option value="${prefix.val}" ${selected}>${prefix.name}</option>`;
            });
        }
    }
    let html = `
  <div class="cd-sdk-form-row-account">
    <div class="cd-sdk-form-row-account-container ${errorClass}" id="${input.inputName.trim()}-input-cont">
      <label for="${input.inputName.trim()}" class="cd-sdk-form-label">${input.label}</label>
      <div class="cd-sdk-form-select-container">
        <select id="${input.inputName.trim()}" name="${input.inputName.trim()}" class="cd-sdk-form-select">
          ${dropDown}
        </select>
      </div>
    </div>
    ${tooltip}
    <span id="error-txt-${input.inputName.trim()}">${errorInfo}</span>
  </div>`;
    return html;
}

function createInputSelectButton(input) {

    let dropDown = ``;

    let ArrObjectKeys = Object.keys(input.options);
    let ArrObjectValues = Object.values(input.options);
    ArrObjectKeys.forEach((key, index) => {
        dropDown += `<option value="${key}">${ArrObjectValues[index]}</option>`;
    })

    let html = `<div class="cd-sdk-form-row-account">
    <div class="cd-sdk-form-row-account-container " id="select-${input.name.trim()}-cont">
     <label for="select-${input.name.trim()}" class="cd-sdk-form-label">${input.name.trim()}</label>
             <div class="cd-sdk-form-select-container">
                 <select id="${input.name.trim()}" name="${input.name.trim()}"  value="${input.options[Object.keys(input.options)[0]]}" class="cd-sdk-form-select ${input.name.trim()}">
                 ${dropDown}
                 </select>
             </div>
     </div>
  </div>`;
    return html;

}

function createInputDate(input, dateFormat, fieldsInError) {
    let errorClass = '';
    let errorInfo = '';
    if (fieldsInError !== '') {
        const fields = Object.keys(fieldsInError);
        fields.forEach(field => {
            if (field.toString() === input.inputName.trim().toString()) {
                errorClass = 'cd-sdk-form-error';
                errorInfo = fieldsInError[field];
            }
        });
    }
    let dateValue = '';
    if (input.value !== '') {
        dateValue = input.value.split('-').join('/');
    }
    let tooltip = !!input.toolTip ? `<span class="cd-sdk-txt-info" id="tooltip-${input.inputName.trim()}">${input.toolTip}</span>` : '';
    let html = `
  <div class="cd-sdk-form-row-account">
    <div class="cd-sdk-form-row-account-container ${errorClass}" id="${input.inputName.trim()}-input-cont">
      <label for="${input.inputName.trim()}" class="cd-sdk-form-label">${input.label}</label>
      <input type="text" class="cd-sdk-form-txt inputDate" value="${dateValue}" id="${input.inputName.trim()}" name="${input.inputName.trim()}" attr-format="${dateFormat}">
    </div>
    ${tooltip}
    <span class="txt-error" id="error-txt-${input.inputName.trim()}">${errorInfo}</span>
  </div>`;
    return html;
}

function configureInputDate() {
    let inputs = document.getElementsByClassName('inputDate');
    var options = {
        year: "numeric",
        month: "2-digit",
        day: "2-digit"
    };

    for (let i = 0; i < inputs.length; i++) {
        const picker = datepicker('#' + inputs[i].id, {
            overlayButton: "Ok",
            format: 'dd/mm/yyyy',
            formatter: (input, date, instance) => {
                const value = date.toLocaleDateString("es", options)
                input.value = value
            },
            customDays: ['Lu.', 'Ma.', 'Mi.', 'Ju.', 'Vi.', 'Sa.', 'Do.'],
            customMonths: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        });

        if (inputs[i].value !== '') {
            const date = inputs[i].value.split('/');
            picker.setDate(new Date(date[2], parseInt(date[1]) - 1, date[0]), true);
        }
    }
}

module.exports = {
    paintFormInputs,
    isValidInput,
    isValidForm,
    getFormValues
};