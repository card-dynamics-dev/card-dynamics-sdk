import axios from "axios";

export default class EnrollmentService {

    constructor(settings){
        this.settings = settings;
    }

    async sendResult(enrollemntId, data){
        return axios.post(`${process.env.ENROLLMENT_API_URL}/enrollment/${enrollemntId}/send-result`, data, {
            headers: {
                'Content-Type': 'application/json',
                'x-api-key': this.settings.api_key,
                'accept-language': this.settings.language || navigator.language || navigator.userLanguage 
            }
        })
        .catch(err => err.response);
    }
}