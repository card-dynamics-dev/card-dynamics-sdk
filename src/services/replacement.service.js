import axios from "axios";

export default class ReplacementService {

    constructor(settings){
        this.settings = settings;
    }

    async new(){
        return axios.get(`${process.env.REPLACEMENT_API_URL}/tokenization/new`, {
            headers: {
              'Content-Type': 'application/json',
              'x-api-key': this.settings.api_key,
              'accept-language': this.settings.language || navigator.language || navigator.userLanguage 
            }
        })
        .catch(err => err.response);
    }

    async tokenize(data){
        return axios.post(`${process.env.REPLACEMENT_API_URL}/tokenization/tokenize`, data, {
            headers: {
                'Content-Type': 'application/json',
                'x-api-key': this.settings.api_key,
                'accept-language': this.settings.language || navigator.language || navigator.userLanguage 
            }
        })
        .catch(err => err.response);
    }

    async change(data){
        return axios.post(`${process.env.REPLACEMENT_API_URL}/changerequest/issuerrequest/change`, data, {
            headers: {
                'Content-Type': 'application/json',
                'x-api-key': this.settings.api_key,
                'accept-language': this.settings.language || navigator.language || navigator.userLanguage 
            }
        })
        .catch(err => err.response);
    }

    async checkStatus(cdChangeRequestId){
        return axios.get(`${process.env.REPLACEMENT_API_URL}/changerequest/issuerrequest/${cdChangeRequestId}`, {
            headers: {
              'Content-Type': 'application/json',
              'x-api-key': this.settings.api_key,
              'accept-language': this.settings.language || navigator.language || navigator.userLanguage 
            }
        })
        .catch(err => err.response);
    }

    async getMerchantList() {
        return axios.get(`${process.env.REPLACEMENT_API_URL}/categorisation/issuer/export`, {
            headers: {
                'Content-Type': 'application/json',
                'x-api-key': this.settings.api_key,
                'accept-language': this.settings.language || navigator.language || navigator.userLanguage 
            }
        })
        .catch(err => err.response);
    }

    async sendResult(data){
        return axios.post(`${process.env.REPLACEMENT_API_URL}/changerequest/issuerrequest/send-result`, data, {
            headers: {
                'Content-Type': 'application/json',
                'x-api-key': this.settings.api_key,
                'accept-language': this.settings.language || navigator.language || navigator.userLanguage 
            }
        })
        .catch(err => err.response);
    }
}