import axios from "axios";
/*import SharedKeys from "../common/shared-keys";
import {
  bankBaseUrl,
  bankToken,
  enrollmentSettingsBaseUrl
} from "../config/api-data"; */
import CryptoHelper, { encryptString } from "../helpers/crypto.helper";

export default class BankService {

  constructor(settings){
    this.settings = settings;
  }
  /**
   * GET  With this method the Merchant will request all the bank conectors available
   */
   getBankList() {
    return axios.get(`${process.env.BANK_ADAPTER_BASE_URL}/api/bank-adapters/${this.settings.country}`, {
      headers: {
        'Authorization': `Bearer ${this.#token}`
      }
    });
  }

   /**
   * GET  With this method the Merchant will request the bank conector by the adapter code
   */
  getBankDetails(adapter){
    return axios.get(`${process.env.BANK_ADAPTER_BASE_URL}/api/bank-adapter/${adapter}/info`, {
      headers: {
        'Authorization': `Bearer ${this.#token}`
      }
    });
  }

  /**
   * POST Request to init login process
   */
  startAggregation(adapter, data){
    return axios.post(`${process.env.BANK_ADAPTER_BASE_URL}/api/adapter/${adapter}/aggregation-start`, data, 
    {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${this.#token}`
      }
    });
  }

  /**
   * POST Request to check aggregation response
   */
  checkAggregationResponse(adapter, data){
    return axios.post(`${process.env.BANK_ADAPTER_BASE_URL}/api/adapter/${adapter}/aggregation-response`, data,
     {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${this.#token}`
      }
    });
  }

  /**
   * GET bank adapter settings
   */
   getBankAdapterSettings(adapter){
     return axios.get(`${process.env.ENROLLMENT_SETTINGS_API_URL}/sdk/button-adapter-settings/${adapter}`, {
      headers: {
        'Content-Type': 'application/json'
      }
     });
  }

  /**
   * GET bank adapter settings by visibility
   */
   getBankAdapterSettingsByVisibility(cdMerchantId){
    return axios.get(`${process.env.ENROLLMENT_SETTINGS_API_URL}/sdk/button-adapter-settings/visibility/merchant/${cdMerchantId}`, {
     headers: {
       'Content-Type': 'application/json'
     }
    });
 }

  /**
   * POST aggregation resume request
   */
  aggregationResume(adapter, data){
    return axios.post(`${process.env.BANK_ADAPTER_BASE_URL}/api/adapter/${adapter}/aggregation-resume`, data, 
    {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${this.#token}`
      }
    });
  }

  get #token(){
    return encryptString(`${this.settings.client_id}:${this.settings.api_token}:${Date.now()}`, this.settings.default_secret)
  }
}