import { decryptObjectValues } from "../helpers/crypto.helper";
import axios from "axios";

let merchantList = []; // Saved list. If there is data saved don't call service again

export default class MerchantService {
  /**
   * GET  With this method the Issuer will request all the merchants available to push an Enrollment
   *      Request and get an array with all the details to render the promotions and registration
   *      forms of all the merchants available to receive new Registrations.
   */

  constructor(settings){
    this.settings = settings;
  }

  async getMerchantList2(fromButton,selectedMerchantId) {

    
    let query = '';

    if(fromButton) query += `/button`;   
    if(selectedMerchantId && selectedMerchantId !== "") query += `?cdMerchantId=${selectedMerchantId}`;

    return  axios.get(`${process.env.ENROLLMENT_API_URL}/enrollment/merchants${query}`, { //Pasar merchantId
      headers: {
        'x-api-key': this.settings.api_key,
        'accept-language': this.settings.language || navigator.language || navigator.userLanguage  
      }
    })
    .then(result => {
      let { data } = result;
      return data.map(item => {

        let el = { merchantType: item.merchantType, merchants: [] };
         
        for(let merchant of item.merchants){
            let { form, merchantDetails } = merchant;
            if(form && merchantDetails){
              el.merchants.push(decryptObjectValues(merchant, this.settings.secret_key, ['form', 'merchantDetails', 'promotions']));
            }
        }
        return el;
      });
    })
    .catch(error => {
      throw error.response;
    });
  }

  async getMerchantList(fromButton,selectedMerchantId) {

    let query = '';

    if(fromButton) query += `/button`;   
    if(selectedMerchantId && selectedMerchantId !== "") query += `?cdMerchantId=${selectedMerchantId}`;
     
   

    if (merchantList.length <= 0) {
      return fetch(`${process.env.ENROLLMENT_API_URL}/enrollment/merchants${query}`, { //Pasar merchantId
        method: 'get',
        mode: 'cors',
        headers: {
          'x-api-key': this.settings.api_key,
          'accept-language': this.settings.language || navigator.language || navigator.userLanguage  
        }
      })
        /* DON'T REMOVE */
        .then(res => {
          if (res.ok) {
            return res.json();
          } else {
            return Promise.reject({
              status: res.status,
              statusText: res.statusText
            });
          }
        })
        .then(res => res.map(resJson => {

          let el = { merchantType: resJson.merchantType, merchants: [] };
           
          for(let merchant of resJson.merchants){
            console.log(merchant)
              let { promotions, form, merchantDetails } = merchant;
              if(form && merchantDetails){
                el.merchants.push(decryptObjectValues(merchant, this.settings.secret_key, ['form', 'merchantDetails', 'promotions']));
              }
          }

          return el;
        }))
        .then(decryptedRes => merchantList = decryptedRes)
    } else {
      return new Promise(resolve => resolve(merchantList));
    }
  }

  /**
   *  categorisation/issuer/export
   *  Card Dynamics Merchants Request for replacement
      Description
      Request to get all the Card Dynamics merchants for a specific Issuer and all the ones
      available having Issuer’s country. Will return both categorized and not categorized
      merchants.
   * 
   */

  async getMerchantListReplacement() {

    return fetch(`${replacementBaseUrl}/categorisation/issuer/export`, {
      method: 'get',
      mode: 'cors',
      headers: {
        'x-api-key': SharedKeys.xApiKey,
        'accept-language': SharedDataService.getFlowsLanguage() || navigator.language || navigator.userLanguage
      }
    })
      /* DON'T REMOVE */
      .then(res => {
        if (res.ok) {
          return res.json();
        } else {
          return Promise.reject({
            status: res.status,
            statusText: res.statusText
          });
        }
      })
  }

  /**
   * POST  The Issuer can send the full list of values to attempt the customer registration on merchant.
   * 
   * @param {registrationForm: {}, cdMerchantId: string, integrityCheckinputName: string} data 
   */
  async merchantRegistration(data) {
    
    return fetch(`${process.env.ENROLLMENT_API_URL}/enrollment/register`, {
      method: 'post',
      mode: 'cors',
      headers: {
        'x-api-key': SharedKeys.xApiKey,
        'Content-Type': 'application/json',
        'accept-language': SharedDataService.getFlowsLanguage() || navigator.language || navigator.userLanguage
      },
      body: JSON.stringify(data)
    })
      .then(res => res)
      .catch(error => console.log('Request failed', error));
  }

  /**
   * POST  After having registered a new customer on merchant, the Issuer can add a customer’s Card Account to the merchant.
   * 
   * @param {enrollmentId: string, customerId: string, cardData: object, integrityCheck: string} data 
   */
  async cardRegistration(data) {
    
    return fetch(`${enrollmentBaseUrl}/enrollment/register/card`, {
      method: 'post',
      mode: 'cors',
      headers: {
        'x-api-key': SharedKeys.xApiKey,
        'Content-Type': 'application/json',
        'accept-language': SharedDataService.getFlowsLanguage() || navigator.language || navigator.userLanguage
      },
      body: JSON.stringify(data)
    })
      .then(res => res)
      .catch(error => console.log('Request failed', error));
  }

  /**
   * POST  After having registered a new customer on merchant, the Issuer can add a customer’s Bank Account to the merchant.
   *
   * @param {enrollmentId: string, customerId: string, bankAccountData: object, integrityCheck: string} data
   */
  async bankAccountRegistration(data) {
    
    return fetch(`${enrollmentBaseUrl}/enrollment/register/bankaccount`, {
      method: 'post',
      mode: 'cors',
      headers: {
        'x-api-key': SharedKeys.xApiKey,
        'Content-Type': 'application/json',
        'accept-language': SharedDataService.getFlowsLanguage() || navigator.language || navigator.userLanguage
      },
      body: JSON.stringify(data)
    })
      .then(res => res)
      .catch(error => console.log('Request failed', error));
  }

  /**
   * POST  The Issuer can send the full list of values to attempt the customer registration on merchant with payment data.
   *
   * @param {registrationData: {}, cdMerchantId: string, cardData: object, bankData: object, promoId: string, integrityCheck: string, enrollmentOrigin: string} data
   */
  async registerWithPayment(data) {
  
    return fetch(`${process.env.ENROLLMENT_API_URL}/enrollment/register-with-payment`, {
      method: 'post',
      mode: 'cors',
      headers: {
        'x-api-key': this.settings.api_key, 
        'Content-Type': 'application/json;charset=UTF-8',
        'accept-language': this.settings.language || navigator.language || navigator.userLanguage
      },
      body: JSON.stringify(data)
    })
      .then(res => res)
      .catch(error => console.log('Request failed', error));
  }

  /**
   * POST  Check status of registration with payment request.
   *
   * @param { enrollmentRequestId: string } data
   */

  async checkStatusRegistrationWithPayment(data) {

    let url = `${process.env.ENROLLMENT_API_URL}/enrollment/${data.enrollmentRequestId}/register-with-payment`;

    return fetch(url, {
        method: 'get',
        mode: 'cors',
        headers: {
            'x-api-key': SharedKeys.xApiKey,
            'Content-Type': 'application/json;charset=UTF-8',
            'accept-language': this.settings.language || navigator.language || navigator.userLanguage
        },
    })
    .then(res => res)
    .catch(error => console.log('Request failed', error));;
  }

  /**
   * POST  The Issuer can send validation code from EMAIL or CODE.
   *
   * @param {enrollmentRequestId: string, validationType: string, code: string} data
   */
  async enrollmentConfirmation(data) {

    return fetch(`${process.env.ENROLLMENT_API_URL}/enrollment/confirmation`, {
      method: 'post',
      mode: 'cors',
      headers: {
        'x-api-key': this.settings.api_key,
        'Content-Type': 'application/json',
        'accept-language': this.settings.language || navigator.language || navigator.userLanguage 
      },
      body: JSON.stringify(data)
    })
      .then(res => res)
      .catch(error => console.log('Request failed', error));
  }

  /**
   * POST  Check status of enrollment confirmation request.
   *
   * @param { enrollmentRequestId: string } data
   */

  async checkStatusEnrollmentConfirmation(data) {
  
    return fetch(`${enrollmentBaseUrl}/enrollment/${data.enrollmentRequestId}/confirmation`,{
        method: 'get',
        mode: 'cors',
        headers: {
            'x-api-key': SharedKeys.xApiKey,
            'Content-Type': 'application/json',
            'accept-language': SharedDataService.getFlowsLanguage() || navigator.language || navigator.userLanguage
        },
    })
        .then(res => res)
        .catch(error => console.log('Request failed', error));
  }
}