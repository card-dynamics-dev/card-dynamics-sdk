import axios from "axios";
import { handleError } from "../helpers/generic.helper";
//import { enrollmentSettingsBaseUrl } from "../config/api-data";

export default class SdkService {

    /** GET skey by issuer id */
    async getIssuerSKey(issuerId) {
        return axios.get(`${process.env.ENROLLMENT_SETTINGS_API_URL}/sdk/${issuerId}/key`)
            .catch(err => {
                return handleError(err);
            });
    }

    request(url, data){
        return axios.post(url, data);
    }
}