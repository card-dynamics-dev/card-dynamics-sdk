const generic = {
    buildDateFromTimestamp: function (timestamp) {
        if(!timestamp){
            return '';
        }
        let a = new Date(timestamp);
        let year = a.getFullYear();
        let month = (a.getMonth() + 1).toString().padStart(2, "0");
        let date = a.getDate().toString().padStart(2, "0");
        return date + '/' + month + '/' + year;
    },

    removeSpace: function(value){
        if(!value){
            return '';
        }

        return value.replace(/\s/g, '');
    },

    removeSlash: function(value){
        if(!value){
            return '';
        }

        return value.replace(/\//g, '');
    },

    replaceI18nValue: function(text, value){
        if(!text){
            return '';
        }

        return text.replace('{value}', value);
    },

    toLowerCase: function(string){
        if(!string){
            return "";
        }
        return string.toLowerCase();
    },

    convertString: function(string) {
        let fixedstring = '';
    
        try {
          fixedstring = decodeURIComponent(escape(string));
        } catch (e) {
          fixedstring = string;
        }
    
        return fixedstring;
      },

    copyContentToClipboard: async function(text) {
        try {
            await navigator.clipboard.writeText(text);
        } catch (_) {}
    },

    validateUrl: function(url) {
        if (url !== '' && url.indexOf("http://") === -1 && url.indexOf("https://") === -1) {
            url = 'https://' + url;
        }
        return url;
    },

    getOS: function() {
        var userAgent = window.navigator.userAgent,
            platform = window.navigator.platform,
            macosPlatforms = ['Macintosh', 'MacIntel', 'MacPPC', 'Mac68K'],
            windowsPlatforms = ['Win32', 'Win64', 'Windows', 'WinCE'],
            iosPlatforms = ['iPhone', 'iPad', 'iPod'],
            os = null;
      
        if (macosPlatforms.indexOf(platform) !== -1) {
          os = 'Mac OS';
        } else if (iosPlatforms.indexOf(platform) !== -1) {
          os = 'iOS';
        } else if (windowsPlatforms.indexOf(platform) !== -1) {
          os = 'Windows';
        } else if (/Android/.test(userAgent)) {
          os = 'Android';
        } else if (!os && /Linux/.test(platform)) {
          os = 'Linux';
        }
      
        return os;
    },
    
    mask: function (string, maskChar, maskCharLength, unmaskedStartLength = 0, unmaskedEndLength = 0) {
        const maskStart = Math.max(0, unmaskedStartLength);
        const maskEnd = Math.max(0, string.length - unmaskedEndLength);
        let maskCharIndex = 0;
        return string
            .split("")
            .map((char, index) => {
                if (index >= maskStart && index < maskEnd) {
                    if(maskCharIndex === maskCharLength){
                        return;
                    }
                    maskCharIndex += 1;
                    return maskChar;
                }
                else {
                    return char;
                }
            })
            .join("");
    },

    handleError: function(err){
        if(err.response){
            return err.response
        }
        else{
            return { status: 500, message: "networkError" }
        }
    }
}

module.exports = generic;