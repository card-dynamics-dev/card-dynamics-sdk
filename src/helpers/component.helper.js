module.exports = {
    
    initViewHtml: function(i18n){
        document.body.outerHTML = `
        <body class="cd-sdk-carddynamics">
         
          <div class="cd-sdk-replacement"></div>

          <div style="display: none" class="cd-sdk-popup cd-sdk-alerts">
            <div class="cd-sdk-popup-container">
              <span class="cd-sdk-popup-icon"></span>
              <div class="cd-sdk-popup-text"></div>
              <div class="cd-sdk-simple-btns">
                  <button type="button">${i18n.button.cancel}</button>
                  <button type="button">${i18n.button.accept}</button>
              </div>
            </div>
          </div>
        </body>`
    }
}