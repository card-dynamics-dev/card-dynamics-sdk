const aes256  = require('../config/libs/aes256');
module.exports = {
    
    encryptString: function(data, secret_key){
        return aes256.encrypt(secret_key, data);
    },

    encryptObject: function(data, secret_key){
        return aes256.encrypt(secret_key, JSON.stringify(data));
    },

    encryptObjectValues: function(data, secret_key, fields){
        
        for(let key of fields) {
            let value = data[key];
            if(!value){
                continue;
            }
            else if(typeof value === 'object'){
                value = JSON.stringify(value);
            }
            
            data[key] = aes256.encrypt(secret_key, value);
        }

        return data;
    },

    encryptAllObjectValues: function(data, secret_key){
        for (const [key, value] of Object.entries(data)){
            if(!value) continue;
            data[key] = aes256.encrypt(secret_key, value);
        }
        return data;
    },

    decryptString: function(data, secret_key){
        return aes256.decrypt(secret_key, data);
    },

    decryptAllObjectValues: function(data, secret_key){
        for (const [key, value] of Object.entries(data)){
            if(!value){
                continue;
            }
            else {
                data[key] = aes256.decrypt(secret_key, value);
            }
        }

        return data;
    },

    decryptObjectValues: function(data, secret_key, fields){
        for(let key of fields){
            let value = data[key];
            if(!value){
                continue;
            } 
            else {
                data[key] = aes256.decrypt(secret_key, data[key]);
            }
        }

        return data;
    },

    integrityCheck: function(data, fields) {
        
        let value = "";
        if(!fields) fields = Object.key(data);

        for(let key of fields){
            if(!data[key]){
                continue;
            }
            else if (typeof data[key] != 'string') {
                value += JSON.stringify(data[key]);
            }
            else {
                value += data[key];
            }
        }

        return aes256.hash256(value);;
    }
}