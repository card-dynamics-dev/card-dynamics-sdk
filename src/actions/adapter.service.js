import locale from "../config/locale/i18n";
import { hideSpinner, openModal, showSpinner } from "../common/sdk.common";
import { decryptString, encryptObject } from "../helpers/crypto.helper";
import addMerchantView from "../views/common/addMerchantView";
import addBankVerificationView from "../views/button/addBankVerificationView";
import { buildDateFromTimestamp, removeSpace } from "../helpers/generic.helper";

let context, i18n;
export async function startAggregation(ctx, adapter, dataValues, autorun = false) {

    let { settings: { language, default_secret }, settings, bankService } = ctx;

    i18n = locale(language);
    context = ctx;

    showSpinner('Conectando con su banco...');

    let requestData = {
      parameters: encryptObject(dataValues, default_secret),
      aggregation_options: {
        products: ["cards", "customers", 'accounts'],
        from_date: null,
        to_date: null
      }
    };

    if (settings && settings.demo) {

      adapter = settings.country && settings.country.toLowerCase() === 'mx' ? 'BNKDUMMX' : 'BNKDUMES';

      Object.assign(requestData, {
        parameters: encryptObject({
          test_type: "normal",
          number_of_products: 2
        }, settings.default_secret)
      });
    }

    if (adapter === 'BNKDUMES' || adapter === 'BNKDUMMX') {
      requestData.force_aggregation = true;
    }

    bankService.startAggregation(adapter, requestData)
      .then(result => {
        let data = result.data;
        if (data.status_name == "PROCESSING") {

          let interval = setInterval(function () {
            checkAggregationResponse(adapter, { aggregation_id: data.aggregation_id }, interval, autorun);
          }, 5000);

          checkAggregationResponse(adapter, { aggregation_id: data.aggregation_id }, interval, autorun);
        }
        else if (data.status_name == "PARAMETERS_INCORRECT") {
          hideSpinner();
          //createModal({
          console.log({
            title: '',
            body: data.status_message,
            btn: 1,
            eventAction: 'Merchant_',
            eventCategory: 'Error_registro_pago',
            eventLabel: 'Aceptar'
          });
          let inputs = Object.keys(data.errors).map(x => { return { name: x } });
          fieldsInError = data.errors;
          isValidForm(inputs, fieldsInError, true);
        }
        else {
          processAggregationResult(adapter, result);
        }
      })
      .catch(err => {
        hideSpinner();
        //createModal({
        console.log({
          title: '',
          body: "Aggregation start failed: " + JSON.stringify(err),
          btn: 1,
          eventAction: 'Merchant_',
          eventCategory: 'Error_registro_pago',
          eventLabel: 'Aceptar'
        });
        console.log(err);
      });
}

export async function aggregationResume (ctx, adapter, dataValues, aggregation_id = null) {

    let { settings: { language, default_secret }, bankService } = ctx;

    i18n = locale(language);
    context = ctx;

    let requestData = {
      aggregation_id,
      parameters: encryptObject(dataValues, default_secret)
    };

    bankService.aggregationResume(adapter, requestData).then(
      result => {
        let data = result.data;
        if (data.status_name === "PROCESSING") {

          let interval = setInterval(function () {
            checkAggregationResponse(adapter, data, interval);
          }, 5000);

          checkAggregationResponse(adapter, data, interval);
        }
        else {
          processAggregationResult(result)
        }
      },
      err => {
        console.log(err);
        openModal({ text: i18n.error.default }, 'error');
        hideSpinner();
    })
  }

/** Helpers */

function checkAggregationResponse(adapter, data, interval, autorun = false) {
  let { bankService } = context;
  bankService.checkAggregationResponse(adapter, { aggregation_id: data.aggregation_id }, false)
    .then(result => {
      processAggregationResult(result, interval, autorun);
    })
    .catch(err => {
      console.log(err);
      hideSpinner();
      clearInterval(interval);
      openModal({ text: i18n.error.default }, 'error');
    });
}

function processAggregationResult(result, interval, autorun = false) {

  let { settings: { form, default_secret, country } } = context;
  result = result.data;

  if (result.status_name === "SUCCESS") {
    //loader('none');
    if (interval) clearInterval(interval);

    let products = decryptString(result.products, default_secret);

    //Personal info
    let personalInfo = {};
    let customers = (products && products.customers) ? products.customers : [];

    let customer = customers.length > 0 ? customers[0] : null;

    if (customer) {

      personalInfo = {
        "name": customer.name,
        "surname": customer.name_extra,
        "surname2": "",
        "birthday": buildDateFromTimestamp(customer.born_date),
        "email": (customer.emails && customer.emails.length > 0) ? customer.emails[0].toLowerCase() : "",
        "document": customer.identification_number,
        "phone-prefix": "",
        "phone-number": (customer.phones && customer.phones.length > 0) ? removeSpace(customer.phones[0]) : ""
      }

      if (customer.contact_addresses && customer.contact_addresses.length > 0) {
        Object.assign(personalInfo, {
          "address-street": customer.contact_addresses[0].street,
          "address-other": customer.contact_addresses[0].street_extra,
          "address-number": '', //street_number
          "address-city": customer.contact_addresses[0].city,
          "address-province": customer.contact_addresses[0].country_state,
          "address-postal-code": customer.contact_addresses[0].zip,
          "address-country": customer && customer.identification_country ? customer.identification_country : country.toUpperCase()
        });
      }
    }

    context.personalInfo = personalInfo;

    //paymentMethods
    let creditCards = [];
    let cards = (products && products.cards) ? products.cards : [];
    cards.forEach(card => {
      creditCards.push({
        cardHolder: card.printed_holder_name,
        pan: card.number,
        expDate: card._expiration_month && card._expiration_year ? card._expiration_month + '' + card._expiration_year : null,
        country: card.country ? card.country : (customer && customer.identification_country ? customer.identification_country : country.toUpperCase()), // TODO: revisar porqué no viene card country
        currency: card.currencies
      });
    });

    let bankAccounts = [];
    let accounts = (products && products.accounts) ? products.accounts : [];
    accounts.forEach(account => {
      let { country, control_digits, bank, branch, number, partial_number, balances } = account;
      let currency = Object.keys(balances);
      let { adapterInfo: { logo } } = context;
      bankAccounts.push({
        accountHolder: `${personalInfo.name} ${personalInfo.surname}`,
        country: account.country ? account.country : (customer && customer.identification_country ? customer.identification_country : country.toUpperCase()), // TODO: revisar porqué no viene card country
        number: `${country}${control_digits}${bank}${branch}${number}`,
        currency
      })
    });

    context.paymentMethods = { creditCards, bankAccounts };

    if (!form) {
      const event = new CustomEvent('sdkcallback', { 
        detail: {
          result: {
            status: "success",
            personalInfo, 
            paymentMethods: { creditCards, bankAccounts } 
          },
          close: true
      }});
      document.dispatchEvent(event);
      hideSpinner();
    }

    else if(context.loginDone){
      const event = new CustomEvent('login-result', { detail: context });
      document.dispatchEvent(event);
    }
    else {
      //Go to merchant view
      context.resultDone = true;
      addMerchantView(context);
    }

  } else if (result.status_name === "PROCESSING") {
    if(result.login_done && form){
      context.loginDone = true;
      const event = new CustomEvent('login-done', { detail: { login: true } });
      document.dispatchEvent(event);
    }
    return result;
  }
  else if (result.status_name == "ACTION_REQUIRED") {
    clearInterval(interval);

    if(autorun){
      const event = new CustomEvent('autorun', { detail: result });
      document.dispatchEvent(event);
    }
    else{
      hideSpinner();

      let { confirmation_parameters: parameters, aggregation_id } = result;

      showSpinner(i18n.spinner.loading);
      context.adapterVerification = {
        parameters,
        aggregation_id
      }

      addBankVerificationView(context);
    }
  }
  else {
    hideSpinner();
    if (interval) clearInterval(interval);
    openModal({ text: result.status_message }, 'error');
    return result;
  }
}